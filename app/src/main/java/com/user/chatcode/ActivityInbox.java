package com.user.chatcode;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.user.chatcode.adapter.HorizontalAdapter;
import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.chatting.ChatApplication;
import com.user.chatcode.chatting.Message;
import com.user.chatcode.chatting.MessageAdapter;
import com.user.chatcode.model.ActiveModel;
import com.user.chatcode.model.MessagesModel;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;
import com.user.chatcode.utils.RoundRectCornerImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.user.chatcode.ActivityChatting.model;
import static com.user.chatcode.ActivityChatting.usergroup;
import static com.user.chatcode.ActivityChatting.usergroup_name;
import static com.user.chatcode.ActivityMainChatting.context;
import static com.user.chatcode.ActivityMainChatting.next_url;

/**
 * Created by user on 10/4/17.
 */

public class ActivityInbox  extends AppCompatActivity implements OnItemClickListener {
    TextView heading;
    static RecyclerView horizontal_recycler_view;
    static HorizontalAdapter horizontalAdapter;
    public static Resources resources;
    static ScrollView parent;
    static JSONObject data = null;
    public static String id,userid;
    private static final int REQUEST_LOGIN = 0;
    private static final int TYPING_TIMER_LENGTH = 600;
    private static RecyclerView mMessagesView;
    static OnItemClickListener clickListener;
    private EditText mInputMessageView;
    private static List<Message> mMessages = new ArrayList<Message>();
    private static MessageAdapter mAdapter;
    private boolean mTyping = false;
    static Boolean update;
    private Handler mTypingHandler = new Handler();
    public static String mUsername;
    private static Socket mSocket;
    public static Activity activity;
    static String next_user_url;
    static String last_id;
    private Boolean isConnected = true;
    public static int a = 0;
    Button back, info;
    static ProgressBar progressBar;
    public static String name,created_by;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        clickListener=this;
        ChatApplication app = (ChatApplication) getApplication();
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
//        mSocket.on("joingroup", onUserJoined);
//        mSocket.on("user left", onUserLeft);
        mSocket.on("updatechat", onNewMessage);
        mSocket.on("typing", onTyping);
        mSocket.on("stop typing", onStopTyping);

        mSocket.connect();
        activity = ActivityInbox.this;
        progressBar = (ProgressBar) findViewById(R.id.progress);
        parent = (ScrollView) findViewById(R.id.parent);
        heading = (TextView) findViewById(R.id.heading);
        info = (Button) findViewById(R.id.info);
        info.setVisibility(View.GONE);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityInbox.this, ActivityInfo.class);
                i.putExtra("position", getIntent().getIntExtra("position", 0));
                startActivity(i);
            }
        });
        if (null != getIntent().getStringExtra("title"))
            heading.setText(getIntent().getStringExtra("title"));
        mInputMessageView = (EditText) findViewById(R.id.message_input);
        mInputMessageView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == R.id.send || id == EditorInfo.IME_NULL) {
                    attemptSend();
                    return true;
                }
                return false;
            }
        });
        mInputMessageView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (null == mUsername) return;
                if (!mSocket.connected()) return;
                if (!mTyping) {
                    mTyping = true;
                    mSocket.emit("typing");
                }
                mTypingHandler.removeCallbacks(onTypingTimeout);
                mTypingHandler.postDelayed(onTypingTimeout, TYPING_TIMER_LENGTH);
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        Button sendButton = (Button) findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSend();
            }
        });
        back = (Button) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        mMessagesView = (RecyclerView) findViewById(R.id.messages);
        mMessages.clear();
        mAdapter = new MessageAdapter(this, mMessages);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mMessagesView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    // Scrolling up
                } else {
                    Boolean a = listIsAtTop();
                    if (a && null != next_url && progressBar.getVisibility() == View.GONE) {
                        getNextMessages(next_url, userid);
                    }
                    //Scrolling down
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView view, int scrollState) {
                if (scrollState == GridView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
//
                }
            }
        });

        mMessagesView.setLayoutManager(new LinearLayoutManager(this));
        mMessagesView.setAdapter(mAdapter);
        resources = getResources();
        mUsername = CommonUtils.getTempFileName(ActivityInbox.this, "name");
        horizontal_recycler_view = (RecyclerView) findViewById(R.id.horizontal_recycler_view);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManager);

//      int currentSize=mMessages.size();
        mMessages.clear();

            userid = getIntent().getStringExtra("userid");
//        mAdapter.notifyItemRangeRemoved(0, currentSize);
            if (userid != null)
                add(userid);
    }

    public void alert(String message) {
        finish();
        final Dialog dialog = new Dialog(context);
        dialog.setContentView(R.layout.show_message_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button back = (Button) dialog.findViewById(R.id.cancel);
        Button readonly = (Button) dialog.findViewById(R.id.readonly);
        TextView message1 = (TextView) dialog.findViewById(R.id.message);
        Log.v("result", "result");
        if(message.contains("full")||
                message.contains("This is not a chatcode")) {
            readonly.setVisibility(View.GONE);
            Log.v("result1", "result1");
            back.setText("OK");
            Log.v("result2", "result2");
        }
        message1.setText(message);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        readonly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                read_only(usergroup);
            }
        });

        dialog.show();

//        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
//        alertDialogBuilder.setCancelable(false);
//        alertDialogBuilder.setMessage(message);
//        alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//            public void onClick(DialogInterface dialog, int id) {
//                dialog.dismiss();
//                finish();
//            }
//        });
//        alertDialogBuilder.create().show();
    }
    public static void  read_only(final String usergroup) {
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.read_chat(usergroup,CommonUtils.getTempFileName(activity,"id")).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    if(response.code()==200) {
                        if(null!=response.body()) {
                            Object resObject = new Gson().toJson(response.body());
                            String resString = resObject.toString();
                            JSONObject obj = new JSONObject(resString);
                            Log.v("response_obj",obj.toString());

                        }
                    }else if(response.code()==500){
                    }

                } catch (Throwable e) {
                    onFailure(call, e);
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.v("response_obj","fail");
            }
        });
    }

    public static void getNextMessages(String url, final String userid) {
            progressBar.setVisibility(View.VISIBLE);
        UserApi userApi = ApiFactory.createServiceForGson();

        userApi.next_page_url_(url,CommonUtils.getTempFileName(activity,"id"),userid).enqueue(new Callback<MessagesModel>() {
            @Override
            public void onResponse(Call<MessagesModel> call, Response<MessagesModel> response) {
                try {
                    if (response.code() == 200) {
                        if (null != response.body()) {
                            next_url = response.body().getNext_page_url();
                            if (null != response.body().getData())
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    mMessages.add(0, new Message.Builder(Message.TYPE_MESSAGE)
                                            .username(response.body().getData().get(i).getName()).
                                                    message(response.body().getData().get(i).getMessage()).build());
                                         mAdapter.notifyItemInserted(0);
                                    if (i == response.body().getData().size() - 1) {
                                        last_id = response.body().getData().get(i).getId();
                                    }
                                }
                                 mAdapter = new MessageAdapter(activity, mMessages);
                                mMessagesView.setLayoutManager(new LinearLayoutManager(activity));
                                mMessagesView.setAdapter(mAdapter);
                                scrollToBottom();

                        }
                    } else if (response.code() == 500) {
                        CommonUtils.getSnackbar(parent, "Internal Server Error");
                    }
                    progressBar.setVisibility(View.GONE);

                } catch (Throwable e) {
                    onFailure(call, e);
                    progressBar.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<MessagesModel> call, Throwable t) {
                CommonUtils.getSnackbar(parent, t.getMessage());
                progressBar.setVisibility(View.GONE);

            }
        });
    }

    private boolean listIsAtTop() {
        if (mMessagesView.getChildCount() == 0) return true;
        return mMessagesView.getChildAt(0).getTop() == 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

//        mSocket.disconnect();
//
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("updatechat", onNewMessage);
//        mSocket.off("joingroup", onUserJoined);
//        mSocket.off("user joined", onUserJoined);
        mSocket.off("user left", onUserLeft);
        mSocket.off("typing", onTyping);
        mSocket.off("stop typing", onStopTyping);

    }


    public static void addLog(final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMessages.add(new Message.Builder(Message.TYPE_LOG)
                        .message(message).build());
                mAdapter.notifyItemInserted(mMessages.size() - 1);
                scrollToBottom();
            }
        });
    }

    private void addParticipantsLog(final int numUsers) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                addLog(getResources().getQuantityString(R.plurals.message_participants, numUsers, numUsers));
            }
        });
    }

    private static void addMessage(String username, String message) {
        mMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                .username(username).message(message).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void addTyping(String username) {
        mMessages.add(new Message.Builder(Message.TYPE_ACTION)
                .username(username).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void removeTyping(String username) {
        for (int i = mMessages.size() - 1; i >= 0; i--) {
            Message message = mMessages.get(i);
            if (message.getType() == Message.TYPE_ACTION && message.getUsername().equals(username)) {
                mMessages.remove(i);
                mAdapter.notifyItemRemoved(i);
            }
        }
    }




    public void add(final String mUserId) {
            mSocket.emit("createonetogroup", CommonUtils.getTempFileName(activity, "id"), CommonUtils.getTempFileName(activity, "name"),usergroup);

//
        mSocket.on("updatechat", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String username;
                String message;
                String room;
                try {
                    username = data.getString("username");
                    message = data.getString("message");
                    room = data.getString("room");
                } catch (JSONException e) {
                    return;
                }
                if (a == 0) {
                    addLog(message);
                    a = 1;
                }
            }
        });
//        switchRooms();
    }


    public void attemptSend() {
        if (null == mUsername) return;
                    if (!mSocket.connected()) return;
                    mTyping = false;
                    String message = mInputMessageView.getText().toString().trim();
                    if (TextUtils.isEmpty(message)) {
                        mInputMessageView.requestFocus();
                        return;
                    }
//        addMessage(mUsername, message);
                    mInputMessageView.setText("");
                    // perform the sending message attempt.
                    mSocket.emit("sendmessage", message, CommonUtils.getTempFileName(activity, "id"),userid);

        }
//        for(int i=0;i<model.size();i++){
//            if(mUsername.equals(model.get(i).getName())){
//                if(model.get(i).getStatus().equals("1")){

//                }else
//                    CommonUtils.addSnackbar(parent,"You can not send message to this group");
//            }
//        }

//        mSocket.on("updatechat", new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            String username;
//                            String message;
//                            username = (String) args[0];
//                            message = (String) args[1];
//                            removeTyping(username);
//                            addMessage(username, message);
//                        }catch (Exception e){
//
//                        }
//                    }
//                });
//            }
//        });


//    private void startSignIn() {
//        Intent intent = new Intent(this, ActivityBeforeMainChatting.class);
//        startActivityForResult(intent, REQUEST_LOGIN);
//    }

    public static void blockOrRemoveChat(String gr_id,String blocked_users,String type){
        mSocket.emit("blockorremove", CommonUtils.getTempFileName(activity, "id")
                , gr_id,blocked_users,type);
        mSocket.on("updategroupusers", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String group_id;
                String status;
                try {
                    group_id = data.getString("group_id");
                    status = data.getString("status");
                } catch (JSONException e) {
                    return;
                }
            }
        });

    }


    private void leave() {
        mSocket.disconnect();
        mSocket.connect();
//        startSignIn();
    }

    public static void scrollToBottom() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMessagesView.scrollToPosition(mAdapter.getItemCount() - 1);
            }
        });
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isConnected) {
                        if (null != mUsername)
//                            mSocket.emit("adduser", mUsername, usergroup_name, CommonUtils.getTempFileName(activity, "id"));
//                        mSocket.emit("adduser", mUsername);
//                        Toast.makeText(getApplicationContext(),
//                                R.string.connect, Toast.LENGTH_LONG).show();
                        isConnected = true;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isConnected = false;
                    Toast.makeText(getApplicationContext(),
                            R.string.disconnect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            R.string.error_connect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    String message;
                    String room;
                    Log.v("data",data.toString());
                    try {
                        username = data.getString("username");
                        message = data.getString("message");
                        room = data.getString("room");
                    } catch (JSONException e) {
                        return;
                    }
                    removeTyping(username);
                    if (!username.equals(""))
                        addMessage(username, message);
                }
            });
        }
    };

    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        return;
                    }

                    addLog(getResources().getString(R.string.message_user_joined, username));
                    addParticipantsLog(numUsers);
                }
            });
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        return;
                    }

                    addLog(getResources().getString(R.string.message_user_left, username));
                    addParticipantsLog(numUsers);
                    removeTyping(username);
                }
            });
        }
    };

    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username, room;
                    try {
                        username = data.getString("username");
                        room = data.getString("room");
                    } catch (JSONException e) {
                        return;
                    }
//                    if(room.equals(usergroup))
                    addTyping(username);
                }
            });
        }
    };




    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username, room;
                    try {
                        username = data.getString("username");
                        room = data.getString("room");
                    } catch (JSONException e) {
                        return;
                    }
//                    if(room.equals(usergroup))
                    removeTyping(username);
                }
            });
        }
    };

    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            if (!mTyping) return;

            mTyping = false;
            mSocket.emit("stop typing");
        }
    };

    @Override
    public void onClick(View view, int position) {

    }

    public static void showDialog(){
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.show_message_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button back = (Button) dialog.findViewById(R.id.cancel);
        Button readonly = (Button) dialog.findViewById(R.id.readonly);
        back.setText("OK");
        readonly.setVisibility(View.GONE);
        TextView message = (TextView) dialog.findViewById(R.id.message);
        message.setText("Can not perform this action !!");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dialog.show();
    }
}

