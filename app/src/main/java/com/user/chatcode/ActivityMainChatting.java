package com.user.chatcode;

import android.annotation.TargetApi;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.ColorStateList;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.user.chatcode.adapter.HorizontalAdapter;
import com.user.chatcode.adapter.InboxAdapter;
import com.user.chatcode.adapter.MainChattingAdapter;
import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.chatting.ChatApplication;
import com.user.chatcode.model.ActiveModel;
import com.user.chatcode.model.ChattingListModel;
import com.user.chatcode.model.ChattingModel;
import com.user.chatcode.model.GroupObject;
import com.user.chatcode.model.InboxListModel;
import com.user.chatcode.qrscanner.QrcodeScreen;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.attr.data;
import static android.R.attr.supportsAssist;
import static android.R.attr.theme;
import static com.user.chatcode.ActivityChatting.activity;
import static com.user.chatcode.ActivityChatting.getNextMessages;
import static com.user.chatcode.ActivityChatting.horizontalAdapter;
import static com.user.chatcode.ActivityChatting.model;
import static com.user.chatcode.ActivityChatting.parent;
import static com.user.chatcode.ActivityChatting.progressBar;
import static com.user.chatcode.ActivityChatting.usergroup;
import static com.user.chatcode.chatting.ActivityBeforeMainChatting.mSocket;
import static com.user.chatcode.utils.Constants.time_type;

/**
 * Created by user on 22/12/16.
 */
public class ActivityMainChatting extends AppCompatActivity implements View.OnClickListener , SwipeRefreshLayout.OnRefreshListener{
private static SwipeRefreshLayout swipeRefreshLayout,swipeRefreshLayout1;
    LinearLayout id_codes,id_inbox,id_scan;
    TextView txt1,txt2,txt3;
    static ListView list,list1;
    static Boolean loading;
    static ProgressBar progress;
    public static String next_url="1";
    public static String next_url1="1";
    private static String mUsername;
    public  static MainChattingAdapter adapter;
  //    private static Socket mSocket;
    ImageView myaccount;
    static Context context;
static InboxAdapter adapter1;
    public static int tabSelected=1;

 public  static ArrayList<ChattingListModel> listdata;
    public  static ArrayList<InboxListModel> listdata1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_mainchatting);
         ChatApplication app = (ChatApplication) getApplication();
//         mSocket = app.getSocket();
//         mSocket.on("login", onLogin);
         context=this;
         swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swiperefresh);
         swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout1 = (SwipeRefreshLayout) findViewById(R.id.swiperefresh1);
        swipeRefreshLayout1.setOnRefreshListener(this);
         list=(ListView)findViewById(R.id.list);
        list1=(ListView)findViewById(R.id.list1);

        id_codes =(LinearLayout)findViewById(R.id.id_codes);
        id_inbox =(LinearLayout)findViewById(R.id.id_inbox);
         id_scan =(LinearLayout)findViewById(R.id.id_scan);
         progress=(ProgressBar)findViewById(R.id.progress);
        txt1=(TextView)findViewById(R.id.text1);
        txt2=(TextView)findViewById(R.id.text2);
         txt3=(TextView)findViewById(R.id.text3);
          findViewById(R.id.myaccount).setOnClickListener(this);
         findViewById(R.id.createchatcode).setOnClickListener(this);
         id_codes.setOnClickListener(this);
         id_inbox.setOnClickListener(this);
         id_scan.setOnClickListener(this);
        findViewById(R.id.button1).setOnClickListener(this);
        findViewById(R.id.button2).setOnClickListener(this);
         findViewById(R.id.button3).setOnClickListener(this);
//         listdata=get_group_user("", CommonUtils.getTempFileName(ActivityMainChatting.this,"id"),"0");
//

     }

     @Override public void onResume(){
         super.onResume();
         refreshList();
     }
//

//    public static void attemptLogin() {
//        mUsername = Constants.user.getName();
//        mSocket.emit("add user", mUsername);
//        mSocket.on("join-room", new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Object room = args[0];
//            }
//        });
//    }
//
//    private Emitter.Listener onLogin = new Emitter.Listener() {
//        @Override
//        public void call(Object... args) {
//            JSONObject data = (JSONObject) args[0];
//            mSocket.emit("create", "test room");
//            mSocket.on("updaterooms", new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Object room = args[0];
//
//                }
//            });
//            int numUsers;
//            try {
//                numUsers = data.getInt("numUsers");
//            } catch (JSONException e) {
//                return;
//            }
//
//            Intent intent = new Intent();
//            intent.putExtra("username", mUsername);
//            intent.putExtra("numUsers", numUsers);
//            setResult(RESULT_OK, intent);
//            finish();
//        }
//    };
@Override
public void onRefresh(){
         swipeRefreshLayout.setRefreshing(true);
         swipeRefreshLayout1.setRefreshing(true);
//    }
    refreshList();
}
   public  void refreshList(){
        //do processing to get new data and set your listview's adapter, maybe  reinitialise the loaders you may be using or so
        //when your data has finished loading, cset the refresh state of the view to false
//       if(tabSelected==1) {
           swipeRefreshLayout.setRefreshing(false);
           next_url="1";
           listdata = get_group_user("", CommonUtils.getTempFileName(ActivityMainChatting.this, "id"), "1");

//       }else {
           swipeRefreshLayout1.setRefreshing(false);
           listdata1 = get_inbox_user(CommonUtils.getTempFileName(ActivityMainChatting.this, "id"));
           if(null!=listdata1||null!=listdata)
               setSeleted(tabSelected);


    }

    public static  void refreshList1(Context activity){
        //do processing to get new data and set your listview's adapter, maybe  reinitialise the loaders you may be using or so
        //when your data has finished loading, cset the refresh state of the view to false
//       if(tabSelected==1) {
        swipeRefreshLayout.setRefreshing(false);
        next_url="1";
        listdata = get_group_user("", CommonUtils.getTempFileName(activity, "id"), "1");

//       }else {
        swipeRefreshLayout1.setRefreshing(false);
        listdata1 = get_inbox_user(CommonUtils.getTempFileName(activity, "id"));

    }


    @Override
    public void onClick(View view) {
   switch (view.getId()) {
       case R.id.id_codes:
           tabSelected=1;
           setSeleted(1);
           break;
       case R.id.id_inbox:
           tabSelected=2;
           setSeleted(2);
           break;
       case R.id.id_scan:
           tabSelected=1;
           setSeleted(3);
           break;
       case R.id.button1:
           setSeleted(1);
           break;
       case R.id.button2:
           setSeleted(2);
           break;
       case R.id.button3:
           setSeleted(3);
           break;
       case R.id.myaccount:
           Intent i = new Intent(ActivityMainChatting.this, ActivityMyProfile.class);
           startActivity(i);
           break;
       case R.id.createchatcode:
           Intent intent = new Intent(ActivityMainChatting.this, CreateChatecode1.class);
           startActivity(intent);
           break;
   }
    }

    public static ArrayList<ChattingListModel>  get_group_user(String url, String user_id, final String val) {
        progress.setVisibility(View.VISIBLE);
        if(null!=next_url&&next_url.equals("1")){
            listdata =new ArrayList<>();
            url=Constants.BASE_URL+Constants.GET_GROUP_URL;
        }
//        {"data":{"total":18,"per_page":6,"current_page":1,"last_page":3,"next_page_url":"http://54.200.70.163/chatcode/public/api/getallgroup?page=2","prev_page_url":null,"from":1,"to":6,"data":[{"id":189,"user_id":2,"name":"testing testing new","max_user":15,"reset_on":"","reset_time":"00:00:00","expiration":"2017-03-23 00:00:00","time_limit":"15","photos":"No","group_icon":"http://54.200.70.163/chatcode/public/groupicons/1490250904-1490250899664.jpg","group_code":"1490250899472","type":1,"created_at":"2017-03-23 06:35:05","updated_at":"2017-03-23 06:35:05","usercount":1},{"id":188,"user_id":2,"name":"abcd","max_user":15,"reset_on":"","reset_time":"00:00:00","expiration":"2017-03-23 00:00:00","time_limit":"15","photos":"No","group_icon":"http://54.200.70.163/chatcode/public/groupicons/1490250718-1490250713560.jpg","group_code":"1490250713374","type":1,"created_at":"2017-03-23 06:31:58","updated_at":"2017-03-23 06:31:58","usercount":1},{"id":118,"user_id":155,"name":"abc test group","max_user":15,"reset_on":"","reset_time":"00:00:00","expiration":"2017-03-31 00:00:00","time_limit":"15","photos":"No","group_icon":"http://54.200.70.163/chatcode/public/groupicons/1490076873-1490076883517.jpg","group_code":"1490076883442","type":1,"created_at":"2017-03-21 06:14:34","updated_at":"2017-03-21 06:14:34","usercount":3},{"id":117,"user_id":2,"name":"abc test","max_user":15,"reset_on":"","reset_time":"00:00:00","expiration":"2017-03-25 00:00:00","time_limit":"15","photos":"No","group_icon":"http://54.200.70.163/chatcode/public/groupicons/1490075229-1490075239436.jpg","group_code":"1490075239368","type":1,"created_at":"2017-03-21 05:47:10","updated_at":"2017-03-21 05:47:10","usercount":1},{"id":112,"user_id":2,"name":"hellos hello","max_user":15,"reset_on":"","reset_time":"00:00:00","expiration":"2017-03-31 00:00:00","time_limit":"15","photos":"No","group_icon":"http://54.200.70.163/chatcode/public/groupicons/1490012667-1490012663722.jpg","group_code":"1490012663551","type":1,"created_at":"2017-03-20 12:24:28","updated_at":"2017-03-20 12:24:28","usercount":5},{"id":111,"user_id":2,"name":"ABC tedt","max_user":7,"reset_on":"Monthly","reset_time":"11:44:00","expiration":"2017-03-26 00:00:00","time_limit":"30","photos":"No","group_icon":"http://54.200.70.163/chatcode/public/groupicons/1490011215-1490011211490.jpg","group_code":"1490011211021","type":1,"created_at":"2017-03-20 12:00:16","updated_at":"2017-03-20 12:00:16","usercount":1}]}}
//        {"message":"Success","Data":[{"id":1,"name":"user755533","email":"+18184918203@gmail.com","phone_number":"+18184918203","profile_image":"1487236296-ForbiddenFruitAlbum.jpg","device":"ios","device_id":"sdasfdfsdfsdf","active":1,"gcm_token":"","push_notification":1,"created_at":"2017-01-03 10:28:37","updated_at":"2017-02-16 09:11:36","firstgroup":[{"id":1,"user_id":1,"other_user_id":2,"name":"testing","created_at":"2017-04-11 08:59:36"}],"secondgroup":[]},{"id":150,"name":"user343235","email":"adyhiiii@jggu.com","profile_image":"1488972335-profile.jpg","device":"Android","device_id":"1635a2f5eb22497e","active":0,"gcm_token":"fVTIeo_MJAQ:APA91bGcDDbS47nvWRe7gE5MrGxcaqZVfewqrRFWP7Jt1Xtvlz_GyVKrnWGVLzFL9gYC873k_GucBRRm3euNVNJJ3w6QdZR-1CNgwKMypIr3rT8Jrj-h_ubw_V4Gb5834hozRi52njcz","push_notification":1,"created_at":"2017-03-08 11:25:14","updated_at":"2017-03-24 11:47:58","firstgroup":[],"secondgroup":[{"id":4,"user_id":2,"other_user_id":150,"name":"1491972790942"}]},{"id":159,"name":"user785181","email":"kekr4@hshs.com","profile_image":"1490101199-profile.jpg","device":"Android","device_id":"fc63992f378f4b86","active":0,"gcm_token":"dI7VoInoUA0:APA91bFe2CzT_XhADP5Z_67nU7tUr1Bs3jP-sxECarmTPTqopmdzyiuWyklAt6gGMAj1Ur_fYYhaw4QPSqTUIprk9iStXGxlZFZ0B5n95lg0dixHELS4b7xx5oapGZe6ByNRaBtHnLSZ","push_notification":1,"created_at":"2017-03-21 12:59:02","updated_at":"2017-04-10 12:43:11","firstgroup":[],"secondgroup":[{"id":2,"user_id":2,"other_user_id":159,"name":"1491972648952"}]}]}
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.get_group_user(url,user_id).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    if(response.code()==200) {
                        if(null!=response.body()) {
                            Object resObject = new Gson().toJson(response.body());
                            String resString = resObject.toString();
                            JSONObject obj = new JSONObject(resString);
                            JSONArray data1 = obj.getJSONObject("data").getJSONArray("data");
                            try {
                                next_url = obj.getJSONObject("data").getString("next_page_url");
                            }catch (Exception e){
                                next_url="";
                            }
                            String banner,id,end_date_time,group_icon,banner_updated_at,time_type,name,user_id,max_user,reset_on,reset_time,expiration,time_limit,photos,group_code,created_at,usercount;
                            for (int i = 0; i <data1.length(); i++) {
                                try {
                                    id = data1.getJSONObject(i).getString("id");
                                }catch (Exception e){
                                    id="";
                                }
                                try {
                                    group_icon = data1.getJSONObject(i).getString("group_icon");
                                }catch (Exception e){
                                    group_icon="";
                                }
                                try {
                                    name = data1.getJSONObject(i).getString("name");
                                }catch (Exception e){
                                    name="";
                                }
                                try {
                                    user_id = data1.getJSONObject(i).getString("user_id");
                                }catch (Exception e){
                                    user_id="";
                                }
                                try {
                                    max_user = data1.getJSONObject(i).getString("max_user");
                                }catch (Exception e){
                                    max_user="";
                                }
                                try {
                                    reset_on = data1.getJSONObject(i).getString("reset_on");
                                }catch (Exception e){
                                    reset_on="";
                                }
                                try {
                                    reset_time = data1.getJSONObject(i).getString("reset_time");
                                }catch (Exception e){
                                    reset_time="";
                                }
                                try {
                                    expiration = data1.getJSONObject(i).getString("expiration");
                                }catch (Exception e){
                                    expiration="";
                                }
                                try {
                                    time_limit = data1.getJSONObject(i).getString("time_limit");
                                }catch (Exception e){
                                    time_limit="";
                                }
                                try {
                                    time_type = data1.getJSONObject(i).getString("time_type");
                                }catch (Exception e){
                                    time_type="";
                                }
                                try {
                                    photos = data1.getJSONObject(i).getString("photos");
                                }catch (Exception e){
                                    photos="";
                                }
                                try {
                                    group_code = data1.getJSONObject(i).getString("group_code");
                                }catch (Exception e){
                                    group_code="";
                                }
                                try {
                                    banner = data1.getJSONObject(i).getString("banner");
                                }catch (Exception e){
                                    banner="";
                                }
                                try {
                                    created_at = data1.getJSONObject(i).getJSONArray("userinfo").getJSONObject(0).getString("updated_at");
                                }catch (Exception e){
                                    created_at="";
                                }
                                try {
                                    end_date_time = data1.getJSONObject(i).getJSONArray("userinfo").getJSONObject(0).getString("end_date_time");
                                }catch (Exception e){
                                    end_date_time="";
                                }
                                try {
                                    usercount = data1.getJSONObject(i).getString("usercount");
                                }catch (Exception e){
                                    usercount="";
                                }
                                try {
                                    banner_updated_at = data1.getJSONObject(i).getString("updated_at");
                                }catch (Exception e){
                                    banner_updated_at="";
                                }

                                 listdata.add(new ChattingListModel(
                                        id,
                                        group_icon,
                                        name,
                                        user_id,
                                        max_user,
                                        reset_on,
                                        reset_time,
                                        expiration,
                                        time_limit,
                                        time_type,
                                        photos,
                                        group_code,
                                        created_at,
                                        usercount,
                                        false,
                                         end_date_time,
                                         banner,
                                         banner_updated_at));
                            }
                            if(null!= ActivityMainChatting.adapter)
                                ActivityMainChatting.adapter.notifyDataSetChanged();
                        }
                        loading=false;
                    }else if(response.code()==500){
                    }

                } catch (Throwable e) {
                    onFailure(call, e);
                }
                progress.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
            }
        });
        return listdata;
    }


    public static ArrayList<InboxListModel>  get_inbox_user( String user_id) {
        listdata1=new ArrayList<>();
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.get_group_user("http://54.200.70.163/chatcode/public/api/getchatuser",user_id).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    if(response.code()==200) {
                        if(null!=response.body()) {
                            Object resObject = new Gson().toJson(response.body());
                            String resString = resObject.toString();
                            JSONObject obj = new JSONObject(resString);
//                            {"message":"Success","Data":[]}
                            JSONArray data1 = obj.getJSONArray("Data");
                            String id,name,email,phone_number,profile_image,device,device_id,active,gcm_token,push_notification,created_at,updated_at,lastmessage;
                            ArrayList<GroupObject> firstgroup,secondgroup;

                            for (int i = 0; i <data1.length(); i++) {
                                try {
                                    id = data1.getJSONObject(i).getString("id");
                                }catch (Exception e){
                                    id="";
                                }
                                try {
                                    name = data1.getJSONObject(i).getString("name");
                                }catch (Exception e){
                                    name="";
                                }
                                try {
                                    email = data1.getJSONObject(i).getString("email");
                                }catch (Exception e){
                                    email="";
                                }
                                try {
                                    phone_number = data1.getJSONObject(i).getString("phone_number");
                                }catch (Exception e){
                                    phone_number="";
                                }
                                 try {
                                     profile_image = data1.getJSONObject(i).getString("profile_image");
                                 }catch (Exception e){
                                     profile_image="";
                                 }
                                 try {
                                     device = data1.getJSONObject(i).getString("device");
                                 }catch (Exception e){
                                     device="";
                                 }
                                try {
                                    device_id = data1.getJSONObject(i).getString("device_id");
                                }catch (Exception e){
                                    device_id="";
                                }
                                try {
                                    active = data1.getJSONObject(i).getString("active");
                                }catch (Exception e){
                                    active="";
                                }
                                try {
                                    gcm_token = data1.getJSONObject(i).getString("gcm_token");
                                }catch (Exception e){
                                    gcm_token="";
                                }
                                try {
                                    push_notification = data1.getJSONObject(i).getString("push_notification");
                                }catch (Exception e){
                                    push_notification="";
                                }
                                 try {
                                     created_at = data1.getJSONObject(i).getString("created_at");
                                }catch (Exception e){
                                     created_at="";
                                }
                                try {
                                    updated_at = data1.getJSONObject(i).getString("updated_at");
                                }catch (Exception e){
                                    updated_at="";
                                }

                                try {
                                    lastmessage = data1.getJSONObject(i).getJSONObject("lastmessage").getString("message");
                                }catch (Exception e){
                                    lastmessage="";
                                }
                                firstgroup = new ArrayList<GroupObject>();
                                secondgroup = new ArrayList<GroupObject>();

                                try {
                                for(int j=0;j<data1.getJSONObject(i).getJSONArray("firstgroup").length();j++) {
                                    try {
                                        firstgroup.add(new GroupObject(
                                                data1.getJSONObject(i).getJSONArray("firstgroup").getJSONObject(j).getString("id"),
                                                data1.getJSONObject(i).getJSONArray("firstgroup").getJSONObject(j).getString("user_id"),
                                                data1.getJSONObject(i).getJSONArray("firstgroup").getJSONObject(j).getString("other_user_id"),
                                                data1.getJSONObject(i).getJSONArray("firstgroup").getJSONObject(j).getString("name")));
                                     } catch (Exception e) {
                                        Log.v("tag",e.getMessage());
                                     }

                                }
                                } catch (Exception e) {
                                        firstgroup = new ArrayList<GroupObject>();
                                    }
                                try {
                                    for(int j=0;j<data1.getJSONObject(i).getJSONArray("secondgroup").length();j++) {
                                        try {
                                            secondgroup.add(new GroupObject(
                                                    data1.getJSONObject(i).getJSONArray("secondgroup").getJSONObject(j).getString("id"),
                                                    data1.getJSONObject(i).getJSONArray("secondgroup").getJSONObject(j).getString("user_id"),
                                                    data1.getJSONObject(i).getJSONArray("secondgroup").getJSONObject(j).getString("other_user_id"),
                                                    data1.getJSONObject(i).getJSONArray("secondgroup").getJSONObject(j).getString("name")));
                                         } catch (Exception e) {
                                            Log.v("tag",e.getMessage());
                                        }
                                    }
                                } catch (Exception e) {
                                    firstgroup = new ArrayList<GroupObject>();
                                }


                                // "lastmessage":null,
                                 // "firstgroup":

                                     listdata1.add(new InboxListModel(
                                             id,
                                             name,
                                             email,
                                             phone_number,
                                             profile_image,
                                             device,
                                             device_id,
                                             active,
                                             gcm_token,
                                             push_notification,
                                             created_at,
                                             updated_at,
                                             lastmessage,
                                             firstgroup,
                                             secondgroup)
                                         );
                            }
                            if(null!=adapter)
                                adapter1.notifyDataSetChanged();
                            else {
                                adapter1 = new InboxAdapter(activity, listdata1, activity);
                                list1.setAdapter(adapter1);
                            }
                        }
                        loading=false;
                    }else if(response.code()==500){
                    }

                } catch (Throwable e) {
                    onFailure(call, e);
                }
                progress.setVisibility(View.GONE);
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
            }
        });
        return listdata1;
    }




    @Override
public void onBackPressed(){
        super.onBackPressed();
        finishAffinity();
        System.exit(0);
     }
     public void setSeleted(int selected){
        if(selected==1){

            swipeRefreshLayout.setVisibility(View.VISIBLE);
            swipeRefreshLayout1.setVisibility(View.GONE);
            adapter = new MainChattingAdapter(ActivityMainChatting.this,listdata, ActivityMainChatting.this);
            list.setAdapter(adapter);
            list.setOnScrollListener(new AbsListView.OnScrollListener() {
                @Override
                public void onScrollStateChanged(AbsListView view, int scrollState) {
                    if (scrollState == AbsListView.OnScrollListener.SCROLL_STATE_IDLE
                            && (list.getLastVisiblePosition() - list.getHeaderViewsCount() -
                            list.getFooterViewsCount()) >= (adapter.getCount() - 1)) {
                        if(null!=next_url&&!loading) {
                            loading=true;
                            get_group_user(next_url, CommonUtils.getTempFileName(ActivityMainChatting.this, "id"), "1");
                        }
                    }
                }

                @Override
                public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {
                }
            });
            mSocket.emit("getrooms", CommonUtils.getTempFileName(ActivityMainChatting.this, "id"),
                    CommonUtils.getTempFileName(ActivityMainChatting.this, "name"));
            findViewById(R.id.button1).setBackground(getResources().getDrawable(R.drawable.codes_active));
            txt1.setTextColor(getResources().getColor(R.color.black));
            findViewById(R.id.button2).setBackground(getResources().getDrawable(R.drawable.inbox_inactive));
            txt2.setTextColor(getResources().getColor(R.color.dialer_color));
            findViewById(R.id.button3).setBackground(getResources().getDrawable(R.drawable.scan_inactive));
            txt3.setTextColor(getResources().getColor(R.color.dialer_color));
        }else if(selected==2){
           swipeRefreshLayout1.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setVisibility(View.GONE);
            adapter1 = new InboxAdapter(ActivityMainChatting.this,listdata1, ActivityMainChatting.this);
            list1.setAdapter(adapter1);
            findViewById(R.id.button1).setBackground(getResources().getDrawable(R.drawable.codes_inactive));
            txt1.setTextColor(getResources().getColor(R.color.dialer_color));
            findViewById(R.id.button2).setBackground(getResources().getDrawable(R.drawable.inbox_active));
            txt2.setTextColor(getResources().getColor(R.color.black));
            findViewById(R.id.button3).setBackground(getResources().getDrawable(R.drawable.scan_inactive));
            txt3.setTextColor(getResources().getColor(R.color.dialer_color));
        }else{
            ActivityMainChatting.tabSelected=1;
            Intent i=new Intent(ActivityMainChatting.this, QrcodeScreen.class);
            startActivity(i);
            findViewById(R.id.button1).setBackground(getResources().getDrawable(R.drawable.codes_inactive));
            txt1.setTextColor(getResources().getColor(R.color.dialer_color));
            findViewById(R.id.button2).setBackground(getResources().getDrawable(R.drawable.inbox_inactive));
            txt2.setTextColor(getResources().getColor(R.color.dialer_color));
            findViewById(R.id.button3).setBackground(getResources().getDrawable(R.drawable.scan_active));
            txt3.setTextColor(getResources().getColor(R.color.black));
        }
    }

    public static void delete_room(final String group_id, final int position,View view) {
        final ProgressDialog progressBar=new ProgressDialog(context);
        progressBar.setMessage("Loading...");
        progressBar.show();
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.delet_chat(group_id).enqueue(new Callback<JsonElement>() {
                                                 @Override
                                                 public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                                                     try {
                                                         if(response.code()==200) {
                                                             progressBar.dismiss();
                                                             if(null!=response.body()) {
                                                                 Object resObject = new Gson().toJson(response.body());
                                                                 String resString = resObject.toString();
                                                                 JSONObject obj = new JSONObject(resString);
                                                                 if(obj.getString("message").equals("success"))
                                                                     listdata.remove(position);
                                                                 adapter.notifyDataSetChanged();

                                                                 if ((list.getLastVisiblePosition() - list.getHeaderViewsCount() -
                                                                         list.getFooterViewsCount()) >= (adapter.getCount() - 1)) {
                                                                     if(null!=next_url&&!loading) {
                                                                         loading=true;
                                                                         get_group_user(next_url, CommonUtils.getTempFileName(context, "id"), "1");
                                                                     }
                                                                 }

                                                              }
                                                         }else if(response.code()==500){
                                                             progressBar.dismiss();
                                                         }

                                                     } catch (Throwable e) {
                                                         progressBar.dismiss();
                                                         onFailure(call, e);
                                                     }
                                                 }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                progressBar.dismiss();
            }
        });
//        view.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent intent = new Intent(context,ActivityChatting.class);
//                intent.putExtra("group", group_id);
//                intent.putExtra("position",position);
//                context.startActivity( intent);
//            }
//        });
    }

//    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
//    public void setSeleted(int selected){
//        if(selected==1){
//            MainChattingAdapter adapter=new MainChattingAdapter(getDummyData1(),ActivityMainChatting.this);
//            list.setAdapter(adapter);
//            id_codes.setBackgroundColor(getResources().getColor(R.color.colorAccent));
//            id_inbox.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            findViewById(R.id.button1).setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
//            txt1.setTextColor(getResources().getColor(R.color.colorPrimary));
//            findViewById(R.id.button2).setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
//            txt2.setTextColor(getResources().getColor(R.color.colorAccent));
//        }else{
//            InboxAdapter adapter=new InboxAdapter(getDummyData2(),ActivityMainChatting.this);
//            list.setAdapter(adapter);
//            id_inbox.setBackgroundColor(getResources().getColor(R.color.colorAccent));
//            id_codes.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
//            findViewById(R.id.button2).setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorPrimary)));
//            txt2.setTextColor(getResources().getColor(R.color.colorPrimary));
//            findViewById(R.id.button1).setBackgroundTintList(ColorStateList.valueOf(getResources().getColor(R.color.colorAccent)));
//            txt1.setTextColor(getResources().getColor(R.color.colorAccent));
//        }
//    }
//    public  ArrayList<ChattingListModel> getDummyData1(){
//        final ArrayList<ChattingListModel> data =new ArrayList<>();
//        mSocket.emit("getrooms", Constants.user.getName());
//        mSocket.on("updaterooms", new Emitter.Listener() {
//            @TargetApi(Build.VERSION_CODES.KITKAT)
//            @Override
//            public void call(Object... args) {
//                try {
//                    JSONArray room1 = new JSONArray(args);
//                    JSONArray room = room1.getJSONArray(0);
//                    for(int i=0;i<room.length();i++){
//                        data.add(new ChattingListModel(room.getJSONObject(i).getString("name"),""+i,false));
//                    }
//                }catch (Exception e){
//                    Log.v("exception",e.getMessage());
//                }
//            }
//        });
//        return data;
//    }




//    public  ArrayList<InboxListModel> getDummyData2(){
//        ArrayList<InboxListModel> data =new ArrayList<>();
//        data.add(new InboxListModel("Username1"));
//        data.add(new InboxListModel("Username2"));
//        data.add(new InboxListModel("Username3"));
//        data.add(new InboxListModel("Username4"));
//        data.add(new InboxListModel("Username5"));
//        data.add(new InboxListModel("Username6"));
//        data.add(new InboxListModel("Username7"));
//        data.add(new InboxListModel("Username8"));
//        data.add(new InboxListModel("Username9"));
//        data.add(new InboxListModel("Username10"));
//        data.add(new InboxListModel("Username11"));
//
//        return data;
//    }
}
