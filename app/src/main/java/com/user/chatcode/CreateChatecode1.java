package com.user.chatcode;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.model.UploadModel;
import com.user.chatcode.util.IabBroadcastReceiver;
import com.user.chatcode.util.IabHelper;
import com.user.chatcode.util.IabResult;
import com.user.chatcode.util.Inventory;
import com.user.chatcode.util.Purchase;
import com.user.chatcode.utils.AlarmReceiver;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.user.chatcode.chatting.ActivityBeforeMainChatting.mSocket;
import static com.user.chatcode.utils.Constants.time_type;

/**
 * Created by user on 21/4/17.
 */

public class CreateChatecode1 extends AppCompatActivity implements  View.OnClickListener ,IabBroadcastReceiver.IabBroadcastListener {
    Switch switch_maxusers, switch_reset, switch_expiration, switch_timelimit, switch_photos, switch_adminonly, switch_banner;
    int pMonth, pYear, pDay;
    TextView picker_maxusers;
    File file=null;
    static TextView picker_reset;
    static String banner_icon="";
    public static TextView picker_expiration;
    EditText name;
    ScrollView parent;
    static TextView picker_limit;
    LinearLayout admin;
    Button done;
    String[] reset = {"Daily", "Weekly", "Monthly", "Annually"};
//    String[] timeLimit = {"Year", "Month", "Days", "Hours", "Minutes"};
String[] timeLimit = {"10 min", "20 min", "30 min", "60 min", "90 min","120 min","150 min"};
    Dialog dialog;
    int newVal = 1, resetType = 0, limit = 0;
    String reset_date;
    NumberPicker np;
    IabHelper iabHelper;
    private IabBroadcastReceiver iabBroadcastReceiver;
    private IabHelper.QueryInventoryFinishedListener gotInventoryListener;
    private IabHelper.OnIabPurchaseFinishedListener purchaseFinishedListener;
    private static final String TAG = "In_app";
    private boolean gotRandomizer = false;
    boolean verifyDeveloperPayload(Purchase p) {
        String payload = p.getDeveloperPayload();

/*
 * TODO: verify that the developer payload of the purchase is correct. It will be
 * the same one that you sent when initiating the purchase.
 *
 * WARNING: Locally generating a random string when starting a purchase and
 * verifying it here might seem like a good approach, but this will fail in the
 * case where the user purchases an item on one device and then uses your app on
 * a different device, because on the other device you will not have access to the
 * random string you originally generated.
 *
 * So a good developer payload has these characteristics:
 *
 * 1. If two different users purchase an item, the payload is different between them,
 *    so that one user's purchase can't be replayed to another user.
 *
 * 2. The payload must be such that you can verify it even when the app wasn't the
 *    one who initiated the purchase flow (so that items purchased by the user on
 *    one device work on other devices owned by the user).
 *
 * Using your own server to store and verify developer payloads across app
 * installations is recommended.
 */

        return true;
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSocket.connect();
        setContentView(R.layout.activity_creatchatcode);
        parent = (ScrollView) findViewById(R.id.parent);
        switch_maxusers = (Switch) findViewById(R.id.switch_maxusers);
        switch_reset = (Switch) findViewById(R.id.switch_reset);
        switch_expiration = (Switch) findViewById(R.id.switch_expiration);
        switch_timelimit = (Switch) findViewById(R.id.switch_timelimit);
        switch_photos = (Switch) findViewById(R.id.switch_photos);
        switch_adminonly = (Switch) findViewById(R.id.switch_adminonly);
        switch_banner = (Switch) findViewById(R.id.switch_banner);
        name = (EditText) findViewById(R.id.name);
        picker_maxusers = (TextView) findViewById(R.id.picker_maxusers);
        picker_reset = (TextView) findViewById(R.id.picker_reset);
        picker_expiration = (TextView) findViewById(R.id.picker_expiration);
        picker_limit = (TextView) findViewById(R.id.picker_limit);
        admin = (LinearLayout) findViewById(R.id.admin);
        findViewById(R.id.create).setOnClickListener(this);
        checkChange(switch_maxusers, picker_maxusers);
        checkChange(switch_reset, picker_reset);
        checkChange(switch_expiration, picker_expiration);
        checkChange(switch_timelimit, picker_limit);
        checkChange(switch_photos, admin);
        picker_maxusers.setOnClickListener(this);
        final Calendar cal = Calendar.getInstance();
        pYear = cal.get(Calendar.YEAR);
        pMonth = cal.get(Calendar.MONTH);
        pDay = cal.get(Calendar.DAY_OF_MONTH);
        picker_reset.setOnClickListener(this);
        picker_limit.setOnClickListener(this);
        picker_expiration.setText(CommonUtils.getCurrentDate());
        picker_expiration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(0);
            }
        });
        String base64Data = getString(R.string.licence_key);

        iabHelper = new IabHelper(CreateChatecode1.this, base64Data);


        gotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                if (iabHelper == null) return;

                if (result.isFailure()) {
                    Log.e(TAG, "Failed to query inventory: " + result);
                    return;
                }

                Purchase purchase = inv.getPurchase("redeem_product_listen");

                gotRandomizer = purchase != null; //TODO add verifyDeveloperPayload(Purchase p)

                Log.e(TAG, "Randomizer is: " + gotRandomizer);
            }
        };
        purchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                if (iabHelper == null) return;

                if (result.isFailure()) {
                    Log.e(TAG, "Error purchasing: " + result);
                    return;
                }

                if (info.getSku().equals("next")) {
                    CommonUtils.Image_Picker_Dialog(CreateChatecode1.this);
                    Log.e(TAG, "Purchased: " + info.getSku());

                    gotRandomizer = true;
                }
            }
        };


//        iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
//            public void onIabSetupFinished(IabResult result) {
//                if (!result.isSuccess()) {
//                    // Oh no, there was a problem.
//                    Log.d("TAG", "Problem setting up In-app Billing: " + result);
//                    Log.i("TAG", "ERROR");
//                }
//                // Hooray, IAB is fully set up!
//                //check owned items & consum em
//                checkOwnedItems();
//                //make a test purchase
//                makePurchase();
//            }
//        });

        iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.e(TAG, "Problem setting up In-app Billing: " + result);
                }
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);

                iabBroadcastReceiver = new IabBroadcastReceiver(CreateChatecode1.this);
                registerReceiver(iabBroadcastReceiver, broadcastFilter);

                Log.d(TAG, "Setup successful. Querying inventory.");
                try {
                    iabHelper.queryInventoryAsync(gotInventoryListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    Log.e(TAG, "Error querying inventory. Another async operation in progress.");
                }
            }
        });


        switch_banner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
//                CommonUtils.Image_Picker_Dialog(CreateChatecode1.this);

                if (b) {
                    if (!gotRandomizer) {
                        try {
                            iabHelper.launchPurchaseFlow(CreateChatecode1.this, "next", 101, purchaseFinishedListener, "");
                        } catch (IabHelper.IabAsyncInProgressException e) {
                            e.printStackTrace();
                            Log.e(TAG, "Error launching purchase flow. Another async operation in progress.");
                        }

                    }
                } else {
//                    pushNotification("0");
                }
            }
        });

    }
    private void makePurchase() {
        try {
            iabHelper.launchPurchaseFlow(this, "next", 10001,
                    mPurchaseFinishedListener, "bGoa+V7g/yqDXvKRqq+JTFn4uQZbPiQJo4pf9RzJ");
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
//            showToast("oh no error purchase" + String.valueOf(e));
        }
    }

    private void checkOwnedItems() {
        try {
            iabHelper.queryInventoryAsync(mGotInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
//            showToast("Oh no error in check()");
            //complain("Error querying inventory. Another async operation in progress.");
        }
    }
    IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Purchase item = inventory.getPurchase("next");
            if (item != null && verifyDeveloperPayload(item)) {
                //Log.d("TAG", "We have gas. Consuming it.");
                try {
                    iabHelper.consumeAsync(inventory.getPurchase("next"), mConsumeFinishedListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    // complain("Error consuming gas. Another async operation in progress.");
//                    iabHelperlper("oh no error when consuming");
                }
                return;
            }
        }

    };

    IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
        public void onIabPurchaseFinished(IabResult result, Purchase purchase) {
            Log.d("TAG", "Purchase finished: " + result + ", purchase: " + purchase);

            // if we were disposed of in the meantime, quit.
            if (iabHelper == null) return;

            if (result.isFailure()) {
                // complain("Error purchasing: " + result);
                return;
            }
            if (!verifyDeveloperPayload(purchase)) {
                // complain("Error purchasing. Authenticity verification failed.");
                return;
            }

            Log.d("TAG", "Purchase successful.");

            if (purchase.getSku().equals("next")) {

                Log.d("TAG", "Purchase is gas. Starting gas consumption.");
                try {
                    iabHelper.consumeAsync(purchase, mConsumeFinishedListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    //complain("Error consuming gas. Another async operation in progress.");
//                    showToast("oh no error when consuming");
                    return;
                }
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (iabHelper != null) try {
            iabHelper.dispose();
        } catch (IabHelper.IabAsyncInProgressException e) {
            e.printStackTrace();
        }
        iabHelper = null;
    }
    // Called when consumption is complete
    IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
        public void onConsumeFinished(Purchase purchase, IabResult result) {
            Log.d("TAG", "Consumption finished. Purchase: " + purchase + ", result: " + result);

            // if we were disposed of in the meantime, quit.
            if (iabHelper == null) return;

            // We know this is the "gas" sku because it's the only one we consume,
            // so we don't check which sku was consumed. If you have more than one
            // sku, you probably should check...
            if (result.isSuccess()) {
                // successfully consumed, so we apply the effects of the item in our
                // game world's logic, which in our case means filling the gas tank a bit
                //Log.d(TAG, "Consumption successful. Provisioning.");
                //mTank = mTank == TANK_MAX ? TANK_MAX : mTank + 1;
                // saveData();
                // alert("You filled 1/4 tank. Your tank is now " + String.valueOf(mTank) + "/4 full!");
            }
            else {
                //complain("Error while consuming: " + result);
            }
            Log.d("TAG", "End consumption flow.");
        }
    };



    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bitmap bitmap = null;
            if (requestCode == 01) {
                Uri uri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == 11) {
                bitmap = (Bitmap) data.getExtras().get("data");
            }
            Bitmap bmap = CommonUtils.resizeImage(null, bitmap);
            file = CommonUtils.createDirectoryAndSaveFile(bmap, "image.jpg", CreateChatecode1.this);
            if(null!=file)
                submitDetails(file);
        }

    }

    @Override
    public void receivedBroadcast() {
        Log.e(TAG, "Received broadcast notification. Querying inventory.");
        try {
            iabHelper.queryInventoryAsync(gotInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            Log.d(TAG, "Error querying inventory. Another async operation in progress.");
        }
    }
    public void checkChange(final Switch switchButton, final View view){
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(switchButton.isChecked())
                    view.setVisibility(View.VISIBLE);
                else
                    view.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                        CommonUtils.pDateSetListener,
                        pYear, pMonth, pDay);
            case 1:
                return new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                        pDateSetListener1,
                        pYear, pMonth, pDay);
        }
        return null;
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.picker_reset:
                showResetPickerDialog();
                break;
            case R.id.picker_limit:
                showTimeLimitPickerDialog();
                break;
            case R.id.picker_maxusers:
                showNumberPickerDialog();
                break;
            case R.id.done:
                Constants.max_capacity=String.valueOf(newVal);
                picker_maxusers.setText(String.valueOf(newVal));
                dialog.dismiss();
                break;
            case R.id.create:
//                mSocket.emit("create", name);
//                mSocket.emit("new message", "message");
//                mSocket.on("updaterooms", new Emitter.Listener() {
//                    @Override
//                    public void call(Object... args) {
//                        Object room = args[0];
//                        Log.v("room",room.toString());
//                    }
//                });
//                 if(Constants.expiration.equals("")) {
//                     int mnt=pMonth;
//                     Constants.expiration = pYear + "-" + (mnt + 1) + "-" + pDay + " 23:59:00";// Month is 0 based so add 1
//                 }
//                Constants.expiration=CommonUtils.getCurrentDate();


                if(Constants.time_type.equals("")) {
                    Constants.time_type = "Minutes";
                }
                if(Constants.time_limit.equals(""))
                    Constants.time_limit="1000";
                if(Constants.max_capacity.equals(""))
                    Constants.max_capacity="15";
                if(switch_photos.isChecked())
                    Constants.photos="Yes";
                else
                    Constants.photos="No";
                if(Constants.expiration.equals("")){
                    Constants.expiration="9999-01-01 00:00:00";// Month is 0 based so add 1
                }
                Constants.name=name.getText().toString();
                if(!Constants.name.equals("")&&!Constants.expiration.equals("")){
//                        &&!Constants.expires.equals("")
//                        &&!Constants.expires.equals("")
//                        &&!Constants.photos.equals("")
//                        &&!Constants.max_capacity.equals("")
//                        &&!Constants.reset.equals("")
//                        &&!Constants.time_limit.equals("")
//                        &&!Constants.expiration.equals("")) {
                    Intent i = new Intent(CreateChatecode1.this, ActivityCreateSuccess.class);
                    startActivity(i);
                    finish();
                }else if(Constants.name.equals("")){
                    CommonUtils.getSnackbar(parent,"Please enter group name");
                }
                break;
        }
    }

    private void showNumberPickerDialogLimit(int type) {
        dialog=new Dialog(CreateChatecode1.this);
        dialog.setContentView(R.layout.dialog_enter_number_picker);
        dialog.setCancelable(false);
        final EditText edittext  = (EditText)dialog.findViewById(R.id.edittext);
        done               = (Button)dialog. findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edittext.getText().toString().equals("")){
                    dialog.dismiss();
                    Constants.time_limit=edittext.getText().toString();
                }
            }
        });

//         if(type==0) {
//            np.setMinValue(1); //from array first value
//            np.setMaxValue(10); //to array last value
//        }else   if(type==1) {
//            np.setMinValue(1); //from array first value
//            np.setMaxValue(12); //to array last value
//        }else  if(type==2) {
//            np.setMinValue(1); //from array first value
//            np.setMaxValue(30); //to array last value
//        }else  if(type==3) {
//            np.setMinValue(1); //from array first value
//            np.setMaxValue(24); //to array last value
//        }else  if(type==4) {
//            np.setMinValue(1); //from array first value
//            np.setMaxValue(60); //to array last value
//        }
//            np.setWrapSelectorWheel(true);
//        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
//            @Override
//            public void onValueChange(NumberPicker picker, int oldVal, int newVal1){
//                newVal=newVal1;
//            }
//        });
        dialog.show();

    }



    private void showNumberPickerDialog() {
        dialog=new Dialog(CreateChatecode1.this);
        dialog.setContentView(R.layout.dialog_number_picker);
        dialog.setCancelable(false);
        np                 = (NumberPicker)dialog.findViewById(R.id.np);
        done               = (Button)dialog. findViewById(R.id.done);
        done.setOnClickListener(this);
        np.setMinValue(1); //from array first value
        np.setMaxValue(10000); //to array last value
        np.setWrapSelectorWheel(true);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal1){
                newVal=newVal1;
            }
        });
        dialog.show();

    }
    private void showResetPickerDialog() {
        dialog=new Dialog(CreateChatecode1.this);
        dialog.setContentView(R.layout.dialog_number_picker);
        dialog.setCancelable(false);
        np                 = (NumberPicker)dialog.findViewById(R.id.np);
        done               = (Button)dialog. findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Constants.reset0n=reset[resetType];
                dialog.dismiss();
                Constants.reset="00:00:00";
                Constants.reset0n=reset[resetType];
//                openTimePickerDialog(true);
            }
        });
        np.setDisplayedValues(reset);
        np.setMinValue(0); //from array first value
        np.setMaxValue(3);
        np.setWrapSelectorWheel(true);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal1){
                resetType=newVal1;
                picker_reset.setText(reset[resetType]+" , 12:00 am");
            }
        });

        dialog.show();

    }

    private void showTimeLimitPickerDialog() {
        dialog=new Dialog(CreateChatecode1.this);
        dialog.setContentView(R.layout.dialog_number_picker);
        dialog.setCancelable(false);
        np                 = (NumberPicker)dialog.findViewById(R.id.np);
        done               = (Button)dialog. findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

                Constants.time_limit=timeLimit[limit].replace(" min","");
                time_type="Minutes";
                picker_limit.setText(timeLimit[limit]);
//                time_type=timeLimit[limit];
//                showNumberPickerDialogLimit(limit);
//                picker_limit.setText(time_type);
            }
        });
        np.setDisplayedValues(timeLimit);
        np.setMinValue(0); //from array first value
        np.setMaxValue(timeLimit.length-1);
        np.setWrapSelectorWheel(true);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal1){
                limit=newVal1;
            }
        });
        dialog.show();

    }

    private void openTimePickerDialog(boolean is24r) {
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(CreateChatecode1.this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                onTimeSetListener, calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), is24r);
        timePickerDialog.show();
    }

    TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar calNow = Calendar.getInstance();
            Calendar calSet = (Calendar) calNow.clone();
//            if(resetType==1)
//            calSet.set(Calendar.DAY_OF_WEEK, hourOfDay);
//            else if(resetType==2)
//                calSet.set(Calendar.DAY_OF_MONTH, reset_date);
//            else if(resetType==3)
//                calSet.set(Calendar.DAY_OF_YEAR, hourOfDay);
            calSet.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calSet.set(Calendar.MINUTE, minute);
            calSet.set(Calendar.SECOND, 0);
            calSet.set(Calendar.MILLISECOND, 0);

            if (calSet.compareTo(calNow) <= 0) {
                // Today Set time passed, count to tomorrow
                calSet.add(Calendar.DATE, 1);
            }

            setAlarm(calSet);
        }
    };

    private void setAlarm(Calendar targetCal) {
        reset_date=(CommonUtils.printDifference_(new StringBuilder()
                // Month is 0 based so add 1
                .append(pYear).append("-")
                .append(pMonth + 1).append("-")
                .append(pDay).append("").toString()));

//        picker_reset.setText(reset[resetType]+","+reset_date);

        String hour= String.valueOf(targetCal.getTime().getHours());
        String min = String.valueOf(targetCal.getTime().getMinutes());
        String sec= String.valueOf(targetCal.getTime().getSeconds());
        if(hour.equals("0")){
            hour="00";
        }if(min.equals("0")){
            min="00";
        }if(sec.equals("0")){
            sec="00";
        }
        Constants.reset=hour+":"+min+":"+sec;
        Constants.reset0n=reset[resetType];
        picker_reset.setText(reset[resetType]+" ,"+(hour+":"+
                min+":"+
                sec));
//        picker_reset.setText(reset[resetType]+","+reset_date);
        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getBaseContext(), 1, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(),
                pendingIntent);

    }

    public  DatePickerDialog.OnDateSetListener pDateSetListener1=
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    pYear   = year;
                    pMonth  = monthOfYear;
                    pDay    = dayOfMonth;
                    updateDisplay1();
                }

            };

    private  void updateDisplay1() {
        reset_date=(CommonUtils.printDifference_(new StringBuilder()
                // Month is 0 based so add 1
                .append(pYear).append("-")
                .append(pMonth + 1).append("-")
                .append(pDay).append("").toString()));

    }
    void submitDetails(File _image) {
        final ProgressDialog progressDialog=CommonUtils.setProgress("Uploading  !!!",this);
        UserApi userApi     = ApiFactory.createServiceForGson();
        String fileName = "";
        RequestBody requestBody= null;
        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        if(null!=_image) {
            String image_name = _image.getName();
            fileName = "group_icon\"; filename=\"" + image_name;
            requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), _image);
            requestBodyMap.put(fileName, requestBody);
        }
//        1492150598380
        userApi.uploadQRInfo(requestBodyMap).enqueue(new Callback<UploadModel>() {
            @Override
            public void onResponse(Call<UploadModel> call, Response<UploadModel> response) {
                if (response.body() != null) {
                    try {
                        if (response.body().getMessage().equals("success")) {
                            CommonUtils.addSnackbar(parent, "Succesfully uploaded");
                            banner_icon=response.body().getNew_image();
                        } else {
                            CommonUtils.addSnackbar(parent, "Unable to upload");
                        }
                    }catch (Exception e){
                        CommonUtils.addSnackbar(parent, "Unable to upload");
                    }
                }
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<UploadModel> call, Throwable t) {
                CommonUtils.addSnackbar(parent, t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

}

