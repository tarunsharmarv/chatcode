package com.user.chatcode;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.user.chatcode.utils.CommonUtils;

/**
 * Created by user on 22/12/16.
 */
public class ActivityAccount extends AppCompatActivity {
    TextView phone,email;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_account);
        phone=(TextView)findViewById(R.id.phone);
        email=(TextView)findViewById(R.id.email);
        phone.setText(CommonUtils.getTempFileName(ActivityAccount.this,"phone_number"));
        email.setText(CommonUtils.getTempFileName(ActivityAccount.this,"email"));
        findViewById(R.id.back).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

}

