package com.user.chatcode.apis;


import com.google.gson.JsonElement;
import com.user.chatcode.model.ConfirmationClass;
import com.user.chatcode.model.LoginModel;
import com.user.chatcode.model.MessagesModel;
import com.user.chatcode.model.UploadModel;
import com.user.chatcode.utils.Constants;

import java.util.Map;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.PartMap;
import retrofit2.http.Url;

import static com.user.chatcode.utils.Constants.UPDATE_TOKEN_URL;

public interface UserApi {

    @FormUrlEncoded
    @POST(Constants.SEND_VERIFICATION_URL)
    Call<LoginModel> send_verification_phone(
            @Field("phone_number") String phone_number);

    @FormUrlEncoded
    @POST(Constants.SEND_VERIFICATION_URL)
    Call<LoginModel> send_verification_email(
            @Field("email") String email);

    @FormUrlEncoded
    @POST(Constants.GETALLUSER_DETAILS_URL)
    Call<JsonElement> get_all_user(
//            @Url String url,
            @Field("group_id") String group_id,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST()
    Call<JsonElement> get_inbox_user(
            @Url String url,
            @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST()
    Call<JsonElement> get_group_user(
            @Url String url,
            @Field("user_id") String user_id);


    @FormUrlEncoded
    @POST(Constants.AFTER_VERIFICATION_URL)
    Call<ConfirmationClass> after_verification_phone(
            @Field("phone_number") String phone_number,
            @Field("device") String device,
            @Field("device_id") String device_id);

    @FormUrlEncoded
    @POST(Constants.AFTER_VERIFICATION_URL)
    Call<ConfirmationClass> after_verification_email(
            @Field("email" ) String email,
            @Field("device") String device,
            @Field("device_id") String device_id);


    @Multipart
    @POST(Constants.UPDATE_PROFILE_URL)
    Call<UploadModel> update_profile(@PartMap Map<String, RequestBody> params);

    @FormUrlEncoded
    @POST()
    Call<MessagesModel> next_page_url(
            @Url String url,
            @Field("group_id" ) String group_id,
            @Field("user_id" ) String user_id);



    @FormUrlEncoded
    @POST()
    Call<MessagesModel> next_page_url_(
            @Url String url,
            @Field("user_id" ) String user_id,
            @Field("other_user_id" ) String other_user_id);


    @Multipart
    @POST(Constants.GALLERY_DETAILS_URL)
    Call<UploadModel> uploadQRInfo(@PartMap Map<String, RequestBody> params);

    @Multipart
    @POST(Constants.UPDATE_BANNER_URL_)
    Call<UploadModel> uploadBanner(@PartMap Map<String, RequestBody> params);

    @FormUrlEncoded
    @POST(Constants.UPDATE_TOKEN_URL)
    Call<JsonElement> updateToken(
            @Field("gcm_token") String gcm_token,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.PUSH_NOTI_URL)
    Call<JsonElement> pushNotification(
            @Field("push_notification") String push_notification,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.PUSH_NOTI_URL)
    Call<JsonElement> pushNotificationInbox(
            @Field("inbox_push_notification") String push_notification,
            @Field("user_id") String user_id);



    @FormUrlEncoded
    @POST(Constants.JOIN_CHAT_URL)
    Call<JsonElement> join_chat(
            @Field("group_code") String group_code,
            @Field("user_id") String user_id);

    @FormUrlEncoded
    @POST(Constants.READ_ONLY_URL)
    Call<JsonElement> read_chat(
            @Field("group_code") String group_code,
            @Field("user_id") String user_id);



    @FormUrlEncoded
    @POST(Constants.DELETE_CHAT_URL)
    Call<JsonElement> delet_chat(
            @Field("group_id" ) String group_id);


    @FormUrlEncoded
    @POST()
    Call<JsonElement> next_inbox_message_url(
            @Url String url,
            @Field("group_id" ) String group_id);

}


