package com.user.chatcode.apis;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.user.chatcode.utils.Constants;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
 import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public final class ApiFactory {

    private ApiFactory() {
    }


    private static Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(Constants.BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build();


    public static <S> S createService(Class<S> sClass) {
        return retrofit.create(sClass);
    }
    public static UserApi createServiceForGson() {
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().connectTimeout(20000, TimeUnit.MINUTES).addInterceptor(interceptor).build();
        retrofit = new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        return retrofit.create(UserApi.class);
    }
}
