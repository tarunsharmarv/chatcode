package com.user.chatcode;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.model.UploadModel;
import com.user.chatcode.util.IabBroadcastReceiver;
import com.user.chatcode.util.IabHelper;
import com.user.chatcode.util.IabResult;
import com.user.chatcode.util.Inventory;
import com.user.chatcode.util.Purchase;
import com.user.chatcode.utils.AlarmReceiver;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.user.chatcode.R.id.profile_image;
import static com.user.chatcode.R.id.switch_chat;
import static com.user.chatcode.chatting.ActivityBeforeMainChatting.mSocket;
import static com.user.chatcode.utils.Constants.max_capacity;
import static com.user.chatcode.utils.Constants.time_type;

/**
 * Created by user on 22/12/16.
 */

public class ActivityCreateChatcode extends AppCompatActivity implements  View.OnClickListener ,IabBroadcastReceiver.IabBroadcastListener {
    Switch switch_maxusers, switch_reset, switch_expiration, switch_timelimit, switch_photos, switch_adminonly, switch_banner;
    int pMonth, pYear, pDay;
    TextView picker_maxusers;
    File file=null;
    static TextView picker_reset;
    static String banner_icon="";
    public static TextView picker_expiration;
    EditText name;
    ScrollView parent;
    static TextView picker_limit;
    LinearLayout admin;
    Button done;
    String[] reset = {"Daily", "Weekly", "Monthly", "Annually"};
    String[] timeLimit = {"Year", "Month", "Days", "Hours", "Minutes"};
    Dialog dialog;
    int newVal = 1, resetType = 0, limit = 0;
    String reset_date;
    NumberPicker np;
    IabHelper iabHelper;
    private IabBroadcastReceiver iabBroadcastReceiver;
    private IabHelper.QueryInventoryFinishedListener gotInventoryListener;
    private IabHelper.OnIabPurchaseFinishedListener purchaseFinishedListener;
    private static final String TAG = "In_app";
    private boolean gotRandomizer = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mSocket.connect();
        setContentView(R.layout.activity_creatchatcode);
        parent = (ScrollView) findViewById(R.id.parent);
        switch_maxusers = (Switch) findViewById(R.id.switch_maxusers);
        switch_reset = (Switch) findViewById(R.id.switch_reset);
        switch_expiration = (Switch) findViewById(R.id.switch_expiration);
        switch_timelimit = (Switch) findViewById(R.id.switch_timelimit);
        switch_photos = (Switch) findViewById(R.id.switch_photos);
        switch_adminonly = (Switch) findViewById(R.id.switch_adminonly);
        switch_banner = (Switch) findViewById(R.id.switch_banner);
        name = (EditText) findViewById(R.id.name);
        picker_maxusers = (TextView) findViewById(R.id.picker_maxusers);
        picker_reset = (TextView) findViewById(R.id.picker_reset);
        picker_expiration = (TextView) findViewById(R.id.picker_expiration);
        picker_limit = (TextView) findViewById(R.id.picker_limit);
        admin = (LinearLayout) findViewById(R.id.admin);
        findViewById(R.id.create).setOnClickListener(this);
        checkChange(switch_maxusers, picker_maxusers);
        checkChange(switch_reset, picker_reset);
        checkChange(switch_expiration, picker_expiration);
        checkChange(switch_timelimit, picker_limit);
        checkChange(switch_photos, admin);
        picker_maxusers.setOnClickListener(this);
        final Calendar cal = Calendar.getInstance();
        pYear = cal.get(Calendar.YEAR);
        pMonth = cal.get(Calendar.MONTH);
        pDay = cal.get(Calendar.DAY_OF_MONTH);
        picker_reset.setOnClickListener(this);
        picker_limit.setOnClickListener(this);
        picker_expiration.setText(CommonUtils.getCurrentDate());
        picker_expiration.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showDialog(0);
            }
        });
        String base64Data = getString(R.string.licence_key);

        iabHelper = new IabHelper(ActivityCreateChatcode.this, base64Data);


        gotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                if (iabHelper == null) return;

                if (result.isFailure()) {
                    Log.e(TAG, "Failed to query inventory: " + result);
                    return;
                }

                Purchase purchase = inv.getPurchase("redeem_product_listen");

                gotRandomizer = purchase != null; //TODO add verifyDeveloperPayload(Purchase p)

                Log.e(TAG, "Randomizer is: " + gotRandomizer);
            }
        };
        purchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                if (iabHelper == null) return;

                if (result.isFailure()) {
                    Log.e(TAG, "Error purchasing: " + result);
                    return;
                }

                if (info.getSku().equals("next")) {
                    CommonUtils.Image_Picker_Dialog(ActivityCreateChatcode.this);
                    Log.e(TAG, "Purchased: " + info.getSku());

                    gotRandomizer = true;
                }
            }
        };
        iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.e(TAG, "Problem setting up In-app Billing: " + result);
                }
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);

                iabBroadcastReceiver = new IabBroadcastReceiver(ActivityCreateChatcode.this);
                registerReceiver(iabBroadcastReceiver, broadcastFilter);

                Log.d(TAG, "Setup successful. Querying inventory.");
                try {
                    iabHelper.queryInventoryAsync(gotInventoryListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    Log.e(TAG, "Error querying inventory. Another async operation in progress.");
                }
            }
        });


        switch_banner.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b) {
                    if (!gotRandomizer) {
                        try {
                            iabHelper.launchPurchaseFlow(ActivityCreateChatcode.this, "next", 101, purchaseFinishedListener, "");
                        } catch (IabHelper.IabAsyncInProgressException e) {
                            e.printStackTrace();
                            Log.e(TAG, "Error launching purchase flow. Another async operation in progress.");
                        }

                    }
                } else {
//                    pushNotification("0");
                }
            }
        });

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bitmap bitmap = null;
            if (requestCode == 01) {
                Uri uri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == 11) {
                bitmap = (Bitmap) data.getExtras().get("data");
            }
//            Bitmap bmap = CommonUtils.resizeImage(profile_image, bitmap);
             file = CommonUtils.createDirectoryAndSaveFile(bitmap, "image.jpg", ActivityCreateChatcode.this);
            if(null!=file)
            submitDetails(file);
        }

}

    @Override
    public void receivedBroadcast() {
        Log.e(TAG, "Received broadcast notification. Querying inventory.");
        try {
            iabHelper.queryInventoryAsync(gotInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            Log.d(TAG, "Error querying inventory. Another async operation in progress.");
        }
    }
    public void checkChange(final Switch switchButton, final View view){
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(switchButton.isChecked())
                    view.setVisibility(View.VISIBLE);
                else
                    view.setVisibility(View.GONE);
            }
        });
    }

    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case 0:
                return new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                        CommonUtils.pDateSetListener,
                        pYear, pMonth, pDay);
            case 1:
                return new DatePickerDialog(this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                        pDateSetListener1,
                        pYear, pMonth, pDay);
         }
        return null;
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
      }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.picker_reset:
                showResetPickerDialog();
                break;
            case R.id.picker_limit:
                showTimeLimitPickerDialog();
                break;
            case R.id.picker_maxusers:
               showNumberPickerDialog();
                break;
            case R.id.done:
                max_capacity=String.valueOf(newVal);
                picker_maxusers.setText(String.valueOf(newVal));
                dialog.dismiss();
                break;
            case R.id.create:
//                mSocket.emit("create", name);
//                mSocket.emit("new message", "message");
//                mSocket.on("updaterooms", new Emitter.Listener() {
//                    @Override
//                    public void call(Object... args) {
//                        Object room = args[0];
//                        Log.v("room",room.toString());
//                    }
//                });
//                 if(Constants.expiration.equals("")) {
//                     int mnt=pMonth;
//                     Constants.expiration = pYear + "-" + (mnt + 1) + "-" + pDay + " 23:59:00";// Month is 0 based so add 1
//                 }
//                Constants.expiration=CommonUtils.getCurrentDate();


                if(Constants.time_type.equals("")) {
                    Constants.time_type = timeLimit[0];
                 }
                if(Constants.time_limit.equals(""))
                    Constants.time_limit="1000";
                if(Constants.max_capacity.equals(""))
                    Constants.max_capacity="15";
                if(switch_photos.isChecked())
                    Constants.photos="Yes";
                else
                    Constants.photos="No";
                Constants.name=name.getText().toString();
                if(!Constants.name.equals("")&&!Constants.expiration.equals("")){
//                        &&!Constants.expires.equals("")
//                        &&!Constants.expires.equals("")
//                        &&!Constants.photos.equals("")
//                        &&!Constants.max_capacity.equals("")
//                        &&!Constants.reset.equals("")
//                        &&!Constants.time_limit.equals("")
//                        &&!Constants.expiration.equals("")) {
                    Intent i = new Intent(ActivityCreateChatcode.this, ActivityCreateSuccess.class);
                    startActivity(i);
                    finish();
                 }else if(Constants.name.equals("")){
                    CommonUtils.getSnackbar(parent,"Please enter group name");
                }else if(Constants.expiration.equals("")){
                    CommonUtils.getSnackbar(parent,"Please add expiration date");
                }
                break;
        }
    }

    private void showNumberPickerDialogLimit(int type) {
        dialog=new Dialog(ActivityCreateChatcode.this);
        dialog.setContentView(R.layout.dialog_enter_number_picker);
        dialog.setCancelable(false);
        final EditText edittext  = (EditText)dialog.findViewById(R.id.edittext);
        done               = (Button)dialog. findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!edittext.getText().toString().equals("")){
                    dialog.dismiss();
                    Constants.time_limit=edittext.getText().toString();
                }
            }
        });

//         if(type==0) {
//            np.setMinValue(1); //from array first value
//            np.setMaxValue(10); //to array last value
//        }else   if(type==1) {
//            np.setMinValue(1); //from array first value
//            np.setMaxValue(12); //to array last value
//        }else  if(type==2) {
//            np.setMinValue(1); //from array first value
//            np.setMaxValue(30); //to array last value
//        }else  if(type==3) {
//            np.setMinValue(1); //from array first value
//            np.setMaxValue(24); //to array last value
//        }else  if(type==4) {
//            np.setMinValue(1); //from array first value
//            np.setMaxValue(60); //to array last value
//        }
//            np.setWrapSelectorWheel(true);
//        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
//            @Override
//            public void onValueChange(NumberPicker picker, int oldVal, int newVal1){
//                newVal=newVal1;
//            }
//        });
        dialog.show();

    }



    private void showNumberPickerDialog() {
        dialog=new Dialog(ActivityCreateChatcode.this);
        dialog.setContentView(R.layout.dialog_number_picker);
        dialog.setCancelable(false);
        np                 = (NumberPicker)dialog.findViewById(R.id.np);
        done               = (Button)dialog. findViewById(R.id.done);
        done.setOnClickListener(this);
        np.setMinValue(1); //from array first value
        np.setMaxValue(10000); //to array last value
        np.setWrapSelectorWheel(true);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal1){
                newVal=newVal1;
            }
        });
        dialog.show();

    }
    private void showResetPickerDialog() {
        dialog=new Dialog(ActivityCreateChatcode.this);
        dialog.setContentView(R.layout.dialog_number_picker);
        dialog.setCancelable(false);
        np                 = (NumberPicker)dialog.findViewById(R.id.np);
        done               = (Button)dialog. findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
                                    @Override
                                    public void onClick(View view) {
                                        Constants.reset=reset[resetType];
                                         dialog.dismiss();
                                        openTimePickerDialog(true);
                                    }
                                });
        np.setDisplayedValues(reset);
        np.setMinValue(0); //from array first value
        np.setMaxValue(3);
        np.setWrapSelectorWheel(true);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal1){
                resetType=newVal1;
            }
        });
        dialog.show();

    }

    private void showTimeLimitPickerDialog() {
        dialog=new Dialog(ActivityCreateChatcode.this);
        dialog.setContentView(R.layout.dialog_number_picker);
        dialog.setCancelable(false);
        np                 = (NumberPicker)dialog.findViewById(R.id.np);
        done               = (Button)dialog. findViewById(R.id.done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();

//                time_limit=timeLimit[limit].replace(" min","");
                time_type=timeLimit[limit];
                 showNumberPickerDialogLimit(limit);
                picker_limit.setText(time_type);
             }
        });
        np.setDisplayedValues(timeLimit);
        np.setMinValue(0); //from array first value
        np.setMaxValue(3);
        np.setWrapSelectorWheel(true);
        np.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal1){
                limit=newVal1;
            }
        });
        dialog.show();

    }

    private void openTimePickerDialog(boolean is24r) {
        Calendar calendar = Calendar.getInstance();
        TimePickerDialog timePickerDialog = new TimePickerDialog(ActivityCreateChatcode.this,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT,
                onTimeSetListener, calendar.get(Calendar.HOUR_OF_DAY),
                calendar.get(Calendar.MINUTE), is24r);
         timePickerDialog.show();
    }

    TimePickerDialog.OnTimeSetListener onTimeSetListener = new TimePickerDialog.OnTimeSetListener() {
        @Override
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
            Calendar calNow = Calendar.getInstance();
            Calendar calSet = (Calendar) calNow.clone();
//            if(resetType==1)
//            calSet.set(Calendar.DAY_OF_WEEK, hourOfDay);
//            else if(resetType==2)
//                calSet.set(Calendar.DAY_OF_MONTH, reset_date);
//            else if(resetType==3)
//                calSet.set(Calendar.DAY_OF_YEAR, hourOfDay);
            calSet.set(Calendar.HOUR_OF_DAY, hourOfDay);
            calSet.set(Calendar.MINUTE, minute);
            calSet.set(Calendar.SECOND, 0);
            calSet.set(Calendar.MILLISECOND, 0);

            if (calSet.compareTo(calNow) <= 0) {
                // Today Set time passed, count to tomorrow
                calSet.add(Calendar.DATE, 1);
            }

            setAlarm(calSet);
        }
    };

    private void setAlarm(Calendar targetCal) {
        reset_date=(CommonUtils.printDifference_(new StringBuilder()
                // Month is 0 based so add 1
                .append(pYear).append("-")
                .append(pMonth + 1).append("-")
                .append(pDay).append("").toString()));

//        picker_reset.setText(reset[resetType]+","+reset_date);

        String hour= String.valueOf(targetCal.getTime().getHours());
        String min = String.valueOf(targetCal.getTime().getMinutes());
        String sec= String.valueOf(targetCal.getTime().getSeconds());
        if(hour.equals("0")){
            hour="00";
        }if(min.equals("0")){
            min="00";
        }if(sec.equals("0")){
            sec="00";
        }
        Constants.reset=hour+":"+min+":"+sec;
        Constants.reset0n=reset[resetType];
        picker_reset.setText(reset[resetType]+" ,"+(hour+":"+
                min+":"+
                sec));
//        picker_reset.setText(reset[resetType]+","+reset_date);
        Intent intent = new Intent(getBaseContext(), AlarmReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(
                getBaseContext(), 1, intent, 0);
        AlarmManager alarmManager = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        alarmManager.set(AlarmManager.RTC_WAKEUP, targetCal.getTimeInMillis(),
                pendingIntent);

    }

    public  DatePickerDialog.OnDateSetListener pDateSetListener1=
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    pYear   = year;
                    pMonth  = monthOfYear;
                    pDay    = dayOfMonth;
                    updateDisplay1();
                }

            };

    private  void updateDisplay1() {
        reset_date=(CommonUtils.printDifference_(new StringBuilder()
                // Month is 0 based so add 1
                .append(pYear).append("-")
                .append(pMonth + 1).append("-")
                .append(pDay).append("").toString()));

    }
    void submitDetails(File _image) {
        final ProgressDialog progressDialog=CommonUtils.setProgress("Uploading  !!!",this);
        UserApi userApi     = ApiFactory.createServiceForGson();
        String fileName = "";
        RequestBody requestBody= null;
        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        if(null!=_image) {
            String image_name = _image.getName();
            fileName = "group_icon\"; filename=\"" + image_name;
            requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), _image);
            requestBodyMap.put(fileName, requestBody);
        }
//        1492150598380
        userApi.uploadQRInfo(requestBodyMap).enqueue(new Callback<UploadModel>() {
            @Override
            public void onResponse(Call<UploadModel> call, Response<UploadModel> response) {
                if (response.body() != null) {
                    try {
                        if (response.body().getMessage().equals("success")) {
                            CommonUtils.addSnackbar(parent, "Succesfully uploaded");
                            banner_icon=response.body().getNew_image();
                         } else {
                            CommonUtils.addSnackbar(parent, "Unable to upload");
                        }
                    }catch (Exception e){
                        CommonUtils.addSnackbar(parent, "Unable to upload");
                    }
                }
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<UploadModel> call, Throwable t) {
                CommonUtils.addSnackbar(parent, t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

}

