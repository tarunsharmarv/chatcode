package com.user.chatcode.adapter;

import android.annotation.TargetApi;
import android.content.Context;
import android.os.Build;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.TextView;

import com.user.chatcode.R;
import com.user.chatcode.utils.Constants;

/**
 * Created by user on 21/12/16.
 */
public class DialerAdater extends BaseAdapter {
    private Context mContext;
    String act;

    public DialerAdater(Context c,String act) {
        mContext = c;
        this.act=act;
    }

    public int getCount() {
        return Constants.list.length;
    }

    public Object getItem(int position) {
        return null;
    }

    public long getItemId(int position) {
        return 0;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        TextView textView;
        if (convertView == null) {
            textView = new TextView(mContext);
            textView.setLayoutParams(new GridView.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
            textView.setTextColor(mContext.getResources().getColor(R.color.dialer_color));
            textView.setTextSize(mContext.getResources().getDimension(R.dimen.text_Size_dialer));
            textView.setPadding(0, 0, 0, 0);
        } else {
            textView = (TextView) convertView;
        }

        if(act.equals("2"))
            textView.setText(Constants.list1[position]);
        else
        textView.setText(Constants.list[position]);
        return textView;
    }


}