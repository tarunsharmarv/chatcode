package com.user.chatcode.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.user.chatcode.ActivityChatting;
import com.user.chatcode.ActivityInbox;
import com.user.chatcode.ActivityMainChatting;
import com.user.chatcode.R;
import com.user.chatcode.model.ActiveModel;
import com.user.chatcode.model.GroupObject;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;
import com.user.chatcode.utils.RoundRectCornerImageView;
import com.user.chatcode.utils.TouchImageView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

import static android.R.attr.path;
import static com.user.chatcode.ActivityChatting.showDialog;
import static com.user.chatcode.ActivityMainChatting.listdata;
import static com.user.chatcode.utils.Constants.IMAGES_URL;


public class HorizontalAdapter extends RecyclerView.Adapter<HorizontalAdapter.MyViewHolder> {
    ArrayList <ActiveModel> horizontalList =new ArrayList<>();
    Context context;
    String group_id;
    Activity activity;

    public HorizontalAdapter(String group_id,ArrayList<ActiveModel> horizontalList, Context context,Activity activity) {
        this.horizontalList = horizontalList;
        this.context = context;
        this.group_id=group_id;
        this.activity=activity;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        ImageView circleImageView;
        ImageView imageView,topPanel;
        public MyViewHolder(View view) {
            super(view);
            topPanel=(ImageView)view.findViewById(R.id.topPanel);
            circleImageView=(ImageView)view.findViewById(R.id.user_active);
            imageView=(ImageView) view.findViewById(R.id.user);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.vertical_items, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {

        if(horizontalList.get(position).getActive().equals("1"))
            holder.circleImageView.setBackgroundTintList(context.getResources().getColorStateList(R.color.selectedcolor));
        else
            holder.circleImageView.setBackgroundTintList(context.getResources().getColorStateList(R.color.dialer_color));
        if(horizontalList.get(position).getStatus().equals("1"))
            holder.topPanel.setVisibility(View.GONE);
        else
            holder.topPanel.setVisibility(View.VISIBLE);

        if(!horizontalList.get(position).getImage().equals("")) {
            String path = Constants.IMAGES_URL + horizontalList.get(position).getImage();
            Picasso.with(context)
                    .load(path)
                    .resize(50, 50)
                    .centerCrop()
                    .error(R.drawable.user)
                    .into(holder.imageView);
        }else
            holder.imageView.setBackgroundTintList(context.getResources().getColorStateList(R.color.colorAccent));

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final Dialog dialog=new Dialog(context);
                dialog.setContentView(R.layout.show_image_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                RoundRectCornerImageView imageView=(RoundRectCornerImageView)dialog.findViewById(R.id.image_View);
                Button block=(Button)dialog.findViewById(R.id.block);
                Button remove=(Button)dialog.findViewById(R.id.remove);
                Button back=(Button)dialog.findViewById(R.id.cancel);
                Button message=(Button)dialog.findViewById(R.id.message);


                TextView name=(TextView)dialog.findViewById(R.id.name);
                if(!horizontalList.get(position).getImage().equals("")) {
                    String path = Constants.IMAGES_URL + horizontalList.get(position).getImage();
                    Picasso.with(context)
                            .load(path)
                            .resize(100, 100)
                            .centerCrop()
                            .error(R.drawable.user)
                            .into(imageView);
                }else
                    holder.imageView.setBackgroundTintList(context.getResources().getColorStateList(R.color.colorAccent));
                name.setText(horizontalList.get(position).getName());
                if(horizontalList.get(position).getStatus().equals("0")){
                    block.setText("Blocked");
                }else{
                    block.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            String a=CommonUtils.getTempFileName(context, "id")+","+
                                    (horizontalList.get(position).getUser_id())+","+
                                    horizontalList.get(position).getUser_id()+","+
                                    ActivityChatting.created_by;
                            if (!CommonUtils.getTempFileName(context, "id").equals(horizontalList.get(position).getUser_id())&&
                                    CommonUtils.getTempFileName(context, "id").equals(ActivityChatting.created_by)) {
                                ActivityChatting.blockOrRemoveChat(group_id, horizontalList.get(position).getUser_id(), "block");
                                holder.topPanel.setVisibility(View.VISIBLE);

                            } else
                                showDialog();
                            dialog.dismiss();
                        }
                    });
                }
                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        String b=CommonUtils.getTempFileName(context, "id")+","+
                                (horizontalList.get(position).getUser_id())+","+
                                ActivityChatting.created_by;
                        if(!CommonUtils.getTempFileName(context, "id").equals(horizontalList.get(position).getUser_id())&&
                                CommonUtils.getTempFileName(context, "id").equals(ActivityChatting.created_by)) {
                            ActivityChatting.blockOrRemoveChat(group_id, horizontalList.get(position).getUser_id(), "remove");
                            int a = Integer.parseInt((listdata.get(position).getUsercount()));
                            listdata.get(position).setUsercount(String.valueOf(a - 1));
                            ActivityChatting activityChatting = (ActivityChatting) context;
                            activityChatting.finish();
                        }else
                            showDialog();
                        dialog.dismiss();
                    }
                });
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                message.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                        String c=CommonUtils.getTempFileName(context, "id")+","+(horizontalList.get(position).getUser_id());
                        if(!CommonUtils.getTempFileName(context, "id").equals(horizontalList.get(position).getUser_id())) {
                            ArrayList<GroupObject> list = horizontalList.get(position).getFirstgroup();
                            ArrayList<GroupObject> list1 = horizontalList.get(position).getSecondgroup();

                            if (list.size() > 0) {
                                ActivityChatting.create(horizontalList.get(position).getName(),activity,list.get(0).getId(),list.get(0).getName(), horizontalList.get(position).getOther_user_id());
                            } else if (list1.size() > 0) {
                                ActivityChatting.create(horizontalList.get(position).getName(),activity,list1.get(0).getId(),list1.get(0).getName(), horizontalList.get(position).getOther_user_id());
                            } else
                                ActivityChatting.create(horizontalList.get(position).getName(),activity,"","", horizontalList.get(position).getOther_user_id());

//                        Intent intent = new Intent(context,ActivityInbox.class);
//                        intent.putExtra("userid", horizontalList.get(position).getUser_id());
//                        intent.putExtra("position",position);
//                        context.startActivity( intent);
                        } else
                            showDialog();
                    }
                });

                dialog.show();
            }

        });

    }

    @Override
    public int getItemCount()
    {
        return horizontalList.size();
    }
}