package com.user.chatcode.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.user.chatcode.ActivityChatting;
import com.user.chatcode.ActivityMainChatting;
import com.user.chatcode.R;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.chatting.MainFragment;
import com.user.chatcode.model.ChattingListModel;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;
import com.user.chatcode.utils.RoundRectCornerImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.user.chatcode.R.id.readonly;


public class MainChattingAdapter extends BaseAdapter {

    ArrayList<ChattingListModel> dataSet=new ArrayList<>();
    Context mContext;
    Activity activity;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtnumber,expired,message;
        ImageView image;
    }

    public MainChattingAdapter(Activity activity,ArrayList<ChattingListModel> data, Context context) {
        this.activity=activity;
        this.dataSet = data;
        this.mContext=context;

    }


    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final ChattingListModel dataModel = dataSet.get(position);
        ViewHolder viewHolder; // view lookup cache stored in tag
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.chat_item, parent, false);
            viewHolder.txtName   = (TextView) convertView.findViewById(R.id.name);
            viewHolder.message = (TextView) convertView.findViewById(R.id.message);
            viewHolder.txtnumber = (TextView) convertView.findViewById(R.id.number);
            viewHolder.expired   = (TextView) convertView.findViewById(R.id.expired);
            viewHolder.image     = (ImageView) convertView.findViewById(R.id.image);
            result=convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }

        final View finalConvertView = convertView;
        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                final Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.show_message_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Button back = (Button) dialog.findViewById(R.id.cancel);
                Button readonly = (Button) dialog.findViewById(R.id.readonly);
                readonly.setText("OK");
                TextView message = (TextView) dialog.findViewById(R.id.message);
                message.setText("Do you want to delete  this chatcode ?");
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                readonly.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityMainChatting.delete_room(dataModel.getId(),position,finalConvertView);
                        dialog.dismiss();
                    }
                });
                dialog.show();
                return true;
            }

        });


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ActivityChatting.class);
                intent.putExtra("group",            dataModel.getId());
                intent.putExtra("group_created_by", dataModel.getUser_id());
                intent.putExtra("position",         position);
                intent.putExtra("title",            dataModel.getName());
                intent.putExtra("updated_at",     dataModel.getCreated_at());
                intent.putExtra("banner",     dataModel.getBanner());
                intent.putExtra("banner_updated",     dataModel.getBanner_updated_at());
                intent.putExtra("end_date_time",     dataModel.getEnd_date_time());
                intent.putExtra("expiration",     dataModel.getExpiration());

                intent.putExtra("after_days",     dataModel.getTime_limit()+","+dataModel.getTime_type());

                activity.startActivity(intent);

            }
        });
        viewHolder.txtName.setText(dataModel.getName());
        viewHolder.txtnumber.setText(dataModel.getUsercount());
        viewHolder.message.setVisibility(View.GONE);
        if(dataModel.getExpired()) {
            viewHolder.expired.setVisibility(View.VISIBLE);
            viewHolder.expired.setText("Expired");
        }else
            viewHolder.expired.setVisibility(View.GONE);
        return convertView;
    }
}