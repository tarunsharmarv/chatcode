package com.user.chatcode.adapter;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.user.chatcode.ActivityChatcodeDetails;
import com.user.chatcode.ActivityChatting;
import com.user.chatcode.ActivityMainChatting;
import com.user.chatcode.R;
import com.user.chatcode.model.ChattingListModel;
import com.user.chatcode.model.InboxListModel;
import com.user.chatcode.utils.Constants;

import java.util.ArrayList;

/**
 * Created by user on 23/12/16.
 */
public class InboxAdapter extends BaseAdapter {

    ArrayList<InboxListModel> dataSet = new ArrayList<>();
    Context mContext;
    Activity activity;

    // View lookup cache
    private static class ViewHolder {
        TextView txtName;
        TextView txtnumber, expired,message;
        //        ImageView image;
        LinearLayout number_layout;
    }

    public InboxAdapter(Activity activity, ArrayList<InboxListModel> data, Context context) {
        this.activity = activity;
        this.dataSet = data;
        this.mContext = context;

    }


    @Override
    public int getCount() {
        return dataSet.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        final InboxListModel dataModel = dataSet.get(position);
        ViewHolder viewHolder; // view lookup cache stored in tag
        final View result;

        if (convertView == null) {
            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(mContext);
            convertView = inflater.inflate(R.layout.chat_item, parent, false);
            viewHolder.txtName = (TextView) convertView.findViewById(R.id.name);
            viewHolder.txtnumber = (TextView) convertView.findViewById(R.id.number);
            viewHolder.message = (TextView) convertView.findViewById(R.id.message);

            viewHolder.number_layout = (LinearLayout) convertView.findViewById(R.id.number_layout);
            viewHolder.expired = (TextView) convertView.findViewById(R.id.expired);
//            viewHolder.image = (ImageView) convertView.findViewById(R.id.image);
            result = convertView;

            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result = convertView;
        }

        final View finalConvertView = convertView;
        convertView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {

                final Dialog dialog = new Dialog(mContext);
                dialog.setContentView(R.layout.show_message_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                Button back = (Button) dialog.findViewById(R.id.cancel);
                Button readonly = (Button) dialog.findViewById(R.id.readonly);
                readonly.setText("OK");
                TextView message = (TextView) dialog.findViewById(R.id.message);
                message.setText("Do you want to delete  this chatcode ?");
                back.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                readonly.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ActivityMainChatting.delete_room(dataModel.getId(), position, finalConvertView);
                        dialog.dismiss();
                    }
                });
                dialog.show();
                return true;
            }

        });

//
//                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
//                alertDialogBuilder.setMessage("Do you want to delete ?");
//                alertDialogBuilder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                    public void onClick(DialogInterface dialog, int id) {
//                        ActivityMainChatting.delete_room(dataModel.getId(),position,finalConvertView);
//                        dialog.dismiss();
//                     }
//                });
//                alertDialogBuilder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
//                            public void onClick(DialogInterface dialog, int id) {
//                                dialog.dismiss();
//                            }
//                         });
//                alertDialogBuilder.create().show();
//                return true;
//            }
//        });
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, ActivityChatting.class);
                if(dataModel.getFirstgroup().size()>0) {
                    intent.putExtra("group", dataModel.getFirstgroup().get(0).getId());
                    intent.putExtra("group_user_name", dataModel.getFirstgroup().get(0).getName());

                }else if(dataModel.getSecondgroup().size()>0) {
                    intent.putExtra("group", dataModel.getSecondgroup().get(0).getId());
                    intent.putExtra("group_user_name", dataModel.getSecondgroup().get(0).getName());
                }
//                }else{
//                    intent.putExtra("group","");
//                }
                intent.putExtra("title", dataModel.getName());
                intent.putExtra("group_created_by", dataModel.getId());
                intent.putExtra("one_to_one", "1");
                intent.putExtra("position", position);
                activity.startActivity(intent);
//                        intent.putExtra("numUsers", numUsers);
//                 mSocket.emit("adduser", dataModel.getName());
//                mSocket.on("updatechat", new Emitter.Listener() {
//                    @Override
//                    public void call(Object... args) {
//                        Object room = args[0];
//
//                    }
//                });
//                Intent intent = new Intent();
//                intent.putExtra("username", dataModel.getName());
//                activity.setResult(RESULT_OK, intent);
//                activity.finish();
//                mSocket.emit("adduser", dataModel.getName());
//                mSocket.on("login",new Emitter.Listener() {
//                    @Override
//                    public void call(Object... args) {
//                        JSONObject data = (JSONObject) args[0];
////                        mSocket.emit("create", "test room");
////                        mSocket.on("updaterooms", new Emitter.Listener() {
////                            @Override
////                            public void call(Object... args) {
////                                Object room = args[0];
////
////                            }
////                        });
//                        int numUsers;
//                        try {
//                            numUsers = data.getInt("numUsers");
//                        } catch (JSONException e) {
//                            return;
//                        }
//

                //
//                    }
//                });

//                Intent i=new Intent(mContext, ActivityChatcodeDetails.class);
//                Intent i=new Intent(mContext, MainActivity.class);
//                i.putExtra("title",dataModel.getName());
//                i.putExtra("number",dataModel.getNumber());
//                mContext.startActivity(i);
            }
        });
        viewHolder.txtName.setText(dataModel.getName());
        viewHolder.message.setText(dataModel.getLastmessage());
        viewHolder.txtnumber.setVisibility(View.GONE);
        viewHolder.expired.setVisibility(View.GONE);
        viewHolder.number_layout.setVisibility(View.GONE);
//        Picasso.with(mContext)
//                .load(Constants.IMAGES_URL+dataModel.getProfile_image())
//                .resize(90,90)
//                .centerCrop()
//                .into(viewHolder.image);
        // Return the completed view to render on screen
        return convertView;
    }
}