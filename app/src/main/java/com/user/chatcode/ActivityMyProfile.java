package com.user.chatcode;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.model.UploadModel;
import com.user.chatcode.util.IabBroadcastReceiver;
import com.user.chatcode.util.IabHelper;
import com.user.chatcode.util.IabResult;
import com.user.chatcode.util.Inventory;
import com.user.chatcode.util.Purchase;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.user.chatcode.ActivityChatting.parent;
import static com.user.chatcode.R.id.edit_user;
import static com.user.chatcode.firebase.MyFirebaseInstanceIDService.refreshedToken;
import static junit.runner.Version.id;

/**
 * Created by user on 21/12/16.
 */
public class ActivityMyProfile extends AppCompatActivity implements View.OnClickListener {
    ImageView profile_image;
    TextView user;
    EditText username;
    Switch switch_chat,switch_inbox;
    ScrollView parent;
    File profileFile;
    RelativeLayout topView;







    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_invite);



        profile_image=(ImageView)findViewById(R.id.profile_image);
        username   =(EditText)findViewById(R.id.username);
        user       =(TextView)findViewById(R.id.user);
//        codename   =(TextView)findViewById(R.id.codename);
        switch_chat =(Switch)findViewById(R.id.switch_chat);
        switch_inbox =(Switch)findViewById(R.id.switch_inbox);

        parent      = (ScrollView)findViewById(R.id.parent);
        topView=(RelativeLayout)findViewById(R.id.topView);
        findViewById(R.id.button_take_new).setOnClickListener(this);
        findViewById(R.id.button_choose).setOnClickListener(this);
        findViewById(R.id.button_cancel).setOnClickListener(this);
        findViewById(R.id.next).setOnClickListener(this);
        findViewById(R.id.account).setOnClickListener(this);
        findViewById(R.id.support).setOnClickListener(this);
        findViewById(R.id.profile_image).setOnClickListener(this);
        findViewById(R.id.signout).setOnClickListener(this);
        Picasso.with(this).load(Constants.IMAGES_URL+CommonUtils.getTempFileName(ActivityMyProfile.this,"profile_image")).resize(200,200).centerCrop().into(profile_image);
      if(null!=CommonUtils.getTempFileName(ActivityMyProfile.this,"push_notification")){
          if(CommonUtils.getTempFileName(ActivityMyProfile.this,"push_notification").equals("1"))
              switch_chat.setChecked(true);
          else
              switch_chat.setChecked(false);
      }


        if(null!=CommonUtils.getTempFileName(ActivityMyProfile.this,"inbox_push_notification")){
            if(CommonUtils.getTempFileName(ActivityMyProfile.this,"inbox_push_notification").equals("1"))
                switch_inbox.setChecked(true);
            else
                switch_inbox.setChecked(false);
        }
        username.setText(CommonUtils.getTempFileName(ActivityMyProfile.this,"name"));
        user.setText(CommonUtils.getTempFileName(ActivityMyProfile.this,"name"));
         switch_chat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    pushNotification("1");
                }else{
                    pushNotification("0");
                }
            }
        });

        switch_inbox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if(b){
                    pushNotificationInbox("1");
                }else{
                    pushNotificationInbox("0");
                }
            }
        });

        parent.setOnScrollChangeListener(new View.OnScrollChangeListener() {
            @Override
            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
                if(topView.getVisibility()==View.VISIBLE)
                    topView.setVisibility(View.GONE);
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
          case R.id.next:
            String user=username.getText().toString();
            if(null!=profileFile&&!user.equals(""))
                submitDetails(user,CommonUtils.getTempFileName(this,"id"),profileFile);
            else if(null==profileFile&&!user.equals("")){
                submitDetails(user,CommonUtils.getTempFileName(this,"id"),null);
            }
            else if(!CommonUtils.getTempFileName(ActivityMyProfile.this,"profile_image").equals("")&&!user.equals("")) {
               this.finish();
            }
            break;
            case R.id.profile_image:
//                topView.setVisibility(View.VISIBLE);
                CommonUtils.Image_Picker_Dialog(this);
                break;
//            case R.id.button_take_new:
//                Intent pictureActionIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(pictureActionIntent, 1);
//                topView.setVisibility(View.GONE);
//                break;
//            case R.id.button_choose:
//                Intent pictureActionIntent1 = new Intent(Intent.ACTION_GET_CONTENT, null);
//                pictureActionIntent1.setType("image/*");
//                pictureActionIntent1.putExtra("return-data", true);
//                startActivityForResult(pictureActionIntent1, 0);
//                topView.setVisibility(View.GONE);
//                break;
//            case R.id.button_cancel:
//                topView.setVisibility(View.GONE);
//                break;
            case R.id.signout:
                Intent i=new Intent(ActivityMyProfile.this, ActivityLogin.class);
                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(i);
                finish();
                CommonUtils.saveTempFileName(this,"logged_in","0");
                break;
            case R.id.account:
                Intent intent = new Intent(ActivityMyProfile.this, ActivityAccount.class);
                startActivity(intent);
                break;
            case R.id.support:
                Intent intent1 = new Intent(ActivityMyProfile.this, WebViewActivity.class);
                startActivity(intent1);
                break;
        }
    }
    void submitDetails(final String name, String id, final File profileImage ) {
        final ProgressDialog progress=new ProgressDialog(this);
        progress.setMessage("Loading !!!");
        progress.show();
        UserApi userApi     = ApiFactory.createServiceForGson();
        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        if(null!=profileImage) {
            String image_name1 = profileImage.getName();
            String fileName1 = "profile_image\"; filename=\"" + image_name1;
            RequestBody requestBody3 = RequestBody.create(MediaType.parse("multipart/form-data"), profileImage);
            requestBodyMap.put(fileName1, requestBody3);
        }
            RequestBody username_        = RequestBody.create(MediaType.parse("multipart/form-data"),name);
        RequestBody user_id_        = RequestBody.create(MediaType.parse("multipart/form-data"), id);
        requestBodyMap.put("username", username_);
        requestBodyMap.put("user_id", user_id_);
        userApi.update_profile(requestBodyMap).enqueue(new Callback<UploadModel>() {
            @Override
            public void onResponse(Call<UploadModel> call, Response<UploadModel> response) {
                if(response.body().getMessage().equals("success")) {
                    CommonUtils.getSnackbar(parent,response.body().getMessage());
                    CommonUtils.saveTempFileName(ActivityMyProfile.this,"name",name);
                    if(null!=profileImage) {
                        CommonUtils.saveTempFileName(ActivityMyProfile.this, "profile_image", response.body().getNew_image());
                    }
                    Intent i = new Intent(ActivityMyProfile.this, ActivityBeforeMainChatting.class);
                    startActivity(i);
                }
            progress.dismiss();
            }
            @Override
            public void onFailure(Call<UploadModel> call, Throwable t) {
                //                CommonUtils.getSnackbar(layoutParent, t.getMessage());
                CommonUtils.getSnackbar(parent,t.getMessage());
            }
        });
        progress.dismiss();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK) {
            Bitmap bitmap = null;
            if (requestCode == 01) {
                Uri uri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == 11) {
                bitmap = (Bitmap) data.getExtras().get("data");
            }
            Bitmap bmap=CommonUtils.resizeImage(profile_image,bitmap);
            profile_image.setImageBitmap(bmap);
            profileFile=CommonUtils.createDirectoryAndSaveFile(bmap,"profile.jpg",ActivityMyProfile.this);
        }
    }
    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }


    public  void  pushNotificationInbox(final String switch_) {
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.pushNotificationInbox(switch_,CommonUtils.getTempFileName(ActivityMyProfile.this,"id")).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    if(response.code()==200) {
                        if(null!=response.body()) {
                            Object resObject = new Gson().toJson(response.body());
                            String resString = resObject.toString();
                            JSONObject obj = new JSONObject(resString);
                            if(obj.getString("message").equals("Success"))
                                CommonUtils.saveTempFileName(ActivityMyProfile.this,"inbox_push_notification",switch_);
                        }
                    }else if(response.code()==500){
                    }

                } catch (Throwable e) {
                    onFailure(call, e);
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
            }
        });
    }



    public  void  pushNotification(final String switch_) {
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.pushNotification(switch_,CommonUtils.getTempFileName(ActivityMyProfile.this,"id")).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    if(response.code()==200) {
                        if(null!=response.body()) {
                            Object resObject = new Gson().toJson(response.body());
                            String resString = resObject.toString();
                            JSONObject obj = new JSONObject(resString);
                            if(obj.getString("message").equals("Success"))
                            CommonUtils.saveTempFileName(ActivityMyProfile.this,"push_notification",switch_);
                        }
                    }else if(response.code()==500){
                    }

                } catch (Throwable e) {
                    onFailure(call, e);
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
            }
        });
    }


}
