package com.user.chatcode;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.squareup.picasso.Picasso;
import com.user.chatcode.adapter.HorizontalAdapter;
import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.chatting.ChatApplication;
import com.user.chatcode.chatting.MainActivity;
import com.user.chatcode.chatting.Message;
import com.user.chatcode.chatting.MessageAdapter;
import com.user.chatcode.model.ActiveModel;
import com.user.chatcode.model.ChattingModel;
import com.user.chatcode.model.GetUserModel;
import com.user.chatcode.model.GroupObject;
import com.user.chatcode.model.LoginModel;
import com.user.chatcode.model.MessagesModel;
import com.user.chatcode.model.UploadModel;
import com.user.chatcode.qrscanner.QrcodeScreen;
import com.user.chatcode.util.IabBroadcastReceiver;
import com.user.chatcode.util.IabHelper;
import com.user.chatcode.util.IabResult;
import com.user.chatcode.util.Inventory;
import com.user.chatcode.util.Purchase;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;
import com.user.chatcode.utils.RoundRectCornerImageView;

import org.apache.commons.lang3.StringEscapeUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.internal.Util;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static android.R.attr.mode;
import static android.R.attr.theme;
import static android.R.attr.x;
import static android.media.CamcorderProfile.get;
import static com.user.chatcode.ActivityInbox.read_only;
import static com.user.chatcode.ActivityMainChatting.adapter;
import static com.user.chatcode.ActivityMainChatting.context;
import static com.user.chatcode.ActivityMainChatting.get_group_user;
import static com.user.chatcode.ActivityMainChatting.next_url;
import static com.user.chatcode.R.id.block;
import static com.user.chatcode.R.id.id_inbox;
import static com.user.chatcode.R.id.numberDialer;
import static com.user.chatcode.R.id.progress;
import static com.user.chatcode.R.id.qrcode;
import static com.user.chatcode.R.id.scrollView;
import static com.user.chatcode.chatting.MainFragment.add;
import static com.user.chatcode.chatting.MainFragment.scrollToBottom;
import static com.user.chatcode.utils.CommonUtils.convertToLocal;
import static com.user.chatcode.utils.CommonUtils.getConverteddate;
import static com.user.chatcode.utils.CommonUtils.getConverteddate1;
import static com.user.chatcode.utils.CommonUtils.getCurrentDate;
import static com.user.chatcode.utils.CommonUtils.getTempFileName;
import static com.user.chatcode.utils.CommonUtils.hideKeyboard;

/**
 * Created by user on 23/12/16.
 */
public class ActivityChatting extends AppCompatActivity
        implements OnItemClickListener,View.OnClickListener ,IabBroadcastReceiver.IabBroadcastListener{
    TextView heading;
    static RecyclerView horizontal_recycler_view;
    static ArrayList<GroupObject> firstgroup, secondgroup;
    static HorizontalAdapter horizontalAdapter;
    String message="";
    Dialog dialog1;
    static String banner_icon="";
    File file=null;
    public static Resources resources;
    static ScrollView parent;
    static String one_to_one="";
    static JSONObject data = null;
    public static String id;
    private static final int REQUEST_LOGIN = 0;
    private static final int TYPING_TIMER_LENGTH = 600;
    private static RecyclerView mMessagesView;
    static OnItemClickListener clickListener;
    private EditText mInputMessageView;
    private static List<Message> mMessages = new ArrayList<Message>();
    private static MessageAdapter mAdapter;
    private boolean mTyping = false;
    static Boolean update;
    static  Date date,date1,updated_date;
    private Handler mTypingHandler = new Handler();
    public static String mUsername;
    private static Socket mSocket;
    public static Activity activity;
    static String next_user_url;
    static String last_id;
    private Boolean isConnected = true;
    public static int a = 0;
    Button back, info;
    static  String updated_at,after_days="0";
    private  Boolean inbox=false;
    static ProgressBar progressBar;
    static String one_to_one_id="";
    public static String usergroup,usergroup_name,created_by;
    public static ArrayList<ActiveModel> model;
    ImageView cover;



    IabHelper iabHelper;
    private IabBroadcastReceiver iabBroadcastReceiver;
    private IabHelper.QueryInventoryFinishedListener gotInventoryListener;
    private IabHelper.OnIabPurchaseFinishedListener purchaseFinishedListener;
    private static final String TAG = "In_app";
    private boolean gotRandomizer = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatting);
        clickListener=this;
        ChatApplication app = (ChatApplication) getApplication();
        mSocket = app.getSocket();
        mSocket.on(Socket.EVENT_CONNECT, onConnect);
        mSocket.on(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.on(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.on(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
//        mSocket.on("joingroup", onUserJoined);
//        mSocket.on("user left", onUserLeft);
        mSocket.on("updatechat", onNewMessage);
        mSocket.on("typing", onTyping);
        mSocket.on("stop typing", onStopTyping);
        mSocket.on("updateactiveuser", updateactiveusers);

        mSocket.connect();
        activity = ActivityChatting.this;
        progressBar = (ProgressBar) findViewById(R.id.progress);
        parent = (ScrollView) findViewById(R.id.parent);
        heading = (TextView) findViewById(R.id.heading);
        info = (Button) findViewById(R.id.info);
        info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(ActivityChatting.this, ActivityInfo.class);
                i.putExtra("position", getIntent().getIntExtra("position", 0));
                startActivity(i);
            }
        });
        cover=(ImageView)findViewById(R.id.cover);
        if (null != getIntent().getStringExtra("title"))
            heading.setText(getIntent().getStringExtra("title"));
        mInputMessageView = (EditText) findViewById(R.id.message_input);
        mInputMessageView.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int id, KeyEvent event) {
                if (id == R.id.send || id == EditorInfo.IME_NULL) {
                    attemptSend();
                    return true;
                }
                return false;
            }
        });
        mInputMessageView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (null == mUsername) return;
                if (!mSocket.connected()) return;
                if (!mTyping) {
                    mTyping = true;
                    mSocket.emit("typing");
                }
                mTypingHandler.removeCallbacks(onTypingTimeout);
                mTypingHandler.postDelayed(onTypingTimeout, TYPING_TIMER_LENGTH);
            }
            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        Button sendButton = (Button) findViewById(R.id.send_button);
        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                attemptSend();
            }
        });
        back = (Button) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent=new Intent(ActivityChatting.this,ActivityBeforeMainChatting.class);
                startActivity(intent);
                finish();
                ActivityCompat.finishAffinity(ActivityChatting.this);
             }
        });
        mMessagesView = (RecyclerView) findViewById(R.id.messages);
        mMessages.clear();
        mAdapter = new MessageAdapter(this, mMessages);
        mMessagesView.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(ActivityChatting.this);
                return false;
            }
        });
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        mMessagesView.setOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                if (dy > 0) {
                    // Scrolling up
                } else {
                    Boolean a = listIsAtTop();
                    if (a && null != next_url && progressBar.getVisibility() == View.GONE) {
                        if(!inbox)
                        getNextMessages(next_url, usergroup, "1");
                        else
                            getInboxMessages(next_url, usergroup, "1");
                    }
                    //Scrolling down
                }
            }

            @Override
            public void onScrollStateChanged(RecyclerView view, int scrollState) {
                if (scrollState == GridView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL) {
//
                }
            }
        });

        mMessagesView.setLayoutManager(new LinearLayoutManager(this));
        mMessagesView.setAdapter(mAdapter);
        resources = getResources();
        mUsername = getTempFileName(ActivityChatting.this, "name");
        horizontal_recycler_view = (RecyclerView) findViewById(R.id.horizontal_recycler_view);
        LinearLayoutManager horizontalLayoutManager = new LinearLayoutManager(activity, LinearLayoutManager.HORIZONTAL, false);
        horizontal_recycler_view.setLayoutManager(horizontalLayoutManager);
        horizontal_recycler_view.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideKeyboard(ActivityChatting.this);
                return false;
            }
        });
//      int currentSize=mMessages.size();
        mMessages.clear();
        if (null != getIntent().getStringExtra("raw")) {
            join_chat(getIntent().getStringExtra("raw"));
            inbox=false;
            try{
                date= convertToLocal(getIntent().getStringExtra("end_date_time"));
                date1=new Date();
                date1.setTime(date.getTime());
            }catch (Exception e){

            }
            try{
                String banner= getIntent().getStringExtra("banner");

                if (null!=banner&&!banner.equals("")){
                    Picasso.with(this).load(banner).into(cover);
                    cover.setVisibility(View.VISIBLE);
                }else
                    cover.setVisibility(View.GONE);
            }catch(Exception e){
                Log.v("tag",one_to_one);
            }
            cover.setOnClickListener(this);
            info.setVisibility(View.VISIBLE);
        } else {
            usergroup = getIntent().getStringExtra("group");
            usergroup_name=getIntent().getStringExtra("group_user_name");
            updated_at=getIntent().getStringExtra("updated_at");
//             String[] a= getIntent().getStringExtra("after_days").split(",");
//             int daysLeft=0;
//            if(a[1].equals("Year"))
//                daysLeft= Integer.parseInt(a[0])*365;
//            if(a[1].equals("Month"))
//                daysLeft= Integer.parseInt(a[0])*30;
//            if(a[1].equals("Days"))
//                daysLeft= Integer.parseInt(a[0])*1;
//            if(a[1].equals("Hours"))
//                daysLeft= (int) (Integer.parseInt(a[0])*(0.0416667));
//            if(a[1].equals("Minutes"))
//                daysLeft= (int) (Integer.parseInt(a[0])*0.000694444);
//            if(daysLeft==0&&a[1].equals("Hours")){
//                date.setTime(date.getTime()+(3600000*Integer.parseInt(a[0])));
//                date=CommonUtils.FormatDate(date);
//            }
//            if(daysLeft==0&&a[1].equals("Minutes")){
//                date.setTime(date.getTime()+(60000*Integer.parseInt(a[0])));
//                date=CommonUtils.FormatDate(date);
//            }
//            if(daysLeft!=0) {
//                after_days = CommonUtils.getCalculatedDate(updated_at, "yyyy-mm-dd HH:mm:ss", daysLeft);
//            }else
//                after_days ="0";

            created_by = getIntent().getStringExtra("group_created_by");
            try {
                one_to_one = getIntent().getStringExtra("one_to_one");
            }catch(Exception e){
                Log.v("tag",one_to_one);
            }
            try{
                 one_to_one_id= getIntent().getStringExtra("one_to_one_id");
//                String next_url = Constants.BASE_URL + Constants.NEXT_PAGE_URL_;
//                inbox=true;
//                getInboxMessages(next_url, usergroup, "0");
            }catch(Exception e){
                Log.v("tag",one_to_one);
            }
//            if(usergroup==null){
//                addinbox(getTempFileName(activity, "name"), usergroup,usergroup_name);
//
//            }
//        mAdapter.notifyItemRangeRemoved(0, currentSize);

            try{
                date= convertToLocal(getIntent().getStringExtra("end_date_time"));
                date1=new Date();
                date1.setTime(date.getTime());
            }catch (Exception e){

            }

            if (usergroup != null) {
                if(usergroup_name!=null){

                    info.setVisibility(View.GONE);
                    cover.setVisibility(View.GONE);
                    inbox = true;
                    ActivityMainChatting.tabSelected=2;
                    addinbox(getTempFileName(activity, "name"), usergroup,usergroup_name);
                }
                else {
                    inbox = false;
                    try{
                        String banner= getIntent().getStringExtra("banner");
                        date= convertToLocal(getIntent().getStringExtra("banner_updated"));
                        updated_date=new Date();
                        updated_date.setTime(date.getTime());
                        Date date = new Date(System.currentTimeMillis());


                        Date today = new Date();
                        long diff =  today.getTime() - updated_date.getTime();
                        int numOfDays = (int) (diff / (1000 * 60 * 60 * 24));
                        int hours = (int) (diff / (1000 * 60 * 60));

                        if (null!=updated_date&&hours<24&&null!=banner&&!banner.equals("")){
                            Picasso.with(this).load(banner).into(cover);
                            cover.setVisibility(View.VISIBLE);
                        }else
                            cover.setVisibility(View.GONE);
                    }catch(Exception e){
                        Log.v("tag",one_to_one);
                    }
                    cover.setOnClickListener(this);
                    info.setVisibility(View.VISIBLE);
                    ActivityMainChatting.tabSelected=1;
                    add(getTempFileName(activity, "name"), usergroup);
                }

            }
        }
        if(null!=created_by&&
                !created_by.equals(CommonUtils.getTempFileName(ActivityChatting.this, "id"))
                &&null!=date1)
                showMessage();

        initializeIAP();
    }

    private void initializeIAP() {
        String base64Data = getString(R.string.licence_key);

        iabHelper = new IabHelper(ActivityChatting.this, base64Data);


        gotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
            @Override
            public void onQueryInventoryFinished(IabResult result, Inventory inv) {
                if (iabHelper == null) return;

                if (result.isFailure()) {
                    Log.e(TAG, "Failed to query inventory: " + result);
                    return;
                }

                Purchase purchase = inv.getPurchase("redeem_product_listen");

                gotRandomizer = purchase != null; //TODO add verifyDeveloperPayload(Purchase p)

                Log.e(TAG, "Randomizer is: " + gotRandomizer);
            }
        };
        purchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
            @Override
            public void onIabPurchaseFinished(IabResult result, Purchase info) {
                if (iabHelper == null) return;

                if (result.isFailure()) {
                    Log.e(TAG, "Error purchasing: " + result);
                    return;
                }

                if (info.getSku().equals("next")) {
                    CommonUtils.Image_Picker_Dialog(ActivityChatting.this);
                    Log.e(TAG, "Purchased: " + info.getSku());

                    gotRandomizer = true;
                }
            }
        };
        iabHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
            public void onIabSetupFinished(IabResult result) {
                if (!result.isSuccess()) {
                    Log.e(TAG, "Problem setting up In-app Billing: " + result);
                }
                IntentFilter broadcastFilter = new IntentFilter(IabBroadcastReceiver.ACTION);

                iabBroadcastReceiver = new IabBroadcastReceiver(ActivityChatting.this);
                registerReceiver(iabBroadcastReceiver, broadcastFilter);

                Log.d(TAG, "Setup successful. Querying inventory.");
                try {
                    iabHelper.queryInventoryAsync(gotInventoryListener);
                } catch (IabHelper.IabAsyncInProgressException e) {
                    Log.e(TAG, "Error querying inventory. Another async operation in progress.");
                }
            }
        });

    }

    public void onResume(){
        super.onResume();

    }
    public  String  join_chat(final String usergroup) {
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.join_chat(usergroup, getTempFileName(activity,"id")).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    if(response.code()==200) {
                        if(null!=response.body()) {
                            Object resObject = new Gson().toJson(response.body());
                            String resString = resObject.toString();
                            JSONObject obj = new JSONObject(resString);
                            message=obj.getString("message");

                            if (null != message && !message.equals("")) {
                                Log.v("message",message+","+usergroup);
                                if(null!=message&&
                                        null!=usergroup)
                                    if(!message.equals("success"))
                                        alert(message, usergroup);
                                try {
                                            Log.v("response_obj", obj.getInt("group_id") + "");
                                            id = String.valueOf(obj.getInt("group_id"));
                                            Log.v("response_obj", id);
                                            ActivityChatting.usergroup = id;
                                            ActivityChatting.add(getTempFileName(activity, "name"), id);
                                            ActivityMainChatting.listdata = get_group_user("", getTempFileName(activity, "id"), "0");
                                            String next_url = Constants.BASE_URL + Constants.NEXT_PAGE_URL;
                                            ActivityChatting.getNextMessages(next_url, usergroup, "0");

                                        }catch (Exception e) {
                                            message="";
                                            Log.v("response_obj", e.toString());
                                        }

//                                    }else {
////                                        alert(message, usergroup);
//                                    }
                            }

                            Log.v("response_obj",message);
//                            if(message.trim().equals("success")){
                        }
                    }else if(response.code()==500){
                        message="";
                    }

                } catch (Throwable e) {
                    onFailure(call, e);
                    message="";
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.v("response_obj","fail");
                message="";
            }
        });

        return message;
    }
    public  void alert(String message, final String usergroup) {
        Log.v("message",message+","+usergroup);
        dialog1 = new Dialog(activity);
        dialog1.setContentView(R.layout.show_message_dialog);
        dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button back = (Button) dialog1.findViewById(R.id.cancel);
        Button readonly = (Button) dialog1.findViewById(R.id.readonly);
        TextView message1 = (TextView) dialog1.findViewById(R.id.message);
        dialog1.show();
        Log.v("result", "result"+","+message);
        if(message.contains("Chatcode already filled with its Max user")||
                message.equals("Sorry, the code you scanned is not a chatcode.")) {
            readonly.setVisibility(View.GONE);
            Log.v("result1", "result1");
            back.setText("OK");
            Log.v("result2", "result2");
        }
        message1.setText(message);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
                Intent i=new Intent(ActivityChatting.this,ActivityBeforeMainChatting.class);
                i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(i);
                finish();
            }
        });
        readonly.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1.dismiss();
                read_only(usergroup);
            }
        });
        Log.v("result3", "result3");
    }

    public  void  read_only(final String usergroup) {
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.read_chat(usergroup, CommonUtils.getTempFileName(this,"id")).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    if(response.code()==200) {
                        if(null!=response.body()) {
                            Object resObject = new Gson().toJson(response.body());
                            String resString = resObject.toString();
                            JSONObject obj = new JSONObject(resString);
                            Log.v("response_obj",obj.toString());

                        }
                    }else if(response.code()==500){
                    }

                } catch (Throwable e) {
                    onFailure(call, e);
                }
            }
            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                Log.v("response_obj","fail");
            }
        });
    }

    public static void getNextMessages(String url, String group_id, final String val) {
        if (val.equals("0")) {
            mMessages.clear();
            url = Constants.BASE_URL + Constants.NEXT_PAGE_URL;
        } else
            progressBar.setVisibility(View.VISIBLE);
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.next_page_url(url, group_id,CommonUtils.getTempFileName(activity,"id")).enqueue(new Callback<MessagesModel>() {
            @Override
            public void onResponse(Call<MessagesModel> call, Response<MessagesModel> response) {
                try {
                    if (response.code() == 200) {
                        if (null != response.body()) {
                            next_url = response.body().getNext_page_url();
                            if (null != response.body().getData())
                                for (int i = 0; i < response.body().getData().size(); i++) {
                                    mMessages.add(0, new Message.Builder(Message.TYPE_MESSAGE)
                                            .username(response.body().getData().get(i).getName()).
                                                    message(response.body().getData().get(i).getMessage()).build());
                                    if (null != mAdapter && val.equals("1"))
                                        mAdapter.notifyItemInserted(0);
                                    if (i == response.body().getData().size() - 1) {
                                        last_id = response.body().getData().get(i).getId();
                                    }
                                }
                            if (val.equals("0")) {
                                mAdapter = new MessageAdapter(activity, mMessages);
                                mMessagesView.setLayoutManager(new LinearLayoutManager(activity));
                                mMessagesView.setAdapter(mAdapter);
                                scrollToBottom();
                            }
                        }
                    } else if (response.code() == 500) {
                        CommonUtils.getSnackbar(parent, "Internal Server Error");
                    }
                    progressBar.setVisibility(View.GONE);

                } catch (Throwable e) {
                    onFailure(call, e);
                    progressBar.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<MessagesModel> call, Throwable t) {
                CommonUtils.getSnackbar(parent, t.getMessage());
                progressBar.setVisibility(View.GONE);

            }
        });
    }


    public void getInboxMessages(String url, String group_id, final String val) {
        inbox = true;
        if (val.equals("0")) {
            mMessages.clear();
            url = Constants.BASE_URL + Constants.NEXT_PAGE_URL_;
        } else
            progressBar.setVisibility(View.VISIBLE);
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.next_inbox_message_url(url, group_id).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    if (response.code() == 200) {
                        if (null != response.body()) {
                                        Object resObject = new Gson().toJson(response.body());
                                        String resString = resObject.toString();
                                        JSONObject obj = new JSONObject(resString);

//                            {
//                                "message": "Success",
//                                    "Data": {
//                                "total": 41,
//                                        "per_page": 20,
//                                        "current_page": 1,
//                                        "last_page": 3,
//                                        "next_page_url": "http://54.200.70.163/chatcode/public/api/getmessagehistory?page=2",
//                                        "prev_page_url": null,
//                                        "from": 1,
//                                        "to": 20,
//                                        "data": [
//                                {
//                                    "id": 11,
//                                        "user_id": 159,
//                                        "one_to_one_group_id": 2,
//                                        "message": "dvbgb",
//                                        "created_at": null,
//                                        "updated_at": "2017-04-12 10:52:40"
//                                },

                            try {
                                next_url = obj.getJSONObject("Data").getString("next_page_url");
                            }catch (Exception e){
                                next_url=null;
                            }
                            if (null !=obj.getJSONObject("Data").getJSONArray("data"))
                                for (int i = 0; i < obj.getJSONObject("Data").getJSONArray("data").length(); i++) {
                                    mMessages.add(0, new Message.Builder(Message.TYPE_MESSAGE)
                                            .username(obj.getJSONObject("Data").getJSONArray("data").getJSONObject(i).getString("user_name")).
                                                    message(obj.getJSONObject("Data").getJSONArray("data").getJSONObject(i).getString("message")).build());
                                    if (null != mAdapter && val.equals("1"))
                                        mAdapter.notifyItemInserted(0);
                                    if (i == obj.getJSONObject("Data").getJSONArray("data").length() - 1) {
                                        last_id = obj.getJSONObject("Data").getJSONArray("data").getJSONObject(i).getString("id");
                                    }
                                }
                            if (val.equals("0")) {
                                mAdapter = new MessageAdapter(activity, mMessages);
                                mMessagesView.setLayoutManager(new LinearLayoutManager(activity));
                                mMessagesView.setAdapter(mAdapter);
                                scrollToBottom();
                            }
                        }
                    } else if (response.code() == 500) {
                        CommonUtils.getSnackbar(parent, "Internal Server Error");
                    }
                    progressBar.setVisibility(View.GONE);

                } catch (Throwable e) {
                    onFailure(call, e);
                    progressBar.setVisibility(View.GONE);

                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                CommonUtils.getSnackbar(parent, t.getMessage());
                progressBar.setVisibility(View.GONE);

            }
        });
    }

    private boolean listIsAtTop() {
        if (mMessagesView.getChildCount() == 0) return true;
        return mMessagesView.getChildAt(0).getTop() == 0;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

//        mSocket.disconnect();
//
        mSocket.off(Socket.EVENT_CONNECT, onConnect);
        mSocket.off(Socket.EVENT_DISCONNECT, onDisconnect);
        mSocket.off(Socket.EVENT_CONNECT_ERROR, onConnectError);
        mSocket.off(Socket.EVENT_CONNECT_TIMEOUT, onConnectError);
        mSocket.off("updatechat", onNewMessage);
//        mSocket.off("joingroup", onUserJoined);
//        mSocket.off("user joined", onUserJoined);
        mSocket.off("user left", onUserLeft);
        mSocket.off("typing", onTyping);
        mSocket.off("stop typing", onStopTyping);
        mSocket.off("updateactiveuser", updateactiveusers);

    }


    public static void addLog(final String message) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMessages.add(new Message.Builder(Message.TYPE_LOG)
                        .message(message).build());
                mAdapter.notifyItemInserted(mMessages.size() - 1);
                scrollToBottom();
            }
        });
    }

    private void addParticipantsLog(final int numUsers) {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                addLog(getResources().getQuantityString(R.plurals.message_participants, numUsers, numUsers));
            }
        });
    }

    private static void addMessage(String username, String message) {
        mMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
                .username(username).message(message).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void addTyping(String username) {
        mMessages.add(new Message.Builder(Message.TYPE_ACTION)
                .username(username).build());
        mAdapter.notifyItemInserted(mMessages.size() - 1);
        scrollToBottom();
    }

    private void removeTyping(String username) {
        for (int i = mMessages.size() - 1; i >= 0; i--) {
            Message message = mMessages.get(i);
            if (message.getType() == Message.TYPE_ACTION && message.getUsername().equals(username)) {
                mMessages.remove(i);
                mAdapter.notifyItemRemoved(i);
            }
        }
    }


//    public static void addnewChat() {
//        update = false;
//        mSocket.on("updatehistory", new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
////                goup_id,messagehistory;
//
//                final JSONArray data = (JSONArray) args[0];
//                try {
//                    final JSONArray goup_id = (JSONArray) args[1];
//                    id = goup_id.toString();
//                    Log.v("item", goup_id.toString());
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//
////                Integer a=(Integer) args[1];
////                id= String.valueOf(a);
////                add(CommonUtils.getTempFileName(activity,"name"),id);
//                if (!update) {
//                    activity.runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            update = true;
//                            mMessages.clear();
//                            try {
//                                for (int i = data.length() - 1; i >= 0; i--) {
//                                    final JSONObject obj = (JSONObject) data.get(i);
//                                    mMessages.add(new Message.Builder(Message.TYPE_MESSAGE)
//                                            .username(obj.getString("name")).message(obj.getString("message")).build());
//                                    if (i == 0) {
//                                        last_id = obj.getString("id");
//                                    }
//                                }
//                                mAdapter = new MessageAdapter(activity, mMessages);
//                                mMessagesView.setLayoutManager(new LinearLayoutManager(activity));
//                                mMessagesView.setAdapter(mAdapter);
//                                mAdapter.setClickListener(clickListener);
//                                scrollToBottom();
//
//// mAdapter.notifyItemInserted(mMessages.size() - 1);
////
////                                    }
//                            } catch (Exception e) {
//                                Log.v("Exception", e.getMessage());
//                            }
//                        }
//                    });
//                }
//            }
//        });
//        get_all_user(id);
////        mSocket.on("updateuserofgroup", new Emitter.Listener() {
////            @Override
////            public void call(Object... args) {
////                JSONArray data = (JSONArray) args[0];
////                final ArrayList<ActiveModel> model=new ArrayList<ActiveModel>();
////                for(int i=0;i<data.length();i++)
////                    try {
////                        model.add(new ActiveModel(data.getJSONObject(i).getString("id"),
////                                data.getJSONObject(i).getString("user_id"),
////                                data.getJSONObject(i).getString("name"),
////                                data.getJSONObject(i).getString("active")));
////                    } catch (JSONException e) {
////                        e.printStackTrace();
////                    }
////                activity.runOnUiThread(new Runnable() {
////                    @Override
////                    public void run() {
////                        horizontalAdapter = new HorizontalAdapter(model, activity);
////                        horizontal_recycler_view.setAdapter(horizontalAdapter);
////                    }
////                });
////            }
////        });
//
//    }

    //    {"data":{"total":56,"per_page":20,"current_page":1,"last_page":3
//            ,"next_page_url":"http://54.200.70.163/chatcode/public/api/getalluserofgroup?page\u003d2",
//            "from":1,"to":20,
//            "data":[{"id":2,"name":"user516665","email":"a@b.com","profile_image":"1487304546-profile.jpg","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-01-04 06:54:44","updated_at":"2017-02-18 08:35:09"},{"id":137,"name":"user872407","email":"huha@b.com","profile_image":"1487395304-profile.jpg","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-18 05:20:58","updated_at":"2017-02-18 05:21:44"},{"id":135,"name":"user731629","email":"axkd@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-16 12:21:39","updated_at":"2017-02-16 12:21:39"},{"id":73,"name":"user261149","email":"ga@b.com","profile_image":"","device":"Android","device_id":"af106949444ffb2","active":1,"created_at":"2017-02-13 07:23:03","updated_at":"2017-02-16 12:19:15"},{"id":133,"name":"user138032","email":"higugiguhiha@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-16 05:06:08","updated_at":"2017-02-16 05:06:08"},{"id":132,"name":"user159360","email":"fuhdhdhdhdhifjfjda@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-16 05:01:29","updated_at":"2017-02-16 05:01:29"},{"id":131,"name":"user36433","email":"jxjdjdhhdua@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-16 04:53:16","updated_at":"2017-02-16 04:53:16"},{"id":130,"name":"user936959","email":"dhhdjdjdhchhcufudidjxa@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-16 04:50:52","updated_at":"2017-02-16 04:50:52"},{"id":129,"name":"user412090","email":"hujhrrutra@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 13:09:02","updated_at":"2017-02-15 13:09:02"},{"id":128,"name":"user616285","email":"jfhdgsegwa@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:56:45","updated_at":"2017-02-15 12:56:45"},{"id":127,"name":"user74705","email":"hdhshsjiwiytta@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:54:17","updated_at":"2017-02-15 12:54:17"},{"id":126,"name":"user81014","email":"vhxhdhshshaioiia@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:50:45","updated_at":"2017-02-15 12:50:45"},{"id":125,"name":"user871701","email":"hhahsdyyduuyta@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:43:33","updated_at":"2017-02-15 12:43:33"},{"id":124,"name":"user122243","email":"bubuhyhyhhyhyhya@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:40:15","updated_at":"2017-02-15 12:40:15"},{"id":123,"name":"user654000","email":"hshdyfyfyygrea@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:33:55","updated_at":"2017-02-15 12:33:55"},{"id":122,"name":"user292510","email":"hdjdjfjfhcjha@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:31:51","updated_at":"2017-02-15 12:31:51"},{"id":121,"name":"user460056","email":"jddjdjjdjsjsjsjsjjdjda@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:29:40","updated_at":"2017-02-15 12:29:40"},{"id":120,"name":"user70001","email":"hiiurwa@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:27:25","updated_at":"2017-02-15 12:27:25"},{"id":119,"name":"user247974","email":"jdkkdjda@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:23:43","updated_at":"2017-02-15 12:23:43"},{"id":118,"name":"user829895","email":"uububhuuhha@b.com","profile_image":"","device":"Android","device_id":"e579113950eae31c","active":1,"created_at":"2017-02-15 12:14:35","updated_at":"2017-02-15 12:14:35"}]}}
    static void get_all_user(final String group_id) {
         UserApi userApi = ApiFactory.createServiceForGson();
        userApi.get_all_user(group_id, getTempFileName(activity,"id")).enqueue(new Callback<JsonElement>() {
            @Override
            public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
                try {
                    Object resObject = new Gson().toJson(response.body());
                    Log.e(getClass().getName(), resObject.toString());
                    if (response.code() == 200) {
                        String resString = resObject.toString();
                        JSONObject obj = new JSONObject(resString);
//                        next_user_url = obj.getJSONObject("data").getString("next_page_url");
                        try {
                            JSONArray data = obj.getJSONArray("data");
                            model = new ArrayList<ActiveModel>();
                            for (int i = 0; i < data.length(); i++) {

                                firstgroup = new ArrayList<GroupObject>();
                                secondgroup = new ArrayList<GroupObject>();

                                try {
                                    for(int j=0;j<data.getJSONObject(i).getJSONObject("userinfo").getJSONArray("firstgroup").length();j++) {
                                        try {
                                            firstgroup.add(new GroupObject(
                                                    data.getJSONObject(i).getJSONObject("userinfo").getJSONArray("firstgroup").getJSONObject(j).getString("id"),
                                                    data.getJSONObject(i).getJSONObject("userinfo").getJSONArray("firstgroup").getJSONObject(j).getString("user_id"),
                                                    data.getJSONObject(i).getJSONObject("userinfo").getJSONArray("firstgroup").getJSONObject(j).getString("other_user_id"),
                                                    data.getJSONObject(i).getJSONObject("userinfo").getJSONArray("firstgroup").getJSONObject(j).getString("name")));
                                        } catch (Exception e) {
                                            Log.v("tag",e.getMessage());
                                        }

                                    }
                                } catch (Exception e) {
                                    firstgroup = new ArrayList<GroupObject>();
                                }
                                try {
                                    for(int j=0;j<data.getJSONObject(i).getJSONObject("userinfo").getJSONArray("secondgroup").length();j++) {
                                        try {
                                            secondgroup.add(new GroupObject(
                                                    data.getJSONObject(i).getJSONObject("userinfo").getJSONArray("secondgroup").getJSONObject(j).getString("id"),
                                                    data.getJSONObject(i).getJSONObject("userinfo").getJSONArray("secondgroup").getJSONObject(j).getString("user_id"),
                                                    data.getJSONObject(i).getJSONObject("userinfo").getJSONArray("secondgroup").getJSONObject(j).getString("other_user_id"),
                                                    data.getJSONObject(i).getJSONObject("userinfo").getJSONArray("secondgroup").getJSONObject(j).getString("name")));
                                        } catch (Exception e) {
                                            Log.v("tag",e.getMessage());
                                        }
                                    }
                                } catch (Exception e) {
                                    firstgroup = new ArrayList<GroupObject>();
                                }
                                model.add(new ActiveModel(data.getJSONObject(i).getString("id"),
                                        data.getJSONObject(i).getString("user_id"),
                                        data.getJSONObject(i).getJSONObject("userinfo").getString("id"),
                                        data.getJSONObject(i).getJSONObject("userinfo").getString("name"),
                                        data.getJSONObject(i).getJSONObject("userinfo").getString("profile_image"),
                                        data.getJSONObject(i).getJSONObject("userinfo").getString("active"),
                                        data.getJSONObject(i).getString("status"),
                                        firstgroup,
                                        secondgroup
                                        ));
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                        {"data":[{"id":214,"user_id":157,"group_id":121,"status":1,"updated_at":"2017-03-21 12:56:12","userinfo":{"id":157,"name":"user322182","active":1,"profile_image":"1490099366-profile.jpg","firstgroup":[],"secondgroup":[]}},{"id":215,"user_id":158,"group_id":121,"status":1,"created_at":"2017-03-21 12:58:10","updated_at":"2017-03-21 12:58:10","userinfo":{"id":158,"name":"user988725","active":1,"profile_image":"1490101062-profile.jpg","firstgroup":[],"secondgroup":[]}},{"id":216,"user_id":159,"group_id":121,"status":1,"created_at":"2017-03-21 12:59:43","updated_at":"2017-03-21 12:59:43","userinfo":{"id":159,"name":"user785181","active":0,"profile_image":"1490101199-profile.jpg","firstgroup":[],"secondgroup":[]}},{"id":219,"user_id":154,"group_id":121,"status":1,"created_at":"2017-03-22 04:33:18","updated_at":"2017-03-22 04:33:18","userinfo":{"id":154,"name":"user790139","active":1,"profile_image":"1490015094-profile.jpg","firstgroup":[],"secondgroup":[]}},{"id":336,"user_id":163,"group_id":121,"status":1,"created_at":"2017-03-27 04:46:30","updated_at":"2017-03-27 04:46:30","userinfo":{"id":163,"name":"user5696020000","active":0,
//                                "profile_image":"1490166966-profile.jpg","firstgroup":[],"secondgroup":[]}}]}
//



                        activity.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                horizontalAdapter = new HorizontalAdapter(group_id,model, activity,activity);

                                horizontal_recycler_view.setAdapter(horizontalAdapter);
                            }
                        });

                    } else if (response.code() == 500) {
                        CommonUtils.getSnackbar(parent, "Internal Server Error");
                    }
                } catch (Throwable e) {
                    onFailure(call, e);
                }
            }

            @Override
            public void onFailure(Call<JsonElement> call, Throwable t) {
                CommonUtils.getSnackbar(parent, t.getMessage());
            }
        });
    }
    public static void create(final String title, final Activity activity1, String one_to_one_id, String name, String id) {
        if (null != one_to_one_id) {
            if (one_to_one_id.equals("")) {
                String details = CommonUtils.getTempFileName(activity, "id") + "," + CommonUtils.getTempFileName(activity, "name") + "," + id;
//                socket.on('createonetogroup', function(user_id,username,other_user_id) {

                mSocket.emit("createonetogroup", CommonUtils.getTempFileName(activity, "id"), CommonUtils.getTempFileName(activity, "name"), id);
                mSocket.on("getoneonetouser", new Emitter.Listener() {
                    @Override
                    public void call(Object... args) {
                        JSONObject data = (JSONObject) args[0];
                        String id,name;
                        try {
                            id = data.getString("roomid");
                            name = data.getString("roomname");
                            Intent intent = new Intent(activity1, ActivityChatting.class);
                            intent.putExtra("group", id);
                            intent.putExtra("group_user_name", name);
                            intent.putExtra("title", title);
                            intent.putExtra("one_to_one", "1");
                            activity1.startActivity(intent);
                            activity1.finish();
                        } catch (JSONException e) {
                            return;
                        }
                    }
                });
            } else {
                Intent intent = new Intent(activity, ActivityChatting.class);
                     intent.putExtra("group", one_to_one_id);
                intent.putExtra("group_user_name", name);
                intent.putExtra("title", title);
                intent.putExtra("one_to_one", "1");
                 activity1.startActivity(intent);
                activity1.finish();

//
//                Intent intent = new Intent(mContext, ActivityChatting.class);
//                if(dataModel.getFirstgroup().size()>0) {
//                    intent.putExtra("group", dataModel.getFirstgroup().get(0).getId());
//                    intent.putExtra("group_user_name", dataModel.getFirstgroup().get(0).getName());
//
//                }else if(dataModel.getSecondgroup().size()>0) {
//                    intent.putExtra("group", dataModel.getSecondgroup().get(0).getId());
//                    intent.putExtra("group_user_name", dataModel.getSecondgroup().get(0).getName());
//                }
//                intent.putExtra("group_created_by", dataModel.getId());
//                intent.putExtra("one_to_one", "1");
//                intent.putExtra("position", position);
//                activity.startActivity(intent);

            }
        }
    }

    public static void add(final String mUsername, final String group) {
        usergroup = group;
        update = false;
        if (ActivityBeforeMainChatting.room2 == null) {
            mSocket.emit("adduser",mUsername, group,getTempFileName(activity, "id"));
            ActivityBeforeMainChatting.room2 = group;
        } else {
            mSocket.emit("switchRoom", usergroup, getTempFileName(activity, "id"));
        }
        get_all_user(usergroup);
        String next_url = Constants.BASE_URL + Constants.NEXT_PAGE_URL;
        ActivityChatting.getNextMessages(next_url, usergroup, "0");
        mSocket.on("updatechat", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String username;
                String message;
                String room;
                try {
                    username = data.getString("username");
                    message = data.getString("message");
                    room = data.getString("room");
                } catch (JSONException e) {
                    return;
                }
                if (a == 0) {
                    addLog(message);
                    a = 1;
                }
            }
        });
//        switchRooms();
    }

    public void addinbox(final String mUsername, final String group,String usergroup_name) {
        usergroup = group;
        update = false;
        if (ActivityBeforeMainChatting.room2 == null) {
            mSocket.emit("adduser", getTempFileName(activity, "name"),usergroup_name, getTempFileName(activity, "id"));
            ActivityBeforeMainChatting.room2 = group;
        } else {
            mSocket.emit("switchRoom", usergroup_name, getTempFileName(activity, "id"));
        }
        get_all_user(usergroup);
        String next_url = Constants.BASE_URL + Constants.NEXT_PAGE_URL_;
        getInboxMessages(next_url, usergroup, "0");
        mSocket.on("updatechat", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String username;
                String message;
                String room;
                try {
                    username = data.getString("username");
                    message = data.getString("message");
                    room = data.getString("room");
                } catch (JSONException e) {
                    return;
                }
                if (a == 0) {
                    addLog(message);
                    a = 1;
                }
            }
        });
//        switchRooms();
    }


    public void attemptSend() {

        Date expiration = new Date();
        try {
            expiration.setTime(convertToLocal(getIntent().getStringExtra("expiration")).getTime());
        } catch (Exception e) {

        }
        Date date = new Date(System.currentTimeMillis());
        if ((expiration.after(date))) {

            if (null == one_to_one || one_to_one.equals("")) {
                mUsername = getTempFileName(activity, "name");
                if (null == mUsername) return;
                if (null != model)
                    for (int i = 0; i < model.size(); i++) {
                        if (mUsername.equals(model.get(i).getName()))
                            if (model.get(i).getStatus().equals("1")) {
                                if (!mSocket.connected()) return;
                                mTyping = false;
                                String message = mInputMessageView.getText().toString().trim();
                                if (TextUtils.isEmpty(message)) {
                                    mInputMessageView.requestFocus();
                                    return;
                                }
//        addMessage(mUsername, message);
                                mInputMessageView.setText("");
                                // perform the sending message attempt.
                                mSocket.emit("sendchat", message, usergroup, getTempFileName(activity, "id"));
                            } else {
                                mSocket.on("stop typing", onStopTyping);
                                CommonUtils.addSnackbar(parent, "Sorry, You can't send message in this group as you are blocked by admin !!!");
                            }
                    }

            } else if (null != one_to_one && one_to_one.equals("1")) {
                mUsername = getTempFileName(activity, "name");
                if (null == mUsername) return;
                if (!mSocket.connected()) return;
                mTyping = false;
                String message = mInputMessageView.getText().toString().trim();
                if (TextUtils.isEmpty(message)) {
                    mInputMessageView.requestFocus();
                    return;
                }
                mInputMessageView.setText("");
                mSocket.emit("sendmessage", message, getTempFileName(activity, "id"), usergroup);
            }

//        for(int i=0;i<model.size();i++){
//            if(mUsername.equals(model.get(i).getName())){
//                if(model.get(i).getStatus().equals("1")){

//                }else
//                    CommonUtils.addSnackbar(parent,"You can not send message to this group");
//            }
//        }

//        mSocket.on("updatechat", new Emitter.Listener() {
//            @Override
//            public void call(final Object... args) {
//                activity.runOnUiThread(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            String username;
//                            String message;
//                            username = (String) args[0];
//                            message = (String) args[1];
//                            removeTyping(username);
//                            addMessage(username, message);
//                        }catch (Exception e){
//
//                        }
//                    }
//                });
//            }
//        });
        }else{
            {
                AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityChatting.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
                dialog.setCancelable(false);
                dialog.setMessage("You can't send message to this group anymore, group has expired");
                dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
//                    Intent intent = new Intent(ActivityChatting.this, ActivityBeforeMainChatting.class);
//                    startActivity(intent);
//                    finish();
//                    ActivityCompat.finishAffinity(ActivityChatting.this);
                        dialog.dismiss();
                    }
                });
                dialog.create().show();
            }
        }
    }

//    private void startSignIn() {
//        Intent intent = new Intent(this, ActivityBeforeMainChatting.class);
//        startActivityForResult(intent, REQUEST_LOGIN);
//    }

    public static void blockOrRemoveChat(String gr_id,String blocked_users,String type){
        mSocket.emit("blockorremove", getTempFileName(activity, "id")
                , gr_id,blocked_users,type);
        mSocket.on("updategroupusers", new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                JSONObject data = (JSONObject) args[0];
                String group_id;
                String status;
                 try {
                    group_id = data.getString("group_id");
                    status = data.getString("status");
                 } catch (JSONException e) {
                    return;
                }
            }
        });

    }


    private void leave() {
        mSocket.disconnect();
        mSocket.connect();
//        startSignIn();
    }

    public static void scrollToBottom() {
        activity.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mMessagesView.scrollToPosition(mAdapter.getItemCount() - 1);
            }
        });
    }

    private Emitter.Listener onConnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if (!isConnected) {
//                        if (null != mUsername)
//                            mSocket.emit("adduser", mUsername, usergroup, getTempFileName(activity, "id"));
//                            mSocket.emit("adduser", mUsername);
//                        Toast.makeText(getApplicationContext(),
//                                R.string.connect, Toast.LENGTH_LONG).show();
                        isConnected = true;
                    }
                }
            });
        }
    };

    private Emitter.Listener onDisconnect = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    isConnected = false;
                    Toast.makeText(getApplicationContext(),
                            R.string.disconnect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };

    private Emitter.Listener onConnectError = new Emitter.Listener() {
        @Override
        public void call(Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getApplicationContext(),
                            R.string.error_connect, Toast.LENGTH_LONG).show();
                }
            });
        }
    };


    public void showMessage(){
        Date date = new Date(System.currentTimeMillis());
        if ((date.after(date1))){
            AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityChatting.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
            dialog.setCancelable(false);
            dialog.setMessage("You can't send message to this group anymore, time limit has expired");
            dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int id) {
//                    Intent intent = new Intent(ActivityChatting.this, ActivityBeforeMainChatting.class);
//                    startActivity(intent);
//                    finish();
//                    ActivityCompat.finishAffinity(ActivityChatting.this);
                    dialog.dismiss();
                }
            });
            dialog.create().show();
    }else{
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Timer timer = new Timer();
                    timer.schedule(new MyTimeTask(), date1);
                }});
        }
    }

    private Emitter.Listener onNewMessage = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
//                    String val=created_by.equals(CommonUtils.getTempFileName(ActivityChatting.this, "id"))+","+
//                            getConverteddate(after_days).after(getConverteddate(getCurrentDate()))+","+
//                                    after_days.equals("0")+","+date.after(getConverteddate(getCurrentDate()));
//                    Log.v("val",val);
//                     if ((!after_days.equals("0")&&!created_by.equals(CommonUtils.getTempFileName(ActivityChatting.this, "id"))&&(
//                            getConverteddate(after_days).after(getConverteddate(getCurrentDate()))))||
//                                    (after_days.equals("0")&&!date.equals(null)&&date.after(getConverteddate(getCurrentDate())))) {

//                    Date date = new Date(System.currentTimeMillis());
//                    if (null!=date1&&(date.before(date1))){


                    JSONObject data = (JSONObject) args[0];
                        String username;
                        String message;
                        String room;
                        Log.v("data", data.toString());
                        try {
                            username = data.getString("username");
                            message = data.getString("message");
                            room = data.getString("room");
                        } catch (JSONException e) {
                            return;
                        }
                        removeTyping(username);
                        if (!username.equals(""))
                            addMessage(username, message);
//                    }else{
//                        AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityChatting.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
//                        dialog.setCancelable(false);
//                        dialog.setMessage("Your time limit has expired");
//                        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int id) {
//                                Intent intent = new Intent(ActivityChatting.this, ActivityBeforeMainChatting.class);
//                                startActivity(intent);
//                                finish();
//                                ActivityCompat.finishAffinity(ActivityChatting.this);
//                            }
//                        });
//                        final AlertDialog alert = dialog.create();
//                        alert.show();
//                    } else{
//                        AlertDialog.Builder dialog = new AlertDialog.Builder(ActivityChatting.this, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
//                        dialog.setCancelable(false);
//                        dialog.setMessage("You can't send message to this group anymore, time limit has expired");
//                        dialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
//                            @Override
//                            public void onClick(DialogInterface dialog, int id) {
//                                Intent intent = new Intent(ActivityChatting.this, ActivityBeforeMainChatting.class);
//                                startActivity(intent);
//                                finish();
//                                ActivityCompat.finishAffinity(ActivityChatting.this);
//                            }
//                        });
//                        final AlertDialog alert = dialog.create();
//                        alert.show();
//                    }
                }
            });
        }
    };

    private Emitter.Listener onUserJoined = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        return;
                    }

                    addLog(getResources().getString(R.string.message_user_joined, username));
                    addParticipantsLog(numUsers);
                }
            });
        }
    };

    private Emitter.Listener onUserLeft = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username;
                    int numUsers;
                    try {
                        username = data.getString("username");
                        numUsers = data.getInt("numUsers");
                    } catch (JSONException e) {
                        return;
                    }

                    addLog(getResources().getString(R.string.message_user_left, username));
                    addParticipantsLog(numUsers);
                    removeTyping(username);
                }
            });
        }
    };

    private Emitter.Listener onTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username, room;
                    try {
                        username = data.getString("username");
                        room = data.getString("room");
                    } catch (JSONException e) {
                        return;
                    }
//                    if(room.equals(usergroup))
                    addTyping(username);
                }
            });
        }
    };


    private Emitter.Listener updateactiveusers = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String user_id, username = "", status="";
                    try {
                        user_id = data.getString("user_id");
                        status = "1";
                    } catch (JSONException e) {
                        user_id = "";
                        try {
                            username = data.getString("username");
                        } catch (JSONException e1) {
                            username = "";
                            e1.printStackTrace();
                        }
                    }
                    if (null != model) {
                        for (int i = 0; i < model.size(); i++) {
                             if (!user_id.equals("") && model.get(i).getUser_id().equals(user_id)) {
                                model.get(i).setActive("1");
//                                 model.get(i).setStatus(status);
                                if (null != horizontalAdapter)
                                    horizontalAdapter.notifyItemChanged(i);
                                break;
                            } else if (!username.equals("") && model.get(i).getName().equals(username)) {
                                model.get(i).setActive("0");
//                                 model.get(i).setStatus(status);
                                 if (null != horizontalAdapter)
                                    horizontalAdapter.notifyItemChanged(i);
                                break;
                            }
                        }
                    }
                }
            });
        }
    };


    private Emitter.Listener onStopTyping = new Emitter.Listener() {
        @Override
        public void call(final Object... args) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    JSONObject data = (JSONObject) args[0];
                    String username, room;
                    try {
                        username = data.getString("username");
                        room = data.getString("room");
                    } catch (JSONException e) {
                        return;
                    }
//                    if(room.equals(usergroup))
                    removeTyping(username);
                }
            });
        }
    };

    private Runnable onTypingTimeout = new Runnable() {
        @Override
        public void run() {
            if (!mTyping) return;

            mTyping = false;
            mSocket.emit("stop typing");
        }
    };

    @Override
    public void onClick(View view, int position) {
        switch (view.getId()) {
            case R.id.username:
                final Dialog dialog = new Dialog(context);
                dialog.setContentView(R.layout.show_image_dialog);
                dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                RoundRectCornerImageView imageView = (RoundRectCornerImageView) dialog.findViewById(R.id.image_View);
                Button back = (Button) dialog.findViewById(R.id.cancel);
                TextView name = (TextView) dialog.findViewById(R.id.name);
                for (int i = 0; i < ActivityChatting.model.size(); i++) {
                    if (ActivityChatting.model.get(i).getName().equals(mMessages.get(position).getUsername())) {
                        if (!ActivityChatting.model.get(i).getImage().equals("")) {
                            String path = Constants.IMAGES_URL + ActivityChatting.model.get(i).getImage();
                            Picasso.with(context)
                                    .load(path)
                                    .resize(100, 100)
                                    .centerCrop()
                                    .error(R.drawable.user)
                                    .into(imageView);
                        }
                        name.setText(ActivityChatting.model.get(i).getName());
                        back.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                dialog.dismiss();
                            }
                        });
                    }
                }
                dialog.show();
                break;
        }
    }

    public static void showDialog(){
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.show_message_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Button back = (Button) dialog.findViewById(R.id.cancel);
        Button readonly = (Button) dialog.findViewById(R.id.readonly);
        back.setText("OK");
        readonly.setVisibility(View.GONE);
        TextView message = (TextView) dialog.findViewById(R.id.message);
        message.setText("Can not perform this action !!");
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        if(!((Activity) context).isFinishing())
        {
            dialog.show();
        }
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cover:
//                CommonUtils.Image_Picker_Dialog(ActivityChatting.this);

                if (!gotRandomizer) {
                    try {
                        iabHelper.launchPurchaseFlow(ActivityChatting.this, "next", 101, purchaseFinishedListener, "");
                    } catch (IabHelper.IabAsyncInProgressException e) {
                        e.printStackTrace();
                        Log.e(TAG, "Error launching purchase flow. Another async operation in progress.");
                    }

                }

                break;
        }
    }

    private class MyTimeTask extends TimerTask
    {

        public void run()
        {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    if(ChatApplication.isActivityVisible()&&null!=date1)
                        showMessage();
                }});
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            Bitmap bitmap = null;
            if (requestCode == 01) {
                Uri uri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == 11) {
                bitmap = (Bitmap) data.getExtras().get("data");
            }
            Bitmap bmap = CommonUtils.resizeImage(cover, bitmap);
            file = CommonUtils.createDirectoryAndSaveFile(bmap, "image.jpg", ActivityChatting.this);
            if(null!=file)
                submitDetails(file);
        }

    }

    void submitDetails(File _image) {
        final ProgressDialog progressDialog=CommonUtils.setProgress("Uploading  !!!",this);
        UserApi userApi     = ApiFactory.createServiceForGson();
        String fileName = "";
        RequestBody requestBody= null;
        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        if(null!=_image) {
            String image_name = _image.getName();
            fileName = "banner\"; filename=\"" + image_name;
            requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), _image);
            requestBodyMap.put(fileName, requestBody);
        }
//        1492150598380

        RequestBody usergroup_     = RequestBody.create(MediaType.parse("multipart/form-data"),usergroup);
        RequestBody user_id        = RequestBody.create(MediaType.parse("multipart/form-data"), CommonUtils.getTempFileName(ActivityChatting.this,"id"));
        requestBodyMap.put("group_id", usergroup_);
        requestBodyMap.put("user_id", user_id);
        userApi.uploadBanner(requestBodyMap).enqueue(new Callback<UploadModel>() {
            @Override
            public void onResponse(Call<UploadModel> call, Response<UploadModel> response) {
                if (response.body() != null) {
                    try {
                        if (response.body().getMessage().equals("success")) {
                            CommonUtils.addSnackbar(parent, "Succesfully uploaded");
                            banner_icon=response.body().getNew_image();
                        } else {
                            CommonUtils.addSnackbar(parent, "Unable to upload");
                        }
                    }catch (Exception e){
                        CommonUtils.addSnackbar(parent, "Unable to upload");
                    }
                }
                progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<UploadModel> call, Throwable t) {
                CommonUtils.addSnackbar(parent, t.getMessage());
                progressDialog.dismiss();
            }
        });
    }

    @Override
    public void receivedBroadcast() {
        Log.e(TAG, "Received broadcast notification. Querying inventory.");
        try {
            iabHelper.queryInventoryAsync(gotInventoryListener);
        } catch (IabHelper.IabAsyncInProgressException e) {
            Log.d(TAG, "Error querying inventory. Another async operation in progress.");
        }
    }
}

