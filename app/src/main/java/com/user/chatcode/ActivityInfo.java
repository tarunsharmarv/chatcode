package com.user.chatcode;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.model.UploadModel;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;
import com.user.chatcode.utils.TimeSpan;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.http.Url;

import static android.icu.lang.UCharacter.GraphemeClusterBreak.T;
import static com.user.chatcode.ActivityMainChatting.listdata;

/**
 * Created by user on 18/2/17.
 */

public class ActivityInfo extends AppCompatActivity implements View.OnClickListener {
    TextView title, photos, max_capacity, reset, limit, expiration, share,expires,enter;
    ImageView qrcode;
    Button crosschatcode;
    URI uri;
    int position;
    String code_id;
    ScrollView parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_success);
        findViewById(R.id.enter).setOnClickListener(this);
        parent = (ScrollView) findViewById(R.id.parent);
        title = (TextView) findViewById(R.id.title);
        photos = (TextView) findViewById(R.id.photos);
        max_capacity = (TextView) findViewById(R.id.max_capacity);
        reset = (TextView) findViewById(R.id.reset);
        limit = (TextView) findViewById(R.id.limit);
        expiration = (TextView) findViewById(R.id.expiration);
        share = (TextView) findViewById(R.id.share);
        expires = (TextView) findViewById(R.id.expires);
        enter =(TextView) findViewById(R.id.enter);
        qrcode = (ImageView) findViewById(R.id.qrcode);
        crosschatcode = (Button) findViewById(R.id.crosschatcode);
        position = getIntent().getIntExtra("position", 0);
        title.setText(listdata.get(position).getName());
        photos.setText(listdata.get(position).getPhotos());
        max_capacity.setText(listdata.get(position).getMax_user());
        if(listdata.get(position).getReset_on().equals("")){
            reset.setText("NA");
        }
        reset.setText(listdata.get(position).getReset_on());
        limit.setText(listdata.get(position).getTime_limit());
        if(!listdata.get(position).getExpiration().equals("9999-01-01 00:00:00")) {
            expiration.setText(CommonUtils.convertDateFromOneFormatToAnother(listdata.get(position).getExpiration(), "yyyy-MM-dd HH:mm:ss", "MMM dd,yyyy"));
            expires.setText(CommonUtils.convertDateFromOneFormatToAnother(listdata.get(position).getExpiration(), "yyyy-MM-dd HH:mm:ss", "dd/MM/yy"));
        }else{
            expiration.setText("NA");
            expires.setText("NA");
        }
        crosschatcode.setOnClickListener(this);
        share.setOnClickListener(this);
        Picasso.with(this).load(listdata.get(position).getGroup_icon()).into(qrcode);
//        addcounter();
    }



    public void addcounter(){
        final Calendar future = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:dd");
        Date date = null;
        try {
            date = dateFormat.parse(listdata.get(position).getExpiration());
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            future.setTime(date); // month is zero based
            listdata.get(position).getExpiration();
            Date now = Calendar.getInstance().getTime();
            final TimeSpan timeSpan = TimeSpan.fromMiliseconds(future.getTimeInMillis() - now.getTime());
//                        Log.d(TAG, "Days left : " + timeSpan.getDays()+" Days "+timeSpan.getHours()+" hour "+timeSpan.getMinutes()+" min "+timeSpan.getSeconds()+" sec "+); // "Days left : 730"
            Timer T = new Timer();
            if(!(timeSpan.getDays()<= 0)) {
                T.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                expires.setText(timeSpan.getDays() + " Days " + timeSpan.getHours() + " hour " + timeSpan.getMinutes() + " min " + timeSpan.getSeconds() + " sec ");
                            }
                        });
                    }
                }, 1000, 1000);
            }else{
                expires.setText("Expired");
            }
        }catch (Exception e){
            expires.setText("-");
        }
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.crosschatcode:
                this.finish();
                break;
            case R.id.share:
                share(listdata.get(position).getName()+" - "+ listdata.get(position).getGroup_icon());
                break;
            case R.id.enter:
                this.finish();
                break;
        }
    }

    private void share( String text) {
        startActivity(Intent.createChooser(CommonUtils.initPhotoShareIntent(text), "Share to"));
    }

    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
    }
}