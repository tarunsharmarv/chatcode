package com.user.chatcode.chatting;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.user.chatcode.ActivityChatting;
import com.user.chatcode.ActivityCreateSuccess;
import com.user.chatcode.ActivityMainChatting;
import com.user.chatcode.ActivityMyProfile;
import com.user.chatcode.R;
import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.model.ChattingListModel;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import io.socket.client.Socket;
import io.socket.emitter.Emitter;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.user.chatcode.ActivityChatting.created_by;
import static com.user.chatcode.ActivityMainChatting.get_group_user;
import static com.user.chatcode.ActivityMainChatting.listdata;
import static com.user.chatcode.ActivityMainChatting.next_url;
import static com.user.chatcode.firebase.MyFirebaseInstanceIDService.refreshedToken;
import static com.user.chatcode.utils.CommonUtils.getTempFileName;

/**
 * Created by user on 9/2/17.
 */

public class ActivityBeforeMainChatting extends AppCompatActivity {
    private static String mUsername;
     public static Socket mSocket;
    static Intent i1;
     public static String id;
    static Activity activity;

    public static String room2=null;
    public static  ArrayList<ChattingListModel> data;
     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
         if(ActivityMainChatting.tabSelected==3)
         ActivityMainChatting.tabSelected=1;
         setContentView(R.layout.activity_before_mainchatting);
        ChatApplication app = (ChatApplication) getApplication();
        mSocket = app.getSocket();
         activity=ActivityBeforeMainChatting.this;
         updatetoken();
         i1 = new Intent(ActivityBeforeMainChatting.this, ActivityMainChatting.class);
//        mSocket.on("login", onLogin);

         mSocket.connect();
        getDummyData1();

     }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        mSocket.off("login", onLogin);
    }




    public static void createRooms(String name){
        String username= getTempFileName(activity,"name");
        mSocket.emit("create", name,username);
        listdata=get_group_user("", getTempFileName(activity,"id"),"1");

    }
    public  void getDummyData1() {
        String id = getTempFileName(ActivityBeforeMainChatting.this, "id");
        String name = getTempFileName(ActivityBeforeMainChatting.this, "name");
        startActivity(i1);
    }
public  void  updatetoken() {
    UserApi userApi = ApiFactory.createServiceForGson();
    userApi.updateToken(refreshedToken, getTempFileName(activity,"id")).enqueue(new Callback<JsonElement>() {
        @Override
        public void onResponse(Call<JsonElement> call, Response<JsonElement> response) {
            try {
                if(response.code()==200) {
                    if(null!=response.body()) {
                        Object resObject = new Gson().toJson(response.body());
                        String resString = resObject.toString();
                        JSONObject obj = new JSONObject(resString);
                     }
                }else if(response.code()==500){
                }
            } catch (Throwable e) {
                onFailure(call, e);
            }
        }
        @Override
        public void onFailure(Call<JsonElement> call, Throwable t) {
        }
    });
}


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        mSocket.disconnect();
        this.finish();
    }
}
