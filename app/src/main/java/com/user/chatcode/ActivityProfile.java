package com.user.chatcode;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;

import com.squareup.picasso.Picasso;
import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.model.UploadModel;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.user.chatcode.R.id.topView;

/**
 * Created by user on 21/12/16.
 */
public class ActivityProfile extends AppCompatActivity  implements View.OnClickListener {
    EditText edit_user;
    ScrollView parent;
    File profileFile;
//    RelativeLayout topView;
    ImageView profile_image;
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        parent =(ScrollView)findViewById(R.id.parent);
        edit_user=(EditText)findViewById(R.id.edit_user);
//        topView=(RelativeLayout)findViewById(R.id.topView);
//        findViewById(R.id.button_take_new).setOnClickListener(this);
//        findViewById(R.id.button_choose).setOnClickListener(this);
//        findViewById(R.id.button_cancel).setOnClickListener(this);
        profile_image=(ImageView)findViewById(R.id.profile_image);
        Picasso.with(ActivityProfile.this).
                load(Constants.IMAGES_URL+CommonUtils.getTempFileName(ActivityProfile.this,"profile_image")).
                resize(200,200).
                centerCrop().
                into(profile_image);
        edit_user.setText(CommonUtils.getTempFileName(this,"name"));
        findViewById(R.id.next).setOnClickListener(this);
        profile_image.setOnClickListener(this);
//        parent.setOnScrollChangeListener(new View.OnScrollChangeListener() {
//            @Override
//            public void onScrollChange(View view, int i, int i1, int i2, int i3) {
//                if(topView.getVisibility()==View.VISIBLE)
//                    topView.setVisibility(View.GONE);
//            }
//        });
    }
    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.next:
                String user=edit_user.getText().toString();
                if(null!=profileFile&&!user.equals(""))
                    submitDetails(user,CommonUtils.getTempFileName(this,"id"),profileFile);
                else if(!CommonUtils.getTempFileName(ActivityProfile.this,"profile_image").equals("")&&!user.equals("")) {
                    Intent i = new Intent(ActivityProfile.this, ActivityBeforeMainChatting.class);
                    startActivity(i);
                }else
                    CommonUtils.getSnackbar(parent,"Please select your username and Profile Image");
                break;
            case R.id.profile_image:
//                topView.setVisibility(View.VISIBLE);
                CommonUtils.Image_Picker_Dialog(this);
                break;
//            case R.id.button_take_new:
//                Intent pictureActionIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                startActivityForResult(pictureActionIntent, 1);
//                topView.setVisibility(View.GONE);
//                break;
//            case R.id.button_choose:
//                Intent pictureActionIntent1 = new Intent(Intent.ACTION_GET_CONTENT, null);
//                pictureActionIntent1.setType("image/*");
//                pictureActionIntent1.putExtra("return-data", true);
//                startActivityForResult(pictureActionIntent1, 0);
//                topView.setVisibility(View.GONE);
//                break;
//            case R.id.button_cancel:
//                topView.setVisibility(View.GONE);
//                break;
        }
    }

    void submitDetails(final String name,String id, File profileImage ) {
        UserApi userApi     = ApiFactory.createServiceForGson();
        String image_name1 = profileImage.getName();
        String fileName1 = "profile_image\"; filename=\"" + image_name1;
        RequestBody username_        = RequestBody.create(MediaType.parse("multipart/form-data"),name);
        RequestBody user_id_        = RequestBody.create(MediaType.parse("multipart/form-data"), id);
        RequestBody requestBody3    = RequestBody.create(MediaType.parse("multipart/form-data"), profileImage);
        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        requestBodyMap.put("username", username_);
        requestBodyMap.put("user_id", user_id_);
        requestBodyMap.put(fileName1, requestBody3);
        userApi.update_profile(requestBodyMap).enqueue(new Callback<UploadModel>() {
            @Override
            public void onResponse(Call<UploadModel> call, Response<UploadModel> response) {
                if(response.body().getMessage().equals("success")) {
                    CommonUtils.getSnackbar(parent,response.body().getMessage());
                    CommonUtils.saveTempFileName(ActivityProfile.this,"name",name);
                    CommonUtils.saveTempFileName(ActivityProfile.this,"profile_image",response.body().getNew_image());
                    Intent i = new Intent(ActivityProfile.this, ActivityBeforeMainChatting.class);
                    startActivity(i);
                }

            }
            @Override
            public void onFailure(Call<UploadModel> call, Throwable t) {
                //                CommonUtils.getSnackbar(layoutParent, t.getMessage());
                CommonUtils.getSnackbar(parent,t.getMessage());
            }
        });
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(resultCode==RESULT_OK) {
            Bitmap bitmap = null;
            if (requestCode == 01) {
                Uri uri = data.getData();
                try {
                    bitmap = MediaStore.Images.Media.getBitmap(getContentResolver(), uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (requestCode == 11) {
                bitmap = (Bitmap) data.getExtras().get("data");
            }
            Bitmap bmap=CommonUtils.resizeImage(profile_image,bitmap);
            profile_image.setImageBitmap(bmap);
            profileFile=CommonUtils.createDirectoryAndSaveFile(bmap,"profile.jpg",ActivityProfile.this);
        }
    }
}
