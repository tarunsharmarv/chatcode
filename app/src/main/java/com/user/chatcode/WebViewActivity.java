package com.user.chatcode;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.user.chatcode.utils.Constants;

import java.util.Locale;

public class WebViewActivity extends AppCompatActivity {
    WebView wv1;
    String language;
    static ConnectivityManager cm;
    AlertDialog dailog;
    AlertDialog.Builder build;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view_activity);
          language= Locale.getDefault().getDisplayLanguage();
//        getSupportActionBar().setDisplayShowHomeEnabled(true);
//        getSupportActionBar().hide();
        checkInternetSettings();
    }


    @Override

    public void onResume(){
        super.onResume();
        if(null!=dailog&&dailog.isShowing()){
            dailog.dismiss();
        }
        checkInternetSettings();
    }
    private void checkInternetSettings() {
        cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        build = new AlertDialog.Builder(WebViewActivity.this);

        if (cm.getNetworkInfo(ConnectivityManager.TYPE_WIFI)
                .isConnectedOrConnecting()
                || cm.getNetworkInfo(ConnectivityManager.TYPE_MOBILE)
                .isConnectedOrConnecting()// if connection is
            // there screen goes
            // to next screen
            // else shows
            // message toast
                ) {
            initWebPage();
        } else {
            build.setMessage("This application requires Internet connection.Would you connect to internet ?");
            build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                     startActivity(new Intent(Settings.ACTION_SETTINGS));

                }
            });
            build.setNegativeButton("No", new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // TODO Auto-generated method stub
                    build.setMessage("Are sure you want to exit?");
                    build.setPositiveButton("Yes", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            finish();
                        }
                    });
                    build.setNegativeButton("NO", new DialogInterface.OnClickListener() {

                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            // TODO Auto-generated method stub
                            dialog.dismiss();
                            checkInternetSettings();

                        }
                    });
                    dailog = build.create();
                    dailog.show();
                }
            });
            dailog = build.create();
            dailog.show();

        }


    }

    @Override
    protected void onSaveInstanceState(Bundle outState )
    {
        super.onSaveInstanceState(outState);
        if(null!=wv1)
        wv1.saveState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState)
    {
        super.onRestoreInstanceState(savedInstanceState);
        wv1.restoreState(savedInstanceState);
    }
    public void initWebPage(){
        wv1=(WebView)findViewById(R.id.webview);

        wv1.setWebChromeClient(new WebChromeClient() {
            private ProgressDialog mProgress;

            @Override
            public void onProgressChanged(WebView view, int progress) {
                Log.v("progreess",progress+"");
                if (mProgress == null) {
                    mProgress = new ProgressDialog(WebViewActivity.this);
                    if(findViewById(R.id.splash).getVisibility()==View.GONE)
                    mProgress.show();
                }
                mProgress.setMessage("Loading... ");
                if (progress>35) {

                    if(findViewById(R.id.splash).getVisibility()==View.GONE)
                    mProgress.dismiss();
                    else{
                        findViewById(R.id.splash).setVisibility(View.GONE);
                        mProgress = null;
                    }

                }
            }
        });
        wv1.getSettings().setLoadsImagesAutomatically(true);
        wv1.getSettings().setJavaScriptEnabled(true);
        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

//        if(language.equals("English"))
            wv1.loadUrl(Constants.web_url);
//        else  if(language.equals("português"))
//            wv1.loadUrl(Constants.web_url2);
//        else  if(language.equals("español"))
//            wv1.loadUrl(Constants.web_url3);
//        else
//            wv1.loadUrl(Constants.web_url1);
            wv1.setWebViewClient(new WebViewClient()
            {
                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {
                    if (url != null && url.startsWith("whatsapp://")) {
                        view.getContext().startActivity(
                                new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        return true;
                    } else {
                        return false;
                    }
                }
            });
    }



    @Override
    public void onBackPressed() {
        if(wv1.canGoBack()) {
            wv1.goBack();
        } else {
            super.onBackPressed();
        }
    }

//    public void initWebPage(){
//        WebView wv1=(WebView)findViewById(R.id.webview);
//        wv1.setWebViewClient(new MyBrowser());
//        wv1.getSettings().setLoadsImagesAutomatically(true);
//        wv1.getSettings().setJavaScriptEnabled(true);
//        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        wv1.loadUrl(Constants.web_url);
//    }

    class MyBrowser extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }
    }
//    public void initWebPage(){
//        WebView wv1=(WebView)findViewById(R.id.webview);
//        wv1.setWebViewClient(new WebViewClient() {
//
//            @Override
//            public void onPageFinished(WebView view, String url) {
//                findViewById(R.id.splash).setVisibility(View.GONE);
//            }
//        });
//
//        wv1.getSettings().setLoadsImagesAutomatically(true);
//        wv1.getSettings().setJavaScriptEnabled(true);
//        wv1.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
//        wv1.loadUrl(Constants.web_url);
//    }
public  boolean isOnline(Context context) {
    ConnectivityManager cm = (ConnectivityManager)context. getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo netInfo = cm.getActiveNetworkInfo();
    return netInfo != null && netInfo.isConnectedOrConnecting(); }
}
