package com.user.chatcode;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.MotionEvent;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.GridView;
import android.widget.LinearLayout;

import com.user.chatcode.adapter.DialerAdater;
import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.model.LoginModel;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by user on 21/12/16.
 */
public class ActivityLogin extends AppCompatActivity implements View.OnClickListener {
    EditText numberDialer;
    LinearLayout parent;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_login);
        checkPermision();
        if(null!=CommonUtils.getTempFileName(ActivityLogin.this,"logged_in")&&
                CommonUtils.getTempFileName(ActivityLogin.this,"logged_in").equals("1")){
            Intent i=new Intent(this, ActivityBeforeMainChatting.class);
            startActivity(i);
            this.finish();
        }
        initView();
    }



    void checkPermision() {
        int currentapiVersion = android.os.Build.VERSION.SDK_INT;
        if (currentapiVersion >= 23) {
            if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ActivityLogin.this, new String[]{android.Manifest.permission.CAMERA}, 1);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ActivityLogin.this, new String[]{android.Manifest.permission.WRITE_EXTERNAL_STORAGE}, 1);
            }
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(ActivityLogin.this, new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, 1);
            }
        }
    }
    @Override
    public boolean onTouchEvent(MotionEvent event) {
        InputMethodManager imm = (InputMethodManager)getSystemService(Context.
                INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        return true;
    }

    private void initView() {
        parent                 = (LinearLayout)  findViewById(R.id.parent);
        numberDialer           = (EditText)      findViewById(R.id.numberDialer);
//        numberDialer.setText("a@b.com");
        findViewById(R.id.cancel).setOnClickListener(this);
        findViewById(R.id.next).setOnClickListener(this);
        numberDialer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(numberDialer.getText().toString().equals(""))
                    findViewById(R.id.cancel).setVisibility(View.GONE);
                    else
                    findViewById(R.id.cancel).setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cancel:
//                numberDialer.setText(numberDialer.getText().toString().substring(0,numberDialer.getText().toString().length()-1));
                numberDialer.setText("");
                break;
            case R.id.next:
                String text=numberDialer.getText().toString();
                if(!text.equals("")) {
                    if(CommonUtils.checkEmail(text)){
                        getemailVerification(text);
                    }else
                        getphoneVerification(text);
                }else{
                    CommonUtils.addSnackbar(parent,"Please add phone Number !!");
                }
                break;
        }
    }




    void getemailVerification(final String email) {
         UserApi userApi = ApiFactory.createServiceForGson();
        userApi.send_verification_email(email).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                try {
                    if(response.code()==200) {
                        if(null!=response.body().getCode()) {
                            CommonUtils.saveTempFileName(ActivityLogin.this,"code",response.body().getCode());
                            CommonUtils.saveTempFileName(ActivityLogin.this,"email",email);
                            CommonUtils.saveTempFileName(ActivityLogin.this,"type","1");
                            Intent i = new Intent(ActivityLogin.this, ActivityConfirmation.class);
                            i.putExtra("phone_number", numberDialer.getText().toString());
                            startActivity(i);
                        }
                    }else if(response.code()==500){
                        CommonUtils.getSnackbar(parent, "Internal Server Error");
                    }
                } catch (Throwable e) {
                    onFailure(call, e);
                }
            }
            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                CommonUtils.getSnackbar(parent,t.getMessage());
             }
        });
    }

    void getphoneVerification(final String phone) {
        UserApi userApi = ApiFactory.createServiceForGson();
        userApi.send_verification_phone(phone).enqueue(new Callback<LoginModel>() {
            @Override
            public void onResponse(Call<LoginModel> call, Response<LoginModel> response) {
                try {
                    if(response.code()==200) {
//                        try {
                            if (null != response.body().getCode()) {
                                CommonUtils.saveTempFileName(ActivityLogin.this, "code", response.body().getCode());
                                CommonUtils.saveTempFileName(ActivityLogin.this, "email", phone);
                                CommonUtils.saveTempFileName(ActivityLogin.this, "type", "2");
                                Intent i = new Intent(ActivityLogin.this, ActivityConfirmation.class);
                                i.putExtra("phone_number", numberDialer.getText().toString());
                                startActivity(i);
                            }else  if (null != response.body().getError()) {
                                CommonUtils.getSnackbar(parent, "Please Check you number");
                            }
//                        }catch (Exception e){
//                            CommonUtils.getSnackbar(parent, "Please Check you number");
//                        }
                    }else if(response.code()==500){
                        CommonUtils.getSnackbar(parent, "Internal Server Error");
                    }
                } catch (Throwable e) {
                    onFailure(call, e);
                }
            }
            @Override
            public void onFailure(Call<LoginModel> call, Throwable t) {
                CommonUtils.getSnackbar(parent,t.getMessage());
            }
        });
    }
}