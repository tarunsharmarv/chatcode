package com.user.chatcode.model;

import java.util.ArrayList;

/**
 * Created by user on 13/2/17.
 */

public class ActiveModel {
        String id,user_id,name,active,image,status,other_user_id;
    ArrayList<GroupObject> firstgroup,secondgroup;

    public ArrayList<GroupObject> getFirstgroup() {
        return firstgroup;
    }

    public void setFirstgroup(ArrayList<GroupObject> firstgroup) {
        this.firstgroup = firstgroup;
    }

    public ArrayList<GroupObject> getSecondgroup() {
        return secondgroup;
    }

    public void setSecondgroup(ArrayList<GroupObject> secondgroup) {
        this.secondgroup = secondgroup;
    }

    public ActiveModel(String id, String user_id,String other_user_id, String name,String image,  String active, String status, ArrayList<GroupObject> firstgroup, ArrayList<GroupObject> secondgroup) {
        this.id = id;
        this.user_id = user_id;
        this.other_user_id=other_user_id;
        this.name = name;
        this.active = active;
        this.image = image;
        this.status = status;
        this.firstgroup = firstgroup;
        this.secondgroup = secondgroup;
    }

    public String getOther_user_id() {
        return other_user_id;
    }

    public void setOther_user_id(String other_user_id) {
        this.other_user_id = other_user_id;
    }


    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }
    public String getUser_id() {
        return user_id;
    }
    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }
}
