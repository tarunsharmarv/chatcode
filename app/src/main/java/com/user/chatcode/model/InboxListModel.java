package com.user.chatcode.model;

import java.util.ArrayList;

/**
 * Created by user on 23/12/16.
 */

public class InboxListModel {
    String id,name,email,phone_number,profile_image,device,device_id,active,gcm_token,push_notification,created_at,updated_at,lastmessage;
    ArrayList<GroupObject> firstgroup,secondgroup;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getPush_notification() {
        return push_notification;
    }

    public void setPush_notification(String push_notification) {
        this.push_notification = push_notification;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public ArrayList<GroupObject> getFirstgroup() {
        return firstgroup;
    }

    public void setFirstgroup(ArrayList<GroupObject> firstgroup) {
        this.firstgroup = firstgroup;
    }

    public ArrayList<GroupObject> getSecondgroup() {
        return secondgroup;
    }

    public void setSecondgroup(ArrayList<GroupObject> secondgroup) {
        this.secondgroup = secondgroup;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getGcm_token() {
        return gcm_token;
    }

    public void setGcm_token(String gcm_token) {
        this.gcm_token = gcm_token;
    }

    public String getLastmessage() {
        return lastmessage;
    }

    public void setLastmessage(String lastmessage) {
        this.lastmessage = lastmessage;
    }

    public InboxListModel(String id, String name, String email, String phone_number, String profile_image, String device, String device_id, String active, String gcm_token, String push_notification, String created_at, String updated_at, String lastmessage, ArrayList<GroupObject> firstgroup, ArrayList<GroupObject> secondgroup) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone_number = phone_number;
        this.profile_image = profile_image;
        this.device = device;
        this.device_id = device_id;
        this.active = active;
        this.gcm_token = gcm_token;
        this.push_notification = push_notification;
        this.created_at = created_at;
        this.updated_at = updated_at;
        this.lastmessage = lastmessage;
        this.firstgroup = firstgroup;
        this.secondgroup = secondgroup;
    }
}
