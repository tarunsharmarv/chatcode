package com.user.chatcode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 4/1/17.
 */
public class LoginModel {
    @SerializedName("code")
    @Expose
    String code;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("error")
    @Expose
    String error;

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
