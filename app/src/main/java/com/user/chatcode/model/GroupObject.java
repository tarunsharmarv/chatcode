package com.user.chatcode.model;

/**
 * Created by user on 12/4/17.
 */

public class GroupObject {
    String id,user_id,other_user_id,name,created_at;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getOther_user_id() {
        return other_user_id;
    }

    public void setOther_user_id(String other_user_id) {
        this.other_user_id = other_user_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public GroupObject(String id, String user_id, String other_user_id, String name ) {
        this.id = id;
        this.user_id = user_id;
        this.other_user_id = other_user_id;
        this.name = name;
     }
}
