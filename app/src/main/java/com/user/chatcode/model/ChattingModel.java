package com.user.chatcode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import static android.R.attr.id;

/**
 * Created by user on 13/2/17.
 */

public class ChattingModel {


    @SerializedName("id")
    @Expose
    String id;
    @SerializedName("user_id")
    @Expose
    String user_id;
    @SerializedName("group_id")
    @Expose
    String group_id;
    @SerializedName("message")
    @Expose
    String message;
    @SerializedName("created_at")
    @Expose
    String created_at;
    @SerializedName("updated_at")
    @Expose
    String updated_at;
    @SerializedName("name")
    @Expose
    String name;
    @SerializedName("active")
    @Expose
    String active;


    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActive() {
        return active;
    }

    public void setActive(String active) {
        this.active = active;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getGroup_id() {
        return group_id;
    }

    public void setGroup_id(String group_id) {
        this.group_id = group_id;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public ChattingModel(String id, String user_id, String group_id, String message, String updated_at) {
        this.id = id;
        this.user_id = user_id;
        this.group_id = group_id;
        this.message = message;
        this.updated_at = updated_at;
    }
}
