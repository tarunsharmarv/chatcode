package com.user.chatcode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by user on 4/1/17.
 */
public class UserModel {

    @SerializedName("id")
    @Expose
    String id;
    @SerializedName("name")
    @Expose
    String name;
    @SerializedName("email")
    @Expose
    String email;
    @SerializedName("phone_number")
    @Expose
    String phone_number;
    @SerializedName("profile_image")
    @Expose
    String profile_image;
    @SerializedName("device")
    @Expose
    String device;
    @SerializedName("device_id")
    @Expose
    String device_id;
    @SerializedName("updated_at")
    @Expose
    String updated_at;
    @SerializedName("created_at")
    @Expose
    String created_at;
    @SerializedName("access_token")
    @Expose
    String access_token;

    public UserModel(String email, String name) {
        this.email = email;
        this.name = name;
    }

    public UserModel(String id, String name, String email, String phone_number, String profile_image,String device_id, String updated_at, String created_at, String access_token) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.phone_number = phone_number;
        this.profile_image = profile_image;
        this.device_id = device_id;
        this.updated_at = updated_at;
        this.created_at = created_at;
        this.access_token = access_token;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public void setPhone_number(String phone_number) {
        this.phone_number = phone_number;
    }

    public String getProfile_image() {
        return profile_image;
    }

    public void setProfile_image(String profile_image) {
        this.profile_image = profile_image;
    }

    public String getAccess_token() {
        return access_token;
    }

    public void setAccess_token(String access_token) {
        this.access_token = access_token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getUpdated_at() {
        return updated_at;
    }

    public void setUpdated_at(String updated_at) {
        this.updated_at = updated_at;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }
}
