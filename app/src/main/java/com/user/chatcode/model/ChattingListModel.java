package com.user.chatcode.model;

/**
 * Created by user on 22/12/16.
 */

public class ChattingListModel {
    String group_icon,banner_updated_at,banner,name,id,user_id,max_user,reset_on,reset_time,expiration,time_type,time_limit,end_date_time,photos,group_code,created_at,usercount;
    Boolean expired;


    public ChattingListModel(String id,String group_code,String name, Boolean expired) {
        this.id=id;
        this.group_code=group_code;
        this.name = name;
        this.expired = expired;
    }

    public ChattingListModel(String id, String group_icon, String name,
                             String user_id, String max_user,
                             String reset_on, String reset_time, String expiration,
                             String time_limit,String time_type, String photos, String group_code,
                             String created_at, String usercount, Boolean expired,String end_date_time,
                             String banner,String banner_updated_at) {
        this.id = id;
        this.group_icon = group_icon;
        this.name = name;
         this.user_id = user_id;
        this.max_user = max_user;
        this.reset_on = reset_on;
        this.reset_time = reset_time;
        this.expiration = expiration;
        this.time_limit = time_limit;
        this.time_type=time_type;
        this.photos = photos;
        this.group_code = group_code;
        this.created_at = created_at;
        this.expired = expired;
        this.usercount=usercount;
        this.end_date_time=end_date_time;
        this.banner=banner;
        this.banner_updated_at=banner_updated_at;
    }

    public String getBanner_updated_at() {
        return banner_updated_at;
    }

    public void setBanner_updated_at(String banner_updated_at) {
        this.banner_updated_at = banner_updated_at;
    }

    public String getBanner() {
        return banner;
    }

    public void setBanner(String banner) {
        this.banner = banner;
    }

    public String getEnd_date_time() {
        return end_date_time;
    }

    public void setEnd_date_time(String end_date_time) {
        this.end_date_time = end_date_time;
    }

    public String getTime_type() {
        return time_type;
    }

    public void setTime_type(String time_type) {
        this.time_type = time_type;
    }

    public String getUsercount() {
        return usercount;
    }

    public void setUsercount(String usercount) {
        this.usercount = usercount;
    }

    public String getGroup_icon() {
        return group_icon;
    }

    public void setGroup_icon(String group_icon) {
        this.group_icon = group_icon;
    }

    public String getUser_id() {
        return user_id;
    }

    public void setUser_id(String user_id) {
        this.user_id = user_id;
    }

    public String getMax_user() {
        return max_user;
    }

    public void setMax_user(String max_user) {
        this.max_user = max_user;
    }

    public String getReset_on() {
        return reset_on;
    }

    public void setReset_on(String reset_on) {
        this.reset_on = reset_on;
    }

    public String getReset_time() {
        return reset_time;
    }

    public void setReset_time(String reset_time) {
        this.reset_time = reset_time;
    }

    public String getExpiration() {
        return expiration;
    }

    public void setExpiration(String expiration) {
        this.expiration = expiration;
    }

    public String getTime_limit() {
        return time_limit;
    }

    public void setTime_limit(String time_limit) {
        this.time_limit = time_limit;
    }

    public String getPhotos() {
        return photos;
    }

    public void setPhotos(String photos) {
        this.photos = photos;
    }

    public String getGroup_code() {
        return group_code;
    }

    public void setGroup_code(String group_code) {
        this.group_code = group_code;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public Boolean getExpired() {
        return expired;
    }

    public void setExpired(Boolean expired) {
        this.expired = expired;
    }
}
