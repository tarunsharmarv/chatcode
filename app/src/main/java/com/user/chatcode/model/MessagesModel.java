package com.user.chatcode.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.user.chatcode.chatting.Message;

import java.util.ArrayList;

/**
 * Created by user on 14/2/17.
 */
public class MessagesModel {

    @SerializedName("total")
    @Expose
    String total;
    @SerializedName("per_page")
    @Expose
    String per_page;
    @SerializedName("current_page")
    @Expose
    String current_page;
    @SerializedName("last_page")
    @Expose
    String last_page;
    @SerializedName("next_page_url")
    @Expose
    String next_page_url;
    @SerializedName("prev_page_url")
    @Expose
    String prev_page_url;
    @SerializedName("from")
    @Expose
    String from;
    @SerializedName("to")
    @Expose
    String to;
    @SerializedName("data")
    @Expose
    ArrayList<ChattingModel> data;

    public String getTotal() {
        return total;
    }

    public void setTotal(String total) {
        this.total = total;
    }

    public String getPer_page() {
        return per_page;
    }

    public void setPer_page(String per_page) {
        this.per_page = per_page;
    }

    public String getCurrent_page() {
        return current_page;
    }

    public void setCurrent_page(String current_page) {
        this.current_page = current_page;
    }

    public String getLast_page() {
        return last_page;
    }

    public void setLast_page(String last_page) {
        this.last_page = last_page;
    }

    public String getNext_page_url() {
        return next_page_url;
    }

    public void setNext_page_url(String next_page_url) {
        this.next_page_url = next_page_url;
    }

    public String getPrev_page_url() {
        return prev_page_url;
    }

    public void setPrev_page_url(String prev_page_url) {
        this.prev_page_url = prev_page_url;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public ArrayList<ChattingModel> getData() {
        return data;
    }

    public void setData(ArrayList<ChattingModel> data) {
        this.data = data;
    }
}
