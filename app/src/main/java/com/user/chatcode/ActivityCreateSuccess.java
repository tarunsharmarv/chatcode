package com.user.chatcode;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;

import com.user.chatcode.apis.ApiFactory;
import com.user.chatcode.apis.UserApi;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.model.UploadModel;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;
import com.user.chatcode.utils.TimeSpan;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

 import static com.user.chatcode.ActivityMainChatting.refreshList1;

/**
 * Created by user on 23/12/16.
 */
public class ActivityCreateSuccess  extends AppCompatActivity implements View.OnClickListener {
    TextView title,photos,max_capacity,reset,limit,expiration,share,expires;
    ImageView qrcode;
    Button crosschatcode;
     Uri uri;
    String code_id;
    ScrollView parent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_success);
        findViewById(R.id.enter).setOnClickListener(this);
        parent            =(ScrollView)findViewById(R.id.parent);
        title             =(TextView)findViewById(R.id.title);
        photos            =(TextView)findViewById(R.id.photos);
        max_capacity      =(TextView)findViewById(R.id.max_capacity);
        reset             =(TextView)findViewById(R.id.reset);
        limit             =(TextView)findViewById(R.id.limit);
        expiration        =(TextView)findViewById(R.id.expiration);
        share             =(TextView)findViewById(R.id.share);
        expires           =(TextView)findViewById(R.id.expires);
        qrcode            =(ImageView)findViewById(R.id.qrcode);
        crosschatcode     =(Button)findViewById(R.id.crosschatcode);
        title       .setText(Constants.name);
        photos      .setText(Constants.photos);
        max_capacity.setText(Constants.max_capacity);
        reset       .setText(Constants.reset0n);
        limit       .setText(Constants.time_limit);
        expiration  .setText(CommonUtils.convertDateFromOneFormatToAnother(Constants.expiration,"yyyy-MM-dd HH:mm:ss","MMM dd,yyyy"));
        expires  .setText(CommonUtils.convertDateFromOneFormatToAnother(Constants.expiration,"yyyy-MM-dd HH:mm:ss","dd/MM/yy"));

        crosschatcode.setOnClickListener(this);
        share.setOnClickListener(this);
        findViewById(R.id.enter).setOnClickListener(this);
        try
        {

//            String data="{\"user_id\":"+Constants.user.getId()+",\"name\":"+"\""+Constants.name+"\""+"\""+",\"photos\":"+"\""+Constants.photos+"\""+",\"max_user\":"+Constants.max_capacity+",\"reset_on\":"+"\""+Constants.reset0n+"\""+",\"reset_time\":"+"\""+Constants.reset+"\""+",\"time_limit\":"+Constants.time_limit+",\"expiration\":"+"\""+Constants.expiration+"\""+"}";
            code_id= String.valueOf(CommonUtils.uniqueCurrentTimeMS());
            Bitmap bmp =  CommonUtils.encodeAsBitmap(code_id);
           uri= CommonUtils.getImageUri(ActivityCreateSuccess.this,bmp);
            String path=CommonUtils.getPath(uri,ActivityCreateSuccess.this);
            qrcode.setImageBitmap(bmp);
            File file=new File(path);
            submitDetails(file);
        } catch (Exception e) {
            e.printStackTrace();
        }
//        addcounter();

     }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.crosschatcode:
            Intent i = new Intent(ActivityCreateSuccess.this, ActivityMainChatting.class);
            startActivity(i);
                finish();
                break;
            case R.id.enter:
                refreshList1(ActivityCreateSuccess.this);
                Intent intent=new Intent(this,ActivityChatting.class);
                intent.putExtra("raw",code_id);
                intent.putExtra("banner",CreateChatecode1.banner_icon);
                intent.putExtra("end_date_time",Constants.expiration);
                intent.putExtra("expiration",Constants.expiration);
                intent.putExtra("title",Constants.name);
                intent.putExtra("group_created_by",CommonUtils.getTempFileName(ActivityCreateSuccess.this,"id"));
                startActivity(intent);
                finish();
                 break;
            case R.id.share:
                share(uri,Constants.name);
                break;
        }
    }
    public void addcounter(){
        final Calendar future = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:dd");
        Date date = null;
        try {
            date = dateFormat.parse(Constants.expiration);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        try {
            future.setTime(date); // month is zero based
             Date now = Calendar.getInstance().getTime();
            final TimeSpan timeSpan = TimeSpan.fromMiliseconds(future.getTimeInMillis() - now.getTime());
//                        Log.d(TAG, "Days left : " + timeSpan.getDays()+" Days "+timeSpan.getHours()+" hour "+timeSpan.getMinutes()+" min "+timeSpan.getSeconds()+" sec "+); // "Days left : 730"
            Timer T = new Timer();
            if(!(timeSpan.getDays()<= 0)) {
                T.scheduleAtFixedRate(new TimerTask() {
                    @Override
                    public void run() {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                expires.setText(timeSpan.getDays() + " Days " + timeSpan.getHours() + " hour " + timeSpan.getMinutes() + " min " + timeSpan.getSeconds() + " sec ");
                            }
                        });
                    }
                }, 1000, 1000);
            }else{
                expires.setText("Expired");
            }
        }catch (Exception e){
            expires.setText("-");
        }
    }
    private void share(Uri photoUri,String text) {
            startActivity(Intent.createChooser(CommonUtils.initPhotoShareMediaIntent(photoUri, text), "Share to"));
    }

    void submitDetails(File _image) {
        final ProgressDialog progressDialog=CommonUtils.setProgress("Uploading  !!!",this);
        UserApi userApi     = ApiFactory.createServiceForGson();
        String fileName = "";
        RequestBody requestBody= null;
        Map<String, RequestBody> requestBodyMap = new HashMap<>();
        if(null!=_image) {
            String image_name = _image.getName();
            fileName = "group_icon\"; filename=\"" + image_name;
            requestBody = RequestBody.create(MediaType.parse("multipart/form-data"), _image);
            requestBodyMap.put(fileName, requestBody);
        }
//        1492150598380
        userApi.uploadQRInfo(requestBodyMap).enqueue(new Callback<UploadModel>() {
            @Override
            public void onResponse(Call<UploadModel> call, Response<UploadModel> response) {
                if (response.body() != null) {
                    try {
                        if (response.body().getMessage().equals("success")) {
                            CommonUtils.addSnackbar(parent, "Succesfully uploaded");
                            String data="{\"user_id\":"+CommonUtils.getTempFileName(ActivityCreateSuccess.this,"id")+", \"group_code\":"+"\""+code_id+"\""+",\"name\":"+"\""+Constants.name+"\""+",\"banner\":"+"\""+CreateChatecode1.banner_icon+"\""+",\"group_icon\":"+"\""+response.body().getNew_image()+"\""+",\"photos\":"+"\""+Constants.photos+"\""+",\"max_user\":"+Constants.max_capacity+",\"reset_on\":"+"\""+Constants.reset0n+"\""+",\"reset_time\":"+"\""+Constants.reset+"\""+",\"time_type\":"+"\""+Constants.time_type+"\""+",\"time_limit\":"+Constants.time_limit+",\"expiration\":"+"\""+Constants.expiration+"\""+"}";
                            ActivityBeforeMainChatting.createRooms(data);
                         } else {
                            ActivityCreateSuccess.this.finish();
                            CommonUtils.addSnackbar(parent, "Unable to upload");
                        }
                    }catch (Exception e){
                        ActivityCreateSuccess.this.finish();
                        CommonUtils.addSnackbar(parent, "Unable to upload");
                    }
                }
                 progressDialog.dismiss();
            }
            @Override
            public void onFailure(Call<UploadModel> call, Throwable t) {
                CommonUtils.addSnackbar(parent, t.getMessage());
                ActivityCreateSuccess.this.finish();
                progressDialog.dismiss();
             }
        });
    }
}
//        {"id":2,"name":"user516665","email":"a@b.com","phone_number":null,"profile_image":null,"device":"Android","device_id":"e579113950eae31c","created_at":"2017-01-04 06:54:44","updated_at":"2017-02-09 10:32:08"},"access_token":"eyJ0eXAiOiJKV1QiLCJhbGciOiJSUzI1NiIsImp0aSI6IjU5YWE3MzkyZDg0M2Q0MGYyNjlhMWI4OTMwZDhjM2U2MGZiZThmMjg4NmYxMzkyZTNhZDY2NjFjMWNjYmVjOGZlMjAxMGYzMmY3OTg2NmMwIn0.eyJhdWQiOiIxIiwianRpIjoiNTlhYTczOTJkODQzZDQwZjI2OWExYjg5MzBkOGMzZTYwZmJlOGYyODg2ZjEzOTJlM2FkNjY2MWMxY2NiZWM4ZmUyMDEwZjMyZjc5ODY2YzAiLCJpYXQiOjE0ODY2Mzg3MTAsIm5iZiI6MTQ4NjYzODcxMCwiZXhwIjoxODAyMTcxNTEwLCJzdWIiOiIyIiwic2NvcGVzIjpbXX0.KeGVlYjZK-Ca5x1cf02RnZs1jTOsSP-vLeU3mVWDUl4uqNhlzBSJZa_ynC4eh-bUQA5Fg6O3nnAcB9W_cEOXCxQ2xMLHalRGPzahu-4KAJ1NPhv2BRjcMiDKPPtNnLXv3S8aOuWYbkkKBaRPanQrvFOqPVoR3mLa8f0IfVaIecLKeJIoGpLpsNiKHpF8izhpqSmR90fPj69HM_KdaFLKW1xXkFQ9ubunYQ6oNvefrMLISm5CjD-Y37MKswj4HD0PsFElKjMEQeSqHiX6vpQ4h5m50CNZ5t0XoKWxrJV9DzU_DBH3NzC9zDDKNVBzhc3ZBizC6kEoTNyHV8kLlOA1bFLN7cm7NTB15QLZR1JY5-NKSrsQUHEKp1hD0vIRPIxIiNryuh4HCm0aMdCU-byhXcHVPKBHWlA7kQ5CW72JzTdS9GcZMswamCiYe7vmQfi2JL4EHLPesLGzKTUpvbc_v2K2NoKpph7XZam7tbMuPwcV7M17exj_9S-rczMF4vAhtftMp1U08ap2YD1xs7kz3rfFtI9tDgCmR5etniCJmvgViurcVdF1dRWeuSqn4e11p69jZrVctv01NiBT6LH4b4oCQVOebL_RfNPt-qeZCaiYew0Pmf9rki7jKUWlQTpyjalsTkm1DlnLUjSgRJsxIg-Og8HRvkd3l7PYW_i1NR4"}
