package com.user.chatcode;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import com.user.chatcode.utils.CommonUtils;
import com.user.chatcode.utils.Constants;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.util.ArrayList;

/**
 * Created by user on 21/12/16.
 */
public class ActivityConfirmation extends AppCompatActivity implements View.OnClickListener {
    EditText numberDialer;
    String phone_number="";
    String deviceId;
    LinearLayout parent;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_confirmation);
        phone_number=getIntent().getStringExtra("phone_number");
        initView();
    }

    private void initView() {
        numberDialer           =(EditText)findViewById(R.id.numberDialer);
        parent                 = (LinearLayout)  findViewById(R.id.parent);
//        GridView gridview      =(GridView) findViewById(R.id.gridview);

        TextView heading=(TextView)findViewById(R.id.textheading);
        heading.setText(heading.getText().toString()+" "+phone_number);
        findViewById(R.id.cancel).setOnClickListener(this);
        findViewById(R.id.next).setOnClickListener(this);
        findViewById(R.id.requestagain).setOnClickListener(this);
        deviceId               = Settings.Secure.getString(this.getContentResolver(),
                Settings.Secure.ANDROID_ID);
//        gridview.setAdapter(new DialerAdater(this,"2"));
//        gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
//            public void onItemClick(AdapterView<?> parent, View v,
//                                    int position, long id) {
//                if(position!=9&&position!=11)
//                    numberDialer.setText(numberDialer.getText().toString()+ Constants.list[position]);
//
//            }
//        });
//        numberDialer.setText(CommonUtils.getTempFileName(ActivityConfirmation.this,"code"));
        numberDialer.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if(numberDialer.getText().toString().equals(""))
                    findViewById(R.id.cancel).setVisibility(View.GONE);
                else
                    findViewById(R.id.cancel).setVisibility(View.VISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
    }


    @Override
    public void onBackPressed(){
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()){
            case R.id.cancel:
                numberDialer.setText(numberDialer.getText().toString().substring(0,numberDialer.getText().toString().length()-1));
                break;
            case R.id.requestagain:
                onBackPressed();
                break;
            case R.id.next:
                if(CommonUtils.getTempFileName(ActivityConfirmation.this,"code").equals(numberDialer.getText().toString()))
                    new jsonTask().execute();
                else
                    CommonUtils.getSnackbar(parent,"Verification code does not match");
                break;
        }
    }


    public class jsonTask extends AsyncTask<String,String,String> {
        ProgressDialog progressDialog;
        @Override
        protected  void onPreExecute(){
            progressDialog=new ProgressDialog(ActivityConfirmation.this);
            progressDialog.setMessage("Loading");
            progressDialog.show();
        }
        @Override
        protected String doInBackground(String... params)
        {
            ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            String email=CommonUtils.getTempFileName(ActivityConfirmation.this,"email");
            if(CommonUtils.checkEmail(phone_number))
                nameValuePairs.add(new BasicNameValuePair("email", email));
            else
                nameValuePairs.add(new BasicNameValuePair("phone_number", email));
            nameValuePairs.add(new BasicNameValuePair("device", Constants.DEVICE_TYPE));
            nameValuePairs.add(new BasicNameValuePair("device_id", deviceId));

            HttpURLConnection connection = null;
            BufferedReader reader = null;
            InputStream is=null;

            try {
                HttpClient httpclient = new DefaultHttpClient();
                HttpPost httppost = new HttpPost("http://54.200.70.163/chatcode/public/api/signup");
                httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
                HttpResponse response = httpclient.execute(httppost);
                HttpEntity entity = response.getEntity();
                is = entity.getContent();
                Log.e("pass 1", "connection success ");
                reader = new BufferedReader(new InputStreamReader(is));
                StringBuffer buffer = new StringBuffer();
                String line = "";
                while ((line = reader.readLine()) != null) {
                    buffer.append(line);
                }
                return (buffer.toString());
            }
            catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            finally {
                if(connection!=null) {
                    connection.disconnect();
                }
                try {
                    if(reader!=null) {
                        reader.close();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            progressDialog.dismiss();
            try {
                JSONObject obj=new JSONObject(s);
                JSONObject jsonObject=obj.getJSONObject("user");
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"id",obj.getJSONObject("user").getString("id"));
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"name",obj.getJSONObject("user").getString("name"));
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"email",obj.getJSONObject("user").getString("email"));
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"phone_number",obj.getJSONObject("user").getString("phone_number"));
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"profile_image",obj.getJSONObject("user").getString("profile_image"));
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"device_id",obj.getJSONObject("user").getString("device_id"));
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"updated_at",obj.getJSONObject("user").getString("updated_at"));
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"created_at",obj.getJSONObject("user").getString("created_at"));
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"access_token",obj.getString("access_token"));
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"logged_in","1");
                CommonUtils.saveTempFileName(ActivityConfirmation.this,"push_notification",obj.getJSONObject("user").getString("push_notification"));

//
//                Constants.user=new UserModel(
//                        obj.getJSONObject("user").getString("id"),
//                        obj.getJSONObject("user").getString("name"),
//                        obj.getJSONObject("user").getString("email"),
//                        obj.getJSONObject("user").getString("phone_number"),
//                        obj.getJSONObject("user").getString("profile_image"),
//                        obj.getJSONObject("user").getString("device_id"),
//                        obj.getJSONObject("user").getString("updated_at"),
//                        obj.getJSONObject("user").getString("created_at"),
//                        obj.getString("access_token"));
                if(null!=jsonObject) {
                    Intent i = new Intent(ActivityConfirmation.this, ActivityProfile.class);
                    startActivity(i);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }
}

