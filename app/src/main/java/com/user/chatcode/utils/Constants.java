package com.user.chatcode.utils;

/**
 * Created by user on 21/12/16.
 */
public class Constants {

    public static String web_url="support.chatcodes.com";
    public static String DEVICE_TYPE ="Android" ;
    public static String[] list={"1","2","3","4","5","6","7","8","9","abc","0","<"};
    public static String[] list1={"1","2","3","4","5","6","7","8","9","","0","<"};
    public static String[] listabc={"q","w","e","r","t","y","u","i","o","p","a","s",
            "d","f","g","h","j","k","l","z","x","c","v","b","n","m"};
    public static String[] listABC={"Q","W","E","R","T","Y","U","I","O","P","A","S",
            "D","F","G","H","J","K","L","Z","X","C","V","B","N","M"};
    public static String[] listsymbols={"!","@","#","$","%","^","&","-","+","(",")",""};
    public static final String BASE_URL                 = "http://54.200.70.163/chatcode/public/api/";
    public static final String  SEND_VERIFICATION_URL   = "sendcodeonemailorphone";/*POST*/
    public static final String  AFTER_VERIFICATION_URL  = "signup";/*POST*/
    public static final String GALLERY_DETAILS_URL      = "uploadimage";/*POST*/
    public static final String GETALLUSER_DETAILS_URL   = "getalluserofgroup";
    public static final String CHAT_SERVER_URL          = "http://54.200.70.163:3000/";
    public static final String IMAGES_URL               = "http://54.200.70.163/chatcode/public/profilepics/";
    public static final String NEXT_PAGE_URL            = "loadoldhistory";
    public static final String UPDATE_PROFILE_URL       = "updateprofile";
    public static final String UPDATE_TOKEN_URL         = "updatepushtoken";
    public static final String PUSH_NOTI_URL            = "pushonoff";
    public static final String DELETE_CHAT_URL          = "deletechatcode";
    public static final String JOIN_CHAT_URL            = "jointochatcode";
    public static final String GET_GROUP_URL            = "getallgroup";
    public static final String READ_ONLY_URL            = "onreadonly";
    public static final String NEXT_PAGE_URL_           = "getmessagehistory";
    public static final String UPDATE_BANNER_URL_       = "uploadbanner";
      //    public static UserModel user;
    public static String name="",photos="",max_capacity="",reset0n="",reset="",time_limit="",expiration="",time_type="";
}

//http://54.200.70.163/chatcode/public/api/getalluserofgroup