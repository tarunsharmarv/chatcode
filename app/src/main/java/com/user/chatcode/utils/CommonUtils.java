package com.user.chatcode.utils;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.MultiFormatWriter;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.user.chatcode.ActivityCreateChatcode;
import com.user.chatcode.CreateChatecode1;
import com.user.chatcode.R;
import com.user.chatcode.chatting.MainActivity;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;

import javax.sql.DataSource;

import static android.R.attr.path;
import static okhttp3.internal.Util.UTC;

/**
 * Created by user on 21/12/16.
 */
public class CommonUtils {
    static int pMonth,pYear,pDay;
    static AlertDialog show;
    public static void addSnackbar(View coordinatorLayout,String message){
        Snackbar snackbar = Snackbar
                .make(coordinatorLayout, message, Snackbar.LENGTH_LONG);
        snackbar.show();
    }
    static String newFormat;
    public static String printDifference_(String createdAt) {
        if (createdAt != null) {

            SimpleDateFormat inputFormat11 = new SimpleDateFormat("yyyy-MM-dd");
            inputFormat11.setTimeZone(TimeZone.getTimeZone("UTC"));
            Date startDate = null;
            try {
                startDate = inputFormat11.parse(createdAt);
                SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd,yyyy");
                newFormat = formatter.format(startDate);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            return newFormat;

        } else {
            return "Not available";
        }

    }
    public static Date FormatDate( Date d ) {
            SimpleDateFormat sd=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            System.out.println(sd.format(d));
             Date date = null;
             try {
                 date = new Date(sd.parse(date.toString()).getTime());
             } catch (ParseException e) {
                 e.printStackTrace();
             }
             System.out.println(date);
             return date;

    }
    public static String getCalculatedDate(String date, String dateFormat, int days) {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat s = new SimpleDateFormat(dateFormat);
        cal.add(Calendar.DAY_OF_YEAR, days);
        try {
            return s.format(new Date(s.parse(date).getTime()));
        } catch (ParseException e) {
            // TODO Auto-generated catch block
            Log.e("TAG", "Error in Parsing Date : " + e.getMessage());
        }
        return null;
    }
    public static String getCurrentDate() {
                Calendar c = Calendar.getInstance();
                SimpleDateFormat formatter = new SimpleDateFormat("MMMM dd,yyyy");
                String date = formatter.format(c.getTime());
            return date;
    }
    public static  Date getConverteddate1(){
        Calendar c = Calendar.getInstance();
        SimpleDateFormat dateFormat = new SimpleDateFormat(("yyyy-MM-dd HH:mm:ss"));
        Date date = null;
        try {
            date = dateFormat.parse(dateFormat.format(c.getTime()));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date);
        return date;
    }

    public static void hideKeyboard(Activity activity) {
        if (activity != null && activity.getWindow() != null && activity.getWindow().getDecorView() != null) {
            InputMethodManager imm = (InputMethodManager)activity.getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(activity.getWindow().getDecorView().getWindowToken(), 0);
        }
    }

public static Date convertToLocal(String strDate){
    SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    dateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
    Date date = null;
    try {
        date = dateFormat.parse(strDate);
    } catch (ParseException e) {
        e.printStackTrace();
    }
    dateFormat.setTimeZone(TimeZone.getDefault());
    String formattedDate = dateFormat.format(date);
    Date date1 = null;
    try {
        date1 = dateFormat.parse(formattedDate);
    } catch (ParseException e) {
        e.printStackTrace();
    }

//     dateFormat.setTimeZone(TimeZone.getDefault());
//    Date date = null;
//    try {
//        date = dateFormat.parse(strDate);
//    } catch (ParseException e) {
//        e.printStackTrace();
//    }
//    System.out.println(date);
    return date1;
}
    public static  Date getConverteddate(String strDate){
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
         Date date = null;
        try {
            date = dateFormat.parse(strDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date);
        return date;
    }

   public static String convertDateFromOneFormatToAnother(String dateStr, String oldFormat, String newFormat){
        try {
             DateFormat srcDf = new SimpleDateFormat(oldFormat);
            Date date = srcDf.parse(dateStr);
            DateFormat destDf = new SimpleDateFormat(newFormat);
            dateStr = destDf.format(date);
            System.out.println("Converted date is : " + dateStr);
        }
        catch (ParseException e) {
            e.printStackTrace();
        }

        return dateStr;
    }




    public static final AtomicLong LAST_TIME_MS = new AtomicLong();
    public static long uniqueCurrentTimeMS() {
        long now = System.currentTimeMillis();
        while(true) {
            long lastTime = LAST_TIME_MS.get();
            if (lastTime >= now)
                now = lastTime+1;
            if (LAST_TIME_MS.compareAndSet(lastTime, now))
                return now;
        }
    }


    public static String getPath(Uri uri,Context context)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = context.getContentResolver().query(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index =             cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        cursor.close();
        return s;
    }

    public static ProgressDialog setProgress(String message, Context context){
        ProgressDialog progressDialog=new ProgressDialog(context);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
        return progressDialog;
    }
    public static Intent initPhotoShareMediaIntent(Uri uri, String text) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_SUBJECT, text);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.putExtra(Intent.EXTRA_STREAM, uri);
        shareIntent.setType("image/*");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        return shareIntent;
    }
    public static Intent initPhotoShareIntent(String text) {
        Intent shareIntent = new Intent();
        shareIntent.setAction(Intent.ACTION_SEND);
        shareIntent.putExtra(Intent.EXTRA_TEXT, text);
        shareIntent.setType("text/plain");
        shareIntent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        return shareIntent;
    }



    public static void saveTempFileName(Context context, String name, String filename) {
        SharedPreferences settings = context.getSharedPreferences("sharedprefs", 0);
        SharedPreferences.Editor editor = settings.edit();
        editor.putString(name, filename);
        editor.commit();
    }

    public static String getTempFileName(Context context, String filename) {
        SharedPreferences settings = context.getSharedPreferences("sharedprefs", 0);
        String value = settings.getString(filename, "");
        return value;
    }


    public static final Pattern EMAIL_ADDRESS_PATTERN = Pattern.compile(
            "[a-zA-Z0-9\\+\\.\\_\\%\\-\\+]{1,256}" +
                    "\\@" +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,64}" +
                    "(" +
                    "\\." +
                    "[a-zA-Z0-9][a-zA-Z0-9\\-]{0,25}" +
                    ")+"
    );
    public static boolean checkEmail(String email) {
        return EMAIL_ADDRESS_PATTERN.matcher(email).matches();
    }

    public static DatePickerDialog.OnDateSetListener pDateSetListener=
            new DatePickerDialog.OnDateSetListener() {

                public void onDateSet(DatePicker view, int year,
                                      int monthOfYear, int dayOfMonth) {
                    pYear   = year;
                    pMonth  = monthOfYear;
                    pDay    = dayOfMonth;
                    updateDisplay();
                }

            };

    private static void updateDisplay() {
        pMonth=pMonth+1;
        Constants.expiration=pYear+"-"+pMonth+"-"+pDay+" 00:00:00";// Month is 0 based so add 1
        CreateChatecode1.picker_expiration.setText(CommonUtils.printDifference_(new StringBuilder()
                // Month is 0 based so add 1
                .append(pYear).append("-")
                .append(pMonth).append("-")
                .append(pDay).append("").toString()));

    }

    public static int white = 0xFFFFFFFF;
    public static int black = 0xFF000000;
    public final static int WIDTH=500;


    public static Bitmap encodeAsBitmap(String str) throws WriterException {
        BitMatrix result;
        Bitmap bitmap=null;
        try
        {
            result = new MultiFormatWriter().encode(str,
                    BarcodeFormat.QR_CODE, WIDTH, WIDTH, null);

            int w = result.getWidth();
            int h = result.getHeight();
            int[] pixels = new int[w * h];
            for (int y = 0; y < h; y++) {
                int offset = y * w;
                for (int x = 0; x < w; x++) {
                    pixels[offset + x] = result.get(x, y) ? black:white;
                }
            }
            bitmap = Bitmap.createBitmap(w, h, Bitmap.Config.ARGB_8888);
            bitmap.setPixels(pixels, 0, 500, 0, 0, w, h);
        } catch (Exception iae) {
            iae.printStackTrace();
            return null;
        }
        return bitmap;
    }

    public static void getSnackbar(View view, String message) {
        Snackbar.make(view, message, Snackbar.LENGTH_LONG).
                setAction("Action", null).
                show();
    }

    public static Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public static void Image_Picker_Dialog(final Activity context) {
        final AlertDialog.Builder myAlertDialog = new AlertDialog.Builder(context,AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
        myAlertDialog.setTitle("Pictures Option");
        myAlertDialog.setMessage("Select Image by using");
        myAlertDialog.setPositiveButton("Gallery", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Intent pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
                pictureActionIntent.setType("image/*");
                pictureActionIntent.putExtra("return-data", true);
                context.startActivityForResult(pictureActionIntent, 01);
                show.dismiss();
            }
        });

        myAlertDialog.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface arg0, int arg1) {
                Intent pictureActionIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                context.startActivityForResult(pictureActionIntent, 11);
                show.dismiss();
            }
        });
        myAlertDialog.setNeutralButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                show.dismiss();
            }
        });
        show = myAlertDialog.show();




//       final Dialog myAlertDialog = new Dialog(context);
//         myAlertDialog.setContentView(R.layout.image_pickup_dialog);
//         myAlertDialog.findViewById(R.id.button_take_new).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent pictureActionIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
//                context.startActivityForResult(pictureActionIntent, 1);
//                myAlertDialog.dismiss();
//            }
//        });
//        myAlertDialog.findViewById(R.id.button_choose).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                Intent pictureActionIntent = new Intent(Intent.ACTION_GET_CONTENT, null);
//                pictureActionIntent.setType("image/*");
//                pictureActionIntent.putExtra("return-data", true);
//                context.startActivityForResult(pictureActionIntent, 0);
//                myAlertDialog.dismiss();
//            }
//        });
//
//        myAlertDialog.findViewById(R.id.button_cancel).setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                myAlertDialog.dismiss();
//            }
//        });
//        myAlertDialog.show();

    }

    public static File createDirectoryAndSaveFile(Bitmap imageToSave, String fileName, Context con) {
        File file = new File(con.getFilesDir(), fileName);
        if (!file.exists()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
//         if (file.exists()) {
//            file.delete();
//        }
        try {
            FileOutputStream out = new FileOutputStream(file);
            imageToSave.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return file;
    }
    public static Bitmap resizeImage(ImageView imageView, Bitmap bitMap){
        int currentBitmapWidth = bitMap.getWidth();
        int currentBitmapHeight = bitMap.getHeight();
        int ivWidth;
        int ivHeight;
        Bitmap newbitMap;
        if(null == imageView){
             ivWidth = 300;
             ivHeight = 300;
            int newWidth = ivWidth;
            int newHeight = (int) Math.floor((double) currentBitmapHeight *( (double) newWidth / (double) currentBitmapWidth));
            newbitMap = Bitmap.createScaledBitmap(bitMap, newWidth, newHeight, true);
        }else {
             ivWidth = imageView.getWidth();
             ivHeight = imageView.getHeight();
            int newWidth = ivWidth;
            int newHeight = (int) Math.floor((double) currentBitmapHeight *( (double) newWidth / (double) currentBitmapWidth));
            newbitMap = Bitmap.createScaledBitmap(bitMap, newWidth/2, newHeight/2, true);
            imageView.setImageBitmap(newbitMap);
        }
//        int newWidth = ivWidth;
//        int newHeight = (int) Math.floor((double) currentBitmapHeight *( (double) newWidth / (double) currentBitmapWidth));
//        Bitmap newbitMap = Bitmap.createScaledBitmap(bitMap, newWidth/2, newHeight/2, true);
//        imageView.setImageBitmap(newbitMap);
        return newbitMap;
    }
}
