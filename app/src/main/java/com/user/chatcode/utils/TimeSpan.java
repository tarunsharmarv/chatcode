package com.user.chatcode.utils;

public class TimeSpan implements Comparable<TimeSpan> {
    Long days;
    Long hours;
    Long minutes;
    Long seconds;
    Long milliseconds;

    public TimeSpan() {
        days = hours = minutes = seconds = milliseconds = 0l;
    }

    public TimeSpan(long _days, long _hours, long _minutes, long _seconds, long _milliseconds) {

        long totalMS = _days * 86400000 + _hours * 3600000 + _minutes * 60000 + _seconds * 1000 + _milliseconds;

        days = Long.valueOf(totalMS / 86400000);
        totalMS -= days * 86400000;

        hours = Long.valueOf((int) totalMS / 3600000);
        totalMS -= hours * 3600000;

        minutes = Long.valueOf((int) totalMS / 60000);
        totalMS -= minutes * 60000;

        seconds = Long.valueOf((int) totalMS / 1000);
        totalMS -= seconds * 1000;

        milliseconds = Long.valueOf((int) totalMS);
    }

    public static TimeSpan fromSeconds(int _seconds) {
        return new TimeSpan(0, 0, 0, _seconds, 0);
    }

    public static TimeSpan fromMiliseconds(long _miliseconds) {
        return new TimeSpan(0, 0, 0, 0, _miliseconds);
    }

    @Override
    public String toString() {
        return Long.toString(getTotalMilliseconds());
    }

    public String toSimpleShortString() {
        if (hours > 0)
            return String.format("%d:%d:%02d", hours, minutes, seconds);
        else
            return String.format("%d:%02d", minutes, seconds);
    }

    public String toShortString() {
        return String.format("%02d:%02d:%02d", hours, minutes, seconds);
    }

    public String toLongString() {
        return String.format("%02d:%02d:%02d:%02d", hours, minutes, seconds, milliseconds);
    }

    public long getHours() {
        return hours;
    }

    public long getDays() {
        return days;
    }

    public long getMinutes() {
        return minutes;
    }

    public long getSeconds() {
        return seconds;
    }

    public long getMilliseconds() {
        return milliseconds;
    }

    public long getTotalMilliseconds() {
        return days * 86400000 + hours * 3600000 + minutes * 60000 + seconds * 1000 + milliseconds;
    }

    @Override
    public int compareTo(TimeSpan another) {
        return (int) (getTotalMilliseconds() - another.getTotalMilliseconds());
    }
}