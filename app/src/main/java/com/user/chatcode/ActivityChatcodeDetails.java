package com.user.chatcode;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

/**
 * Created by user on 23/12/16.
 */
public class ActivityChatcodeDetails extends AppCompatActivity{
    TextView heading,title,number;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatcodedetails);
        heading=(TextView)findViewById(R.id.heading);
        title  =(TextView)findViewById(R.id.title);
        number  =(TextView)findViewById(R.id.number);
        if(null!=getIntent().getStringExtra("title")) {
            heading.setText(getIntent().getStringExtra("title"));
            title.setText(getIntent().getStringExtra("title"));
        }if(null!=getIntent().getStringExtra("number")){
            number.setText(getIntent().getStringExtra("number"));
        }
     }
}