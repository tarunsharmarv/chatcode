package com.user.chatcode.qrscanner;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.google.zxing.Result;
import com.user.chatcode.ActivityChatting;
import com.user.chatcode.ActivityMainChatting;
import com.user.chatcode.R;
import com.user.chatcode.chatting.ActivityBeforeMainChatting;
import com.user.chatcode.utils.CommonUtils;

import me.dm7.barcodescanner.zxing.ZXingScannerView;

public class QrcodeScreen extends AppCompatActivity implements ZXingScannerView.ResultHandler {
    private ZXingScannerView mScannerView;

    public static String valFrom;
    @Override
    public void onCreate(Bundle state) {
        super.onCreate(state);
        setContentView(R.layout.qr_screen);
         mScannerView = (ZXingScannerView)findViewById(R.id.content_frame);
         mScannerView.setResultHandler(this);
         mScannerView.startCamera();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        ActivityMainChatting.tabSelected=1;
        Intent intent=new Intent(this,ActivityBeforeMainChatting.class);
         startActivity(intent);
        finish();
        ActivityCompat.finishAffinity(QrcodeScreen.this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mScannerView.stopCamera();
    }


    @Override
    public void handleResult(Result rawResult) {
        ActivityMainChatting.tabSelected=1;
        Intent intent=new Intent(this,ActivityChatting.class);
        intent.putExtra("raw",rawResult.getText());
        startActivity(intent);
         finish();
        Log.v("result", rawResult.getText());
    }
}